import React, { useEffect, useState, useRef } from "react";
import { CButton } from "@coreui/react";

const SelectFile = ({ onChange = () => {} }) => {
  const InputFile = useRef(null);

  const func_openSelectfile = () => {
    InputFile.current.click();
  };

  const func_pickFile = () => {
    onChange(InputFile.current);
  };
  return (
    <>
      <CButton
        onClick={() => {
          func_openSelectfile();
        }}
        block
        variant="outline"
        color="info"
      >
        Choose File <span style={{color: "red"}}>* ขนาดรูปที่แนะนำ 1000 x 1000 pixels</span>
      </CButton>
      <input
        onChange={(e) => {
          func_pickFile();
        }}
        type="file"
        style={{ display: "none" }}
        ref={InputFile}
      ></input>
    </>
  );
};

export default SelectFile;
