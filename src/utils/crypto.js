import {secretKey, algorithm, randomBytes } from "../env"
import crypto from 'crypto';
// const crypto = require('crypto');
// const algorithm = process.env.algorithm;
// const secretKey = process.env.secretKey;
const iv = crypto.randomBytes(parseInt(randomBytes));

export const encrypt = (text) => {
  console.log(iv);
  const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return {
    iv: iv.toString('hex'),
    content: encrypted.toString('hex'),
  };
};

export const decrypt = (hash) => {
  // console.log("hash::", hash);
  // console.log("algorithm::", algorithm);
  const decipher = crypto.createDecipheriv(
    algorithm,
    secretKey,
    Buffer.from(hash.iv, 'hex')
  );
  // console.log("decipher::", decipher);
  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, 'hex')),
    decipher.final(),
  ]);
  // console.log("decrpyted::", decrpyted);
  return decrpyted.toString();
};

