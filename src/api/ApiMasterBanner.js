import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterBanner extends Component {

    static getAllMtBanner = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mt_banner/get`,
            method: "post",
            data: data
        });
        return result;
    };
    static updateMtBanner = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mt_banner`,
            method: "put",
            data: data
        });
        return result;
    };
    static update_status = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mt_banner/status`,
            method: "put",
            data: data
        });
        return result;
    };
    static deleteMtBanner = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mt_banner/${id}`,
            method: "delete",
        });
        return result;
    };

    static insertDataMtBanner = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mt_banner`,
            method: "post",
            data: data
        });
        return result;
    };
}

export default ApiMasterBanner;