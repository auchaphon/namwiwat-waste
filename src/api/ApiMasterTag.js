import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterTag extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_tag/get`,
      method: "post",
      data: data
    });
    return result;
  };
  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_tag",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_tag",
      method: "delete",
      data: data,
    });
    return result;
  };

  static getddl = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_tag/getddl`,
      method: "get",
    });
    return result;
  };
}
