import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiTranRedeem extends Component {
    static getAllTranRedeem = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/get`,
            method: "post",
            data: data
        });
        return result;
    };
    static updateRedeemStatus = async (id,update) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/${id}/${update}`,
            method: "put",
        });
        return result;
    };
    static unapproveRedeemStatus = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/unapprove`,
            method: "put",
            data: data
        });
        return result;
    };
    static updateTracking = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/updatetracking`,
            method: "put",
            data: data
        });
        return result;
    };

    static getLogTranRedeem = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/log`,
            method: "post",
            data: data
        });
        return result;
    };

    static onExport = async (data) => {
      await setHeaderAuth();
      const result = await axios({
        url: `tranredeem/export`,
        method: "post",
        data: data
      });
      return result;
    };

    static onExportPending = async (data) => {
      await setHeaderAuth();
      const result = await axios({
        url: `tranredeem/getexport`,
        method: "post",
        data: data
      });
      return result;
    };
 
}


export default ApiTranRedeem;