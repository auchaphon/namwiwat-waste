import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiDashboard extends Component {
    static getAll = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `dashboard`,
            method: "get",
        });
        return result;
    };
    static getDashboard = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `dashboard`,
            method: "post",
            data: data
        });
        return result;
    };
    static getTop10Product = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `dashboard/top10product`,
            method: "post",
            data: data
        });
        return result;
    };
    static getTop10Catalog = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `dashboard/top10catalog`,
            method: "post",
            data: data
        });
        return result;
    };
}


export default ApiDashboard;