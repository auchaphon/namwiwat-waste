import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterArm extends Component {
  static getAll = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_arm/get`,
      method: "post",
      data: data,
    });
    return result;
  };
  static update = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: `mt_arm`,
      method: "put",
      data: data,
    });
    return result;
  };
  static update_status = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_arm/status`,
      method: "put",
      data: data,
    });
    return result;
  };
  static delete = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_arm/${id}`,
      method: "delete",
    });
    return result;
  };

  static insert = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: `mt_arm`,
      method: "post",
      data: data,
    });
    return result;
  };
}

export default ApiMasterArm;
