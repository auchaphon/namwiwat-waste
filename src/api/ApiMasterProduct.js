import axios from "axios";
import { Component } from "react";
import { setHeaderAuth, setHeaderFromDataAuth } from "../utils";

export default class ApiMasterProduct extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlist`,
      method: "post",
      data: data
    });
    return result;
  };

  static getedit = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getedit?pid=${id}`,
      method: "get",
    });
    return result;
  };

  static getlistidcommanotequeal = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlistidcommanotequeal`,
      method: "post",
      data: data,
    });
    return result;
  };

  static getlistbyidcomma = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlistbyidcomma`,
      method: "post",
      data: data,
    });
    return result;
  };

  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_product",
      method: "delete",
      data: data,
    });
    return result;
  };

  static save = async (data, id) => {
    await setHeaderAuth();
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: "mt_product",
      method: id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };

  static update_status = async (data, id) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_product/status",
      method: "put",
      data: data,
    });
    return result;
  };

  static uploadFile = async (data) => {
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: "mt_product/uploadfile",
      method: "post",
      data: data,
    });
    return result;
  };
  static onExport = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/export`,
      method: "post",
      data: data
    });
    return result;
  };

  static update_stock = async (data, id) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_product/stock",
      method: "put",
      data: data,
    });
    return result;
  };

  static update_stock_attr = async (data, id) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_product/stockattr",
      method: "put",
      data: data,
    });
    return result;
  };
}
