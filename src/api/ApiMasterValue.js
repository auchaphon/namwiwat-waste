import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterValue extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_value/getlist`,
      method: "post",
      data: data,
    });
    return result;
  };
  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_value",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };

  static getddlsize = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_value/getddlsize`,
      method: "get",
    });
    return result;
  };
}
