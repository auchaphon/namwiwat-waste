import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiUser extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_user/get`,
      method: "post",
      data: data,
    });
    return result;
  };
  static getDetail = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_user/${id}`,
      method: "get",
    });
    return result;
  };
  // static getByBranch = async (branch_id) => {
  //     await setHeaderAuth();
  //     const result = await axios({
  //         url: `mt_user/branch/${branch_id}`,
  //         method: "get",
  //     });
  //     return result;
  // };
  // static getByTeam = async (team_id) => {
  //     await setHeaderAuth();
  //     const result = await axios({
  //         url: `mt_user/team/${team_id}`,
  //         method: "get",
  //     });
  //     return result;
  // };
  // static getForTeam = async () => {
  //     await setHeaderAuth();
  //     const result = await axios({
  //         url: `mt_user/forTeam`,
  //         method: "get",
  //     });
  //     return result;
  // };
  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_user",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };

  static login = async (data) => {
    const result = await axios({
      url: "mt_user/login",
      method: "post",
      data: data,
    });
    return result;
  };

  static changePass = async (data) => {
    const result = await axios({
      url: "mt_user/changePass",
      method: "post",
      data: data,
    });
    return result;
  };

  static editrole = async (data) => {
    const result = await axios({
      url: "mt_user/editrole",
      method: "post",
      data: data,
    });
    return result;
  };
}
