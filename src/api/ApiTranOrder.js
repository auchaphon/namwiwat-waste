import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiTranOrder extends Component {
    static getlistOrder = async (data) => {
        const result = await axios({
            url: `tranOrder/listOrder`,
            method: "post",
            data: data
        });
        return result;
    };
    static onExport = async (data) => {
        const result = await axios({
            url: `tranOrder/export`,
            method: "post",
            data: data
        });
        return result;
    };
    static onExportDetail = async (data) => {
        const result = await axios({
            url: `tranOrder/exportdetail`,
            method: "post",
            data: data
        });
        return result;
    };

    static getOrderDetail = async (order_ref) => {
        const result = await axios({
            url: `tranOrderDetail/getOrder`,
            method: "post",
            data: order_ref
        });
        return result;
    };

    static getOrderWithDetail = async (data) => {
        const result = await axios({
            url: `tranOrderDetail/getOrderDetail`,
            method: "post",
            data: data
        });
        return result;
    };

    static getComment = async (order_ref) => {
        const result = await axios({
            url: `tran_order_comment/${order_ref}`,
            method: "get"
        });
        return result;
    };

    static insertComment = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_order_comment`,
            method: "post",
            data: data
        });
        return result;
    };

    static deleteComment = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_order_comment/${id}`,
            method: "delete"
        });
        return result;
    };

    static getOrderRef = async (order_ref) => {
        const result = await axios({
            url: `tranOrder/getOrder`,
            method: "post",
            data: order_ref
        });
        return result;
    };
    static getLogPayment = async (order_ref) => {
        const result = await axios({
            url: `tranOrder/getLogPay`,
            method: "post",
            data: order_ref
        });
        return result;
    };

    static UpdateNote = async (data) => {
        const result = await axios({
            url: `tranOrder/updatenote`,
            method: "post",
            data: data
        });
        return result;
    };

    static UpdateTracking = async (data) => {
        const result = await axios({
            url: `tranOrder/updatetracking`,
            method: "post",
            data: data
        });
        return result;
    };
    static UpdateAd = async (data) => {
        const result = await axios({
            url: `tranOrder/updateAd`,
            method: "post",
            data: data
        });
        return result;
    };

}


export default ApiTranOrder;