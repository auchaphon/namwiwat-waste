import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterFontColour extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_fontcolour`,
      method: "get",
    });
    return result;
  };
  static getActive = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_fontcolour/active`,
      method: "get",
    });
    return result;
  };

  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_fontcolour",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };
}

export default ApiMasterFontColour;
