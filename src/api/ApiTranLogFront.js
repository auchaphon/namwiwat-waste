import axios from 'axios';
import { Component } from 'react';
import { setHeaderAuth } from "../utils";

class ApiTranLogFront extends Component {
    static get = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_log_front/get`,
            method: "post",
            data: data
        });
        return result;
    };

    static onExport = async (data) => {
      await setHeaderAuth();
      const result = await axios({
        url: `tran_log_front/export`,
        method: "post",
        data: data
      });
      return result;
    };
}


export default ApiTranLogFront;