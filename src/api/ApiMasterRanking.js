import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterRedeem extends Component {

    static getAllMtRanking = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtranking`,
            method: "get",
        });
        return result;
    };
    static updateMtRanking = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtranking`,
            method: "put",
            data: data
        });
        return result;
    };
}

export default ApiMasterRedeem;