import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterMenu extends Component {
  static get = async (country) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_menu`,
      method: "get",
    });
    return result;
  };

  static findByrole = async (model) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_menu/findByrole`,
      method: "post",
      data: model,
    });
    return result;
  };

  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_menu",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };

  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_menu/${data.id}`,
      method: "delete",
    });
    return result;
  };

  static editrole = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_menu/editrole",
      method: "post",
      data: data,
    });
    return result;
  };
}

export default ApiMasterMenu;
