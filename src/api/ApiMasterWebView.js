import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterWebView extends Component {
  static getList = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `webview`,
      method: "get",
    });
    return result;
  };

  static update = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `webview/update`,
      method: "post",
      data: data,
    });
    return result;
  };
}

export default ApiMasterWebView;
