import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterCategory extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_category/get`,
      method: "post",
      data: data
    });
    return result;
  };
  static getParent = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_category/getparent`,
      method: "get"
    });
    return result;
  };
  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_category",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_category",
      method: "delete",
      data: data,
    });
    return result;
  };

  static getddl = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_category/getddl",
      method: "get",
    });
    return result;
  };

  static getsubddl = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_category/getsubddl`,
      method: "get",
    });
    return result;
  };
}
