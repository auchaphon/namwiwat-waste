import axios from 'axios';
import { Component } from 'react';
import { setHeaderAuth } from "../utils";

class ApiTranLogBack extends Component {
    static get = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_log_back/get`,
            method: "post",
            data: data
        });
        return result;
    };
    static addlog = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_log_back`,
            method: "post",
            data: data
        });
        return result;
    };
}


export default ApiTranLogBack;