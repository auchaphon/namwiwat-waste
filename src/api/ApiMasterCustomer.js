import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterCustomer extends Component {
  static getList = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_customer/get`,
      method: "post",
      data: data
    });
    return result;
  };
  static update = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_customer/update`,
      method: "put",
      data: data
    });
    return result;
  };

  static GetEdit = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_customer/getedit?id=${id}`,
      method: "get",
    });
    return result;
  };

  static onExport = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_customer/export`,
      method: "post",
      data: data
    });
    return result;
  };
}
