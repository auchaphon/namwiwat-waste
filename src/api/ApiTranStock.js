import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiTranStock extends Component {

    static getAllTranStock = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/get`,
            method: "post",
            data: data
        });
        return result;
    };

    static GetAllTranStockProduct = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/product`,
            method: "get",
        });
        return result;
    };

    static GetAllTranStockRedeem = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/redeem`,
            method: "get",
        });
        return result;
    };

    static GetTranStockByIdRelate = async (data) => {

        await setHeaderAuth();
        const result = await axios({
            url: `transtock/check`,
            method: "post",
            data : data
        });
        return result;
    };

    static insertDataTranStock = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock`,
            method: "post",
            data : data
        });
        return result;
    };

    static updateBalanceTranStock = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock`,
            method: "put",
            data: data
        });
        return result;
    };

    static deleteTranStock = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/${id}`,
            method: "delete",
        });
        return result;
    };

    static insertDataMtRedeem = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mtredeem`,
            method: "post",
            data: data
        });
        return result;
    };

    static GetTranStockDetail = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/detail`,
            method: "post",
            data : data
        });
        return result;
    };

    static GetLogTranStock = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `transtock/log`,
            method: "post",
            data: data
        });
        return result;
    };

    static onLogExport = async (data) => {
      await setHeaderAuth();
      const result = await axios({
        url: `transtock/logexport`,
        method: "post",
        data: data
      });
      return result;
    };

    static onExport = async (data) => {
      await setHeaderAuth();
      const result = await axios({
        url: `transtock/export`,
        method: "post",
        data: data
      });
      return result;
    };
}

export default ApiTranStock;