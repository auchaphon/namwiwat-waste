import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterPlayername extends Component {
  static get = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_playername/get`,
      method: "post",
      data: data,
    });
    return result;
  };
  static save = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_playername",
      method: data.id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };
  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_playername",
      method: "delete",
      data: data,
    });
    return result;
  };
}
