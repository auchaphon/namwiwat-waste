import React from "react";

const User = React.lazy(() => import("./views/user"));
const Mt_tag = React.lazy(() => import("./views/mt_tag"));
const Mt_playername = React.lazy(() => import("./views/mt_playername"));
const Mt_arm = React.lazy(() => import("./views/mt_arm"));
const Mt_category = React.lazy(() => import("./views/mt_category"));
const Mt_banner = React.lazy(() => import("./views/masterBanner"));
const UserDetail = React.lazy(() => import("./views/user/detail"));
const EmailTemplate = React.lazy(() => import("./views/emailTemplate"));
const EmailTemplateDetail = React.lazy(() =>
  import("./views/emailTemplate/detail")
);
const LogTransactionFont = React.lazy(() =>
  import("./views/logTransactionFont/LogTransactionFont")
);
const LogTransactionBack = React.lazy(() =>
  import("./views/logTransactionBack/LogTransactionBack")
);
const TranRedeem = React.lazy(() => import("./views/tranRedeem"));
const MasterRedeem = React.lazy(() => import("./views/masterRedeem"));
const MasterPromotion = React.lazy(() => import("./views/masterPromotion"));
const Mt_product = React.lazy(() => import("./views/mt_product"));
const MasterCampaign = React.lazy(() => import("./views/masterCampaign"));
const MasterRanking = React.lazy(() => import("./views/masterRanking"));

const TranStock = React.lazy(() => import("./views/tranStock"));
const UpdateTranStock = React.lazy(() => import("./views/tranStock/update"));
const TranStockLog = React.lazy(() => import("./views/tranStock/log"));
const mt_customer = React.lazy(() => import("./views/mt_customer"));
const Dashboard = React.lazy(() => import("./views/dashboard"));
const MasterMenu = React.lazy(() => import("./views/masterMenu"));
const LogTransactionRedeem = React.lazy(() =>
  import("./views/logTransactionRedeem")
);
const LogTransactionStock = React.lazy(() =>
  import("./views/logTransactionStock")
);

const Mt_productEdit = React.lazy(() =>
  import("./views/mt_product/ProductEdit")
);
const CustomerEdit = React.lazy(() =>
  import("./views/mt_customer/CustomerEdit")
);
const Invoice = React.lazy(() => import("./views/invoice"));
const InvoiceDetail = React.lazy(() => import("./views/invoice/detail"));
const InvoiceDetailReport = React.lazy(() => import("./views/invoice_report"));
const MasterWebView = React.lazy(() => import("./views/masterWebView"));
const mt_value = React.lazy(() => import("./views/mt_value"));
// const LogTransactionActivity = React.lazy(() => import('./views/logTransactionActivity/LogTransactionActivity'))
// const SearchVerify = React.lazy(() => import('./views/searchVerify/SearchVerify'))
// const Branch = React.lazy(() => import('./views/branch'))
// const Team = React.lazy(() => import('./views/team'))
// const UserTeam = React.lazy(() => import('./views/userTeam'))
// const UserRole = React.lazy(() => import('./views/userRole'))
// const Menu = React.lazy(() => import('./views/menuPermission'))
const Config = React.lazy(() => import("./views/masterConfig"));
// const GroupPsPermission = React.lazy(() => import('./views/groupPsPermission'))
// const VerifyCard = React.lazy(() => import('./views/verifyCard'))
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const CustomerReport = React.lazy(() => import("./views/customer_report"));
const MtFontColour = React.lazy(() => import("./views/mt_fontcolour"));
const routes = [
  { path: "/", exact: true, name: "หน้าหลัก" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/user", name: "ผู้ใช้งาน (Staff)", component: User },
  { path: "/userdetail", name: "ผู้ใช้งาน (Staff)", component: UserDetail },
  { path: "/emailtemplate", name: "Email Template", component: EmailTemplate },
  {
    path: "/emailtemplatedetail",
    name: "Email Template",
    component: EmailTemplateDetail,
  },
  {
    path: "/logtransaction/fontend",
    name: "รายการพฤติกรรมลูกค้า",
    component: LogTransactionFont,
  },
  {
    path: "/logtransaction/backend",
    name: "พนักงาน",
    component: LogTransactionBack,
  },
  { path: "/tranredeem", name: "การแลกของรางวัล", component: TranRedeem },
  { path: "/mtredeem", name: "ของรางวัล", component: MasterRedeem },
  {
    path: "/mtpromotion",
    name: "โปรโมชัน",
    component: MasterPromotion,
  },
  { path: "/transtock", name: "คลังสินค้า", component: TranStock },
  {
    path: "/updatetranstock",
    name: "แก้ไขข้อมูล คลังสินค้า",
    component: UpdateTranStock,
  },
  {
    path: "/transtocklog",
    name: "ประวัติสต็อกสินค้า",
    component: TranStockLog,
  },
  { path: "/mtcampaign", name: "แคมเปญ", component: MasterCampaign },
  // { path: '/logtransaction/activitylog', name: 'Activity Log', component: LogTransactionActivity },
  // { path: '/searchverify', name: 'ค้นหารายการยืนยันตัวตน', component: SearchVerify },
  { path: "/setting", name: "การตั้งค่าระบบ", component: Config },
  // { path: '/branch', name: 'ฐานข้อมูลสาขา / ทีม', component: Branch },
  // { path: '/team', name: 'ฐานข้อมูล ทีม', component: Team },
  // { path: '/user_team', name: 'ฐานข้อมูล ผู้ร่วมทีม', component: UserTeam },
  // { path: '/user_role', name: 'สิทธิการเข้าถึง ผู้ใช้งาน', component: UserRole },
  // { path: '/menu', name: 'สิทธิ์การเข้าถึงเมนู', component: Menu },
  // { path: '/group_permission', name: 'ตั้งค่ากลุ่ม/ สิทธิ์ข้อมูลตัวตน', component: GroupPsPermission },
  // { path: '/verify_card', name: 'ตรวจสอบสถานะบัตรประชาชน', component: VerifyCard },
  { path: "/MtTag", name: "แท็ก", component: Mt_tag },
  { path: "/Mt_playername", name: "ชื่อผู้เล่น", component: Mt_playername },
  { path: "/Mt_arm", name: "อาร์ม", component: Mt_arm },
  { path: "/mtfontcolour", name: "สีตัวอักษร", component: MtFontColour },
  {
    path: "/MtCategory",
    name: "หมวดหมู่สินค้า",
    component: Mt_category,
  },
  {
    path: "/Mt_menu",
    name: "สิทธิ์การใช้งาน​",
    component: MasterMenu,
  },

  { path: "/Mtbanner", name: "แบนเนอร์", component: Mt_banner },
  { path: "/Mt_product", name: "สินค้า", component: Mt_product },
  {
    path: "/customer_level",
    name: "ระดับสมาชิก",
    component: MasterRanking,
  },
  {
    path: "/Mt_productEdit",
    name: "สินค้า",
    component: Mt_productEdit,
  },
  {
    path: "/mt_customer",
    name: "ข้อมูลลูกค้า",
    component: mt_customer,
  },
  {
    path: "/mt_customerEdit",
    name: "ข้อมูลลูกค้า",
    component: CustomerEdit,
  },
  { path: "/invoice", name: "รายการสั่งซื้อ", component: Invoice },
  {
    path: "/invoicedetail",
    name: "รายละเอียด รายการสั่งซื้อ",
    component: InvoiceDetail,
  },
  {
    path: "/invoicedetailreport",
    name: "รายการสั่งซื้อรายสินค้า",
    component: InvoiceDetailReport,
  },
  {
    path: "/logTranRedeem",
    name: "รายการแลกของรางวัล",
    component: LogTransactionRedeem,
  },
  {
    path: "/logTranStock",
    name: "รายการเข้า - ออก คลังสินค้า ",
    component: LogTransactionStock,
  },
  {
    path: "/customer_report",
    name: "Customer Report",
    component: CustomerReport,
  },
  {
    path: "/masterwebview",
    name: "การตั้งค่าเมนู",
    component: MasterWebView,
  },
  {
    path: "/mt_value",
    name: "ไซต์",
    component: mt_value,
  },
];
export default routes;
