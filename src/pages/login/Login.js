import React, { useRef, useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import { Link } from "react-router-dom";
import CIcon from "@coreui/icons-react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import ApiUser from "../../api/ApiUser.js";
import { setLocalStorage } from "../../utils/localStorage.js";
import { useSelector, useDispatch } from "react-redux";

const logo = require("../../images/namwiwat.png");

const Login = () => {
  const history = useHistory();

  const formInput = useRef(null);
  const [error, seterror] = useState();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [new_password, setNewPassword] = useState("");
  const [new_password_rpt, setNewPasswordRpt] = useState("");
  const [typeLogin, setTypeLogin] = useState(true);
  const dispatch = useDispatch();

  const login = async (e) => {
    e.preventDefault();
    const model = {
      username: username,
      password: password,
    };
    Swal.showLoading();
    try {
      const result = await ApiUser.login(model);
      if (result.status == 200) {
        console.log("result.data::", result.data);
        const { user, token } = result.data;
        // console.log("user", user);
        // console.log("token", token);
        // console.log("menu", menu);
        Swal.close();
        dispatch({ type: "set_user", user: user, token: token });
        setLocalStorage("token", token);
        // setLocalStorage('menu', menu);
        // setLocalStorage('role', user.role.role_name);
        history.push("/");
      }
    } catch (error) {
      Swal.close();
      console.log("error::", error);
      const { data } = error.response;
      // console.log("data", data);
      if (data.message) {
        // console.log("data?.message", data?.message);
        seterror(data?.message);
      } else if (data) {
        // console.log("data", data);
        seterror(data);
      }
    }
  };

  // const changePass = async (e) => {
  //   e.preventDefault();

  //   if (new_password != new_password_rpt) {
  //     seterror("รหัสผ่าน กับรหัสผ่านยืนยันไม่ตรงกัน");
  //     return false;
  //   }

  //   const model = {
  //     username: username,
  //     password: password,
  //     new_password: new_password,
  //   };
  //   Swal.showLoading();
  //   try {
  //     const result = await ApiUser.changePass(model);
  //     if (result.status == 200) {
  //       Swal.fire({
  //         icon: "success",
  //         title: "แก้ไขรหัสผ่านสำเร็จ",
  //         timer: 2000,
  //       }).then((success) => {
  //         history.go(0);
  //       });
  //     }
  //   } catch (error) {
  //     Swal.close();
  //     const { data } = error.response;
  //     console.log("data", data);
  //     if (data.message) {
  //       console.log("data?.message", data?.message);
  //       seterror(data?.message);
  //     } else if (data) {
  //       console.log("data", data);
  //       seterror(data);
  //     }
  //   }
  // };

  // const toggleLogin = () => {
  //   setTypeLogin(!typeLogin);
  // };
  return (
    <div className="c-app c-default-layout bg-light flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4 border">
                <CCardBody className="text-center d-flex-center d-flex" style={{ flexDirection: 'column' }}>
                  <img src={logo} className="c-login-brand" />
                  <br />
                  <h5 className="text-primary">Waste Sterile Traceability</h5>
                </CCardBody>
                <CCardBody>
                  <form ref={formInput} onSubmit={login} hidden={!typeLogin}>
                    {/* <p className="text-muted">Sign in to your account</p> */}
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="text"
                        name="username"
                        placeholder="ชื่อผู้ใช้งาน"
                        autoComplete={"off"}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="password"
                        name="password"
                        placeholder="รหัสผ่าน"
                        autoComplete={"off"}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CRow
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <div>
                        <CButton
                          className="px-3 bg-primary text-white"
                          // onClick={login}
                          type="submit"
                          color="bg-blue"
                        >
                          เข้าสู่ระบบ
                        </CButton>
                        {/* <Link to="/home">
                          <CButton color="primary" className="px-4">
                            Login
                          </CButton>
                        </Link> */}
                      </div>
                      {/* <div className="text-right">
                        <CButton
                          className="px-3"
                          onClick={() => toggleLogin()}
                          color="ghost"
                        >
                          เปลี่ยนรหัสผ่าน
                        </CButton>
                      </div> */}
                    </CRow>
                  </form>
                  {/* <form
                    ref={formInput}
                    onSubmit={changePass}
                    hidden={typeLogin}
                  >
                    <h1>Change Password</h1>
                    <p className="text-muted">Change your password</p>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="password"
                        name="password"
                        placeholder="Old Password"
                        autoComplete={"off"}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="password"
                        name="new_password"
                        placeholder="New Password"
                        autoComplete={"off"}
                        onChange={(e) => setNewPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type="password"
                        name="new_password_rpt"
                        placeholder="Repeat Password"
                        autoComplete={"off"}
                        onChange={(e) => setNewPasswordRpt(e.target.value)}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton
                          type="submit"
                          color="warning-custom"
                          className="px-4"
                        >
                          Submit
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton
                          onClick={() => toggleLogin()}
                          className="px-4"
                          color="ghost"
                        >
                          Login
                        </CButton>
                      </CCol>
                    </CRow>
                  </form> */}
                  <label className="text-danger mt-3">{error}</label>
                </CCardBody>
              </CCard>
              {/* <CCard
                className="text-black bg-white py-5 d-md-down-none"
                style={{ width: "34%" }}
              >
                <CCardBody className="text-center d-flex-center d-flex" style={{ flexDirection: 'column' }}>
                  <img src={logo} className="c-login-brand" />
                  <br />
                  <h5 className="text-primary">Waste Sterile Traceability</h5>
                </CCardBody>
              </CCard> */}
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
