import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

// sidebar nav config
import navigation from "./_nav";
import ApiMasterMenu from "../api/ApiMasterMenu";

const logoAsl = require("../images/AslLogo.png");
const TheSidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);
  const [nevItem, setNevItem] = useState([]);
  const userState = useSelector((state) => state.user);
  const menuState = useSelector((state) => state.menu);

  useEffect(() => {
    handlePermission();
    return () => { };
  }, [userState, menuState]);

  useEffect(() => { }, []);

  const handlePermission = async () => {
    if (userState) {
      try {
        var model = {
          is_superadmin: userState.is_superadmin,
          is_admin: userState.is_admin,
          is_operator: userState.is_operator,
        };
        const result = await ApiMasterMenu.findByrole(model);
        if (result.status === 200) {
          const { data } = result.data;
          func_genaretemenu(data);
        }
      } catch (error) { }
    }
  };

  const func_genaretemenu = (datamenu) => {
    var menulv1 = datamenu.filter((x) => x.level == 1);
    var menulv2 = datamenu.filter((x) => x.level == 2);
    var menulv3 = datamenu.filter((x) => x.level == 3);

    var nav = [];
    menulv1.forEach((data1, index) => {
      var child1 = menulv2.filter((x) => x.parent == data1.id);

      if (child1.length > 0) {
        var setmodel = {
          _tag: "CSidebarNavTitle",
          _children: [data1.menu_name],
          icon: data1.icon,
        };
        nav.push(setmodel);

        child1.forEach((data2, index) => {
          var child2 = menulv3.filter((x) => x.parent == data2.id);

          if (child2.length > 0) {
            var setmodel2 = {
              _tag: "CSidebarNavDropdown",
              name: data2.menu_name,
              icon: data2.icon,
              _children: [],
            };

            child2.forEach((data3, index) => {
              var setmodel3 = {
                _tag: "CSidebarNavItem",
                name: data3.menu_name,
                to: data3.link,
                icon: data3.icon,
              };
              setmodel2._children.push(setmodel3);
            });

            nav.push(setmodel2);
          } else {
            var setmodel2 = {
              _tag: "CSidebarNavItem",
              name: data2.menu_name,
              icon: data2.icon,
              to: data2.link,
            };

            nav.push(setmodel2);
          }
        });
      } else {
        var setmodel = {
          _tag: "CSidebarNavItem",
          name: data1.menu_name,
          to: data1.link,
          icon: data1.icon,
        };

        nav.push(setmodel);
      }
    });
    console.log('nav::', JSON.stringify(nav))
    setNevItem(nav);
  };

  return (
    <CSidebar
      colorScheme="custom"
      show={show}
      foldable
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <div>
          <img width="40" src={logoAsl}/>
        </div> */}
        <div>Back End</div>
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
