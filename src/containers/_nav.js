import React from "react";
const file_spreadsheet_duotone = require("./../assets/icons/file-spreadsheet-duotone.svg");
const user_duo = require("./../assets/icons/user-duotone-5.svg");
const dashboard = require("./../assets/icons/dashboard.svg");

export default [
  {
    "_tag": "CSidebarNavItem",
    "name": "Dashboard",
    "to": "/dashboard",
    "icon": "cil-speedometer"
  },
  {
    "_tag": "CSidebarNavTitle",
    "_children": [
      "ข้อมูลผู้ใช้งาน (Staff)"
    ],
    "icon": null
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ผู้ใช้งาน (Staff)",
    "icon": "cil-user",
    "to": "/user"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "สิทธิ์การใช้งาน​",
    "icon": "cil-settings",
    "to": "/Mt_menu"
  },
  {
    "_tag": "CSidebarNavTitle",
    "_children": [
      "ข้อมูลผู้ใช้งาน (ลูกค้า)"
    ],
    "icon": null
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ข้อมูลลูกค้า",
    "icon": "cil-user",
    "to": "/mt_customer"
  },
  {
    "_tag": "CSidebarNavTitle",
    "_children": [
      "Process การทำงาน"
    ],
    "icon": null
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "รับเข้าระบบ (Reception)",
    "icon": "cil-list",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "กระบวนการฆ่าเชื้อ (Sterilizer)",
    "icon": "cil-list",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "กระบวนการส่งออก (Export)",
    "icon": "cil-list",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "รับเข้าจุดพักขยะที่ผ่านการฆ่าเชื้อ",
    "icon": "cil-list",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "จัดส่ง (Delivery)",
    "icon": "cil-list",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavTitle",
    "_children": [
      "ข้อมูลระบบ"
    ],
    "icon": null
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "เครื่อง Sterile",
    "icon": "cil-settings",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ถาด (Container Tray)",
    "icon": "cil-settings",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavTitle",
    "_children": [
      "รายงาน"
    ],
    "icon": null
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ข้อมูลขยะรับเข้า",
    "icon": "cil-copy",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ข้อมูลส่งออกขยะ",
    "icon": "cil-copy",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "รอบการฆ่าเชื้อ",
    "icon": "cil-copy",
    "to": "/"
  },
  {
    "_tag": "CSidebarNavItem",
    "name": "ผู้ปฏิบัติงาน",
    "icon": "cil-copy",
    "to": "/"
  },
]