import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  CButton,
  CCol,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useHistory, useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import { setLocalStorage } from "../utils/localStorage.js";
import ApiUser from "../api/ApiUser.js";

const TheHeaderDropdown = (props) => {
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.user);
  const history = useHistory();
  const [role, setRole] = useState("");
  const [modalChangePass, setModalChangePass] = useState(false);
  const [oldPass, setOldPass] = useState("");
  const [newPass, setnewPass] = useState("");
  const [confirmPass, setConfirmPass] = useState("");

  const toggleChangePass = () => {
    setModalChangePass(!modalChangePass)
  }
  useEffect(() => {
    // console.log("userState::", userState);
    console.log(userState);
    var rolearr = [];
    if (userState.is_superadmin == 1) {
      rolearr.push("Super Admin");
    }

    if (userState.is_admin == 1) {
      rolearr.push("Admin");
    }

    if (userState.is_operator == 1) {
      rolearr.push("Operator");
    }

    var roletext = "";
    for (var i = 0; i < rolearr.length; i++) {
      if (i === rolearr.length - 1) {
        roletext += rolearr[i];
      } else {
        roletext += rolearr[i] + ", ";
      }
    }

    setRole(`( ${roletext} )`);
    return () => {};
  }, [userState]);

  const changeRole = (item) => {
    Swal.fire({
      title: "",
      text: `Change Role To ${item.role_name}`,
      icon: "question",
      showCancelButton: true,
      cancelButtonText: "Cancel",
      confirmButtonText: "Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        const user = { ...userState };
        user.role = item;
        setLocalStorage("role", item.role_name);
        dispatch({ type: "set_user", user: user });
        setTimeout(() => {
          history.push("/");
          history.go(0);
        }, 500);
      }
    });
  };

  const logout = () => {
    history.push("/login");
    setTimeout(() => {
      dispatch({ type: "set_user", user: {} });
    }, 300);
  };

  const onSubmitChangePass = async (e) => {
    e.preventDefault();

    if (newPass != confirmPass) {
      Swal.fire({
          icon: "error",
          title: "รหัสผ่านใหม่ กับรหัสผ่านยืนยันไม่ตรงกัน",
      });
      return false;
    }

    const model = {
      username: userState.username,
      password: oldPass,
      new_password: newPass,
    };
    Swal.showLoading();
    try {
      const result = await ApiUser.changePass(model);
      if (result.status == 200) {
        Swal.fire({
          icon: "success",
          title: "แก้ไขรหัสผ่านสำเร็จ",
          timer: 2000,
        }).then((success) => {
          history.go(0);
        });
      }
    } catch (error) {
      Swal.close();
      const { data } = error.response;
      console.log("data", data);
      if (data.message) {
        console.log("data?.message", data?.message);
        Swal.fire({
            icon: "error",
            title: data?.message,
        });
      } else if (data) {
        console.log("data", data);
        Swal.fire({
            icon: "error",
            title: data,
        });
      }
    }
  }

  return (
    <>
      <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <span className="mr-2">{`${userState.userfullname} : ${role}`}</span>
        </CDropdownToggle>
        <CDropdownMenu className="pt-0" placement="bottom-end">
          <CDropdownItem header tag="div" color="light" className="text-center">
            <strong>ชื่อผู้ใช้งาน : {userState.username}</strong>
          </CDropdownItem>
          {userState.roles
            ? userState.roles.map((item, index) => (
                <CDropdownItem
                  onClick={() =>
                    item.role_code == userState.role.role_code
                      ? null
                      : changeRole(item)
                  }
                >
                  {item.role_code == userState.role.role_code && (
                    <CIcon
                      style={{ color: "#8CC63F" }}
                      color="success-custom"
                      name="cil-check-alt"
                      className="mfe-2"
                    />
                  )}
                  {item.role_name}
                </CDropdownItem>
              ))
            : ""}
          <CDropdownItem divider />
          <CDropdownItem onClick={() => toggleChangePass()}>
            <CIcon name="cil-lock-locked" className="mfe-2" />
            เปลี่ยนรหัสผ่าน
          </CDropdownItem>
          <CDropdownItem divider />
          <CDropdownItem onClick={() => logout()}>
            <CIcon name="cil-lock-locked" className="mfe-2" />
            ออกจากระบบ
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
      <CModal
          show={modalChangePass}
          onClose={toggleChangePass}
          // style={{ width: '133%' }}
      >
          <CModalHeader closeButton>
              <CModalTitle>เพิ่มข้อมูล</CModalTitle>
          </CModalHeader>
          <CForm onSubmit={(e) =>onSubmitChangePass(e)} action="" >
              <CModalBody>
                  <CRow>
                      <CCol xs="12" sm="12">
                          <CRow>
                              <CCol xs="12">
                                  <CFormGroup>
                                      <CLabel htmlFor="oldPass"> รหัสผ่านเก่า <span style={{ color: 'red' }}>*</span></CLabel>
                                      <CInput onChange={e => setOldPass(e.target.value)} value={oldPass} id="oldPass" required />
                                  </CFormGroup>
                              </CCol>
                          </CRow>
                          <CRow>
                              <CCol xs="12">
                                  <CFormGroup>
                                      <CLabel htmlFor="newPass"> รหัสผ่านใหม่ <span style={{ color: 'red' }}>*</span></CLabel>
                                      <CInput onChange={e => setnewPass(e.target.value)} value={newPass} id="newPass" required />
                                  </CFormGroup>
                              </CCol>
                          </CRow>
                          <CRow>
                              <CCol xs="12">
                                  <CFormGroup>
                                      <CLabel htmlFor="confirmPass"> ยืนยันรหัสผ่าน <span style={{ color: 'red' }}>*</span></CLabel>
                                      <CInput onChange={e => setConfirmPass(e.target.value)} value={confirmPass} id="confirmPass" required />
                                  </CFormGroup >
                              </CCol>
                          </CRow>
                      </CCol>
                  </CRow >
              </CModalBody>
              <CModalFooter>
                  <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                  <CButton
                      color="secondary"
                      onClick={toggleChangePass}
                  >ยกเลิก</CButton>
              </CModalFooter>
          </CForm >
      </CModal >
    </>
  );
};

export default TheHeaderDropdown;
