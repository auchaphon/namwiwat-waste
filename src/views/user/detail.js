import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiUser from "../../api/ApiUser";
import Swal from "sweetalert2";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Details = (props, { data }) => {
  var id = new URLSearchParams(props.location.search).get("id");
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [userfullname, setUserfullname] = useState("");
  const [useremail, setUserEmail] = useState("");
  const [empId, setEmpId] = useState("");
  const [is_lock, setIsLock] = useState(false);
  const [role, setRole] = useState("opt");
  const [record_status, set_record_status] = useState("A");

  const [isAdmin, setIsAdmin] = useState(false);
  const [isOperator, setIsOperator] = useState(false);
  const [isSuperAdmin, setIsSuperAdmin] = useState(false);

  const [isPassword, setIsPassword] = useState(true);

  useEffect(() => {
    if (id != 0) {
      getDetail();
    }

    return () => { };
  }, []);

  const func_add_log = async (detail, json) => {

  };

  const getDetail = async () => {
    try {
      var result = await ApiUser.getDetail(id);
      //   var result;
      //   if (userState.role.role_code == "admin"){
      //     result = await ApiUser.get();
      //   }else if (userState.role.role_code == "user_branch"){
      //     result = await ApiUser.getByBranch(userState.branch_id);
      //   }else if (userState.role.role_code == "user_team" || userState.role.role_code == "user"){
      //     result = await ApiUser.getByTeam(userState.team_id);
      //   }
      if (result.status === 200) {
        const { data } = result.data;
        // setIsAdmin(data.is_admin == 1 ? true : false);
        // setIsOperator(data.is_operator == 1 ? true : false);
        // setIsSuperAdmin(data.is_superadmin == 1 ? true : false);

        setUsername(data.username);
        setPassword("");
        setUserfullname(data.userfullname);
        setUserEmail(data.useremail);
        setEmpId(data.emp_id);
        setIsLock(data.is_locked == 1);
        var role = "opt";
        if (data.is_operator == 1) {
          role = "opt";
        } else if (data.is_superadmin == 1) {
          role = "sup";
        } else if (data.is_admin == 1) {
          role = "adm";
        }
        setRole(role);
        set_record_status(data.record_status);
      }
    } catch (error) { }
  };

  const onSubmit = (e) => {
    e.preventDefault();

  };

  const saveData = async (data) => {

  };

  const onBack = () => {
    history.push(`/user`);
    history.go(0);
    // toggle();
    // setDataSelected('');
    // setUsername('');
    // setPassword('');
    // setUserfullname('');
    // setUserEmail('');
    // setEmpId('');
    // setIsLock(false);
  };
  return (
    <>
      <CCard>
        {/* <CCardHeader>
                    <b>ข้อมูลผู้ใช้</b>
                </CCardHeader> */}
        <CForm onSubmit={onSubmit} action="">
          <CCardBody>
            <CRow className="card-section-header">
              <b>ข้อมูลผู้ใช้งาน</b>
            </CRow>
            <CRow className="mt-4">
              <CCol xs="4">
                <CFormGroup>
                  <CLabel htmlFor="username">
                    รหัสผู้ใช้งาน <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="username"
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    required
                    readOnly={id != 0}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="4">
                <CFormGroup>
                  <CLabel htmlFor="password">
                    รหัสผ่าน <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <div>
                    <CInput
                      type={isPassword ? "password" : "text"}
                      id="password"
                      onChange={(e) => setPassword(e.target.value)}
                      value={password}
                      required={id == 0}
                    />
                    <span
                      className="c-changetype"
                      onClick={() => {
                        setIsPassword(!isPassword);
                      }}
                    >
                      {isPassword ? (
                        <FontAwesomeIcon icon={faEye} />
                      ) : (
                        <FontAwesomeIcon icon={faEyeSlash} />
                      )}
                    </span>
                  </div>
                </CFormGroup>
              </CCol>
              <CCol xs="4">
                <CFormGroup>
                  <CLabel htmlFor="empId">
                    รหัสพนักงาน <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="empId"
                    onChange={(e) => setEmpId(e.target.value)}
                    value={empId}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="4">
                <CFormGroup>
                  <CLabel htmlFor="userfullname">
                    ชื่อ - นามสกุล <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="userfullname"
                    onChange={(e) => setUserfullname(e.target.value)}
                    value={userfullname}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="4">
                <CFormGroup>
                  <CLabel htmlFor="useremail">
                    อีเมล <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    type="email"
                    id="useremail"
                    onChange={(e) => setUserEmail(e.target.value)}
                    value={useremail}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="4" hidden={id == 0}>
                <CFormGroup>
                  <CLabel htmlFor="role" className="col-12 row">
                    สิทธิ์การใช้งาน
                  </CLabel>
                  <CSelect
                    id="role"
                    onChange={(e) => setRole(e.target.value)}
                    value={role}
                    required
                  >
                    <option value="opt">Operator</option>
                    <option value="sup">Super Admin</option>
                    <option value="adm">Admin</option>
                  </CSelect>
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="2" hidden={id == 0}>
                <CFormGroup>
                  <CLabel htmlFor="is_lock" className="col-12 row">
                    Lock
                  </CLabel>
                  <CSwitch
                    id="is_lock"
                    className={"mx-1"}
                    shape={"pill"}
                    color={"primary"}
                    variant="outline"
                    labelOn={"\u2713"}
                    labelOff={"\u2715"}
                    checked={is_lock}
                    onChange={(e) => setIsLock(!is_lock)}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="2" hidden={id == 0}>
                <CFormGroup>
                  <CLabel htmlFor="record_status" className="col-12 row">
                    สถานะ
                  </CLabel>
                  <CSwitch
                    id="record_status"
                    className={"mx-1"}
                    shape={"pill"}
                    color={"primary"}
                    // variant="outline"
                    // labelOn={"\u2713"}
                    // labelOff={"\u2715"}
                    checked={record_status == "A"}
                    onChange={(e) => set_record_status(e.target.checked ? "A" : "I")}
                  />
                </CFormGroup>
              </CCol>
              <CCol sm={id == 0 ? "10" : "6"}></CCol>
              <CCol sm="1" className="mt-4">
                <CButton
                  type="submit"
                  color="primary-custom"
                  variant="reverse"
                  block
                >
                  บันทึก
                </CButton>{" "}
              </CCol>
              <CCol sm="1" className="mt-4">
                <CButton
                  color="danger-custom"
                  variant="reverse"
                  block
                  onClick={onBack}
                >
                  ยกเลิก
                </CButton>
              </CCol>
            </CRow>
          </CCardBody>
        </CForm>
      </CCard>
    </>
  );
};
export default Details;
