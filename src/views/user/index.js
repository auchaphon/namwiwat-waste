import React, { useEffect, useState } from "react";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCardFooter,
    CCol,
    CRow,
    CButton,
    CLabel,
    CInput,
    CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiUser from "../../api/ApiUser";
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from "../../api/ApiTranLogBack";

const User = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [empid, setEmpId] = useState("");
    const [username, setUsername] = useState("");
    const [userfullname, setUserfullname] = useState("");
    const [email, setEmail] = useState("");
    const [record_status, set_record_status] = useState("A");
    const userState = useSelector((state) => state.user);

    useEffect(() => {
        getData();
        return () => { };
    }, []);

    const func_add_log = async (detail, json) => {
    };

    // useEffect(() => {
    //     getData();
    //     return () => {
    //     }
    // }, [empid, username, userfullname, email]);

    const getData = async () => {
        try {
            var model = {
                empid: empid,
                username: username,
                userfullname: userfullname,
                email: email,
                record_status: record_status
            }
            //var result = await ApiUser.get(model);
            const data = [
                {
                    "id": 1,
                    "active_date": null,
                    "emp_id": "57000",
                    "ip_addr": null,
                    "is_locked": 0,
                    "last_attempt_date": null,
                    "last_locked_date": null,
                    "last_password_date": "2022-01-16T10:34:20.000Z",
                    "password": "PXElJJHaaH/1LWI1SP+x3hBByF1jNnOuLEmG9CMAA7E=",
                    "password_expire_date": "2022-04-16T10:34:20.000Z",
                    "unsuccess": 0,
                    "useremail": "admin@admin.com",
                    "userfullname": "Admin",
                    "username": "adminn",
                    "is_superadmin": 1,
                    "is_admin": 1,
                    "is_operator": 1,
                    "record_status": "A",
                    "createdate": "2021-06-16T00:57:36.000Z",
                    "updatedate": "2022-01-31T17:44:19.000Z",
                    "updateby": "Admin",
                    "createby": null
                },
                {
                    "id": 2,
                    "active_date": null,
                    "emp_id": "57001",
                    "ip_addr": null,
                    "is_locked": 0,
                    "last_attempt_date": null,
                    "last_locked_date": null,
                    "last_password_date": "2022-01-16T10:34:20.000Z",
                    "password": "PXElJJHaaH/1LWI1SP+x3hBByF1jNnOuLEmG9CMAA7E=",
                    "password_expire_date": "2022-04-16T10:34:20.000Z",
                    "unsuccess": 0,
                    "useremail": "auchaphon@appcreation-sw.com",
                    "userfullname": "Auchaphon",
                    "username": "Auchaphon",
                    "is_superadmin": 1,
                    "is_admin": 0,
                    "is_operator": 0,
                    "record_status": "A",
                    "createdate": "2021-06-16T00:57:36.000Z",
                    "updatedate": "2022-01-31T17:44:19.000Z",
                    "updateby": "Admin",
                    "createby": null
                }
            ]
            setRawDataTable(data);
            var res = data.sort((a, b) => a.createdate > b.createdate ? -1 : 1);
            for (let i = 0; i < res.length; i++) {
                res[i].order = i + 1
                if (res[i].userfullname == null) {
                    res[i].userfullname = "ไม่ระบุ"
                }
                if (res[i].useremail == null) {
                    res[i].useremail = "ไม่ระบุ"
                }
                if (res[i].group_name == null) {
                    res[i].group_name = "ไม่ระบุ"
                }
                if (res[i].team_name == null) {
                    res[i].team_name = "ไม่ระบุ"
                }
                if (res[i].branch_name == null) {
                    res[i].branch_name = "ไม่ระบุ"
                }
            }
            setDataTable(res);
        } catch (error) {

        }
    }
    const onSearch = async () => {
        // var model = {
        //     activity_detail: "Menu User Search",
        //     json: JSON.stringify({
        //         empid: empid,
        //         username: username,
        //         userfullname: userfullname,
        //         email: email,
        //     })
        // }
        // await ApiTranLogBack.addlog(model);
        getData();
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (empid && empid.length > 0) {
        //         result = result.filter((x) => x.emp_id.toLowerCase().includes(empid.toLowerCase()));
        //     }
        //     if (username && username.length > 0) {
        //         result = result.filter((x) => x.username.toLowerCase().includes(username.toLowerCase()));
        //     }
        //     if (userfullname && userfullname.length > 0) {
        //         result = result.filter((x) => x.userfullname.toLowerCase().includes(userfullname.toLowerCase()));
        //     }
        //     if (email && email.length > 0) {
        //         result = result.filter((x) => x.useremail.toLowerCase().includes(email.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setEmpId("");
        setUsername("");
        setUserfullname("");
        setEmail("");
        set_record_status("A");
        // getData();
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>รหัสพนักงาน</CLabel>
                                    <CInput onChange={e => setEmpId(e.target.value)} value={empid} id="empid" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>รหัสผู้ใช้งาน</CLabel>
                                    <CInput onChange={e => setUsername(e.target.value)} value={username} id="username" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>ชื่อ-นามสกุล</CLabel>
                                    <CInput onChange={e => setUserfullname(e.target.value)} value={userfullname} id="userfullname" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>อีเมลพนักงาน</CLabel>
                                    <CInput onChange={e => setEmail(e.target.value)} value={email} id="email" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                    </CSelect>
                                </CCol>
                                <CCol sm="2" className="pt-4">
                                    <CRow>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b>ข้อมูลผู้ใช้งาน</b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables
                                refreshData={getData}
                                data={dataTable}
                                func_add_log={func_add_log}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default User;
