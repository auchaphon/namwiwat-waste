import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiUser from "../../api/ApiUser";
import Swal from "sweetalert2";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => { }, func_add_log }) => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalConfirm, setModalConfirm] = useState(false);
  const [dataSelected, setDataSelected] = useState("");
  const userState = useSelector((state) => state.user);
  const [username, setUsername] = useState("");
  // const [edit_icon_icon, setEditIcon] = useState(edit_icon);

  useEffect(() => {
    return () => { };
  }, []);

  const toggle = () => {
    setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%" } },
    {
      key: "option",
      label: "",
      _style: { width: "1%" },
      filter: false,
    },
    {
      key: "record_status",
      label: "สถานะ",
      _style: { width: "1%", textAlign: "center" },
    },
    {
      key: "is_superadmin",
      label: "Operation",
      _style: { width: "1%", textAlign: "center" },
    },
    {
      key: "is_admin",
      label: "Guest",
      _style: { width: "1%", textAlign: "center" },
    },
    {
      key: "is_operator",
      label: "Admin",
      _style: { width: "1%", textAlign: "center" },
    },
    {
      key: "emp_id",
      label: "รหัสพนักงาน",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    {
      key: "username",
      label: "รหัสผู้ใช้งาน",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    {
      key: "userfullname",
      label: "ชื่อ - นามสกุล",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    {
      key: "useremail",
      label: "อีเมลพนักงาน",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    // {
    //   key: "userroles",
    //   label: "สิทธิ์การใช้งาน",
    //   _style: { minWidth: "150px", textAlign: "center" },
    // },
    // { key: "branch_name", label: "สาขา" , _style: { minWidth: "150px" }},
    // { key: "team_name", label: "ทีม" , _style: { minWidth: "150px" }},
    // { key: "group_name", label: "กลุ่ม/ สิทธิ์ข้อมูลตัวตน" , _style: { minWidth: "200px" }},
    {
      key: "createdate",
      label: "วันที่สร้างข้อมูลล่าสุด",
      _style: { minWidth: "200px", textAlign: "center" },
    },
    {
      key: "createby",
      label: "ผู้สร้างข้อมูล",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    {
      key: "updatedate",
      label: "วันที่แก้ไขข้อมูลล่าสุด",
      _style: { minWidth: "200px", textAlign: "center" },
    },
    {
      key: "updateby",
      label: "ผู้แก้ไขข้อมูล",
      _style: { minWidth: "150px", textAlign: "center" },
    },
  ];

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  };

  const newDetails = (record) => {
    history.push(`/userdetail?id=0`);
    history.go(0);
    // toggle();
    // setDataSelected('');
    // setUsername('');
    // setPassword('');
    // setUserfullname('');
    // setUserEmail('');
    // setEmpId('');
    // setIsLock(false);
  };

  const deleteDetails = (record) => {
    toggleConfirm();
    setDataSelected(record);
    setUsername(record.username);
  };

  const editDetails = (record) => {
    history.push(`/userdetail?id=${record.id}`);
    history.go(0);
    // toggle();
    //   setDataSelected(record);
    // setUsername(record.username);
    // setPassword("");
    // setUserfullname(record.userfullname);
    // setUserEmail(record.useremail);
    // setEmpId(record.emp_id);
    // setIsLock(record.is_locked == 1);
  };

  // const onSubmit = (e) => {
  //     e.preventDefault();
  //     const model = {
  //         "id": dataSelected.id,
  //         "username": username,
  //         "password": password,
  //         "userfullname": userfullname,
  //         "useremail": useremail,
  //         "emp_id": empId,
  //         "is_locked": is_lock ? 1 : 0,
  //     }
  //     saveData(model);
  // }

  const func_changeRole = async (e, modules, id) => {

  };

  const onDelete = async () => {
  };

  const func_change_status = async (e, record) => {

  };

  const saveData = async (data) => {
  }

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => newDetails()}
            color="primary-custom"
            variant="reverse"
          >
            <CIcon name="cil-plus" />
            <span className="ml-2">
              <b>สร้างผู้ใช้งาน</b>
            </span>
          </CButton>
        </CCol>
      </CRow>
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          // 'username':
          //     (item) => (
          //         <td>
          //             <CLink href={`/#/user_role?id=${item.id}&user=${item.username}`}>{item.username}</CLink>
          //         </td>
          //     ),
          updatedate: (item) => (
            <td className="text-center">
              {moment(item.updatedate)
                // .add(543, "years")
                .format("DD-MM-YYYY HH:mm")}
            </td>
          ),
          createdate: (item) => (
            <td className="text-center">
              {moment(item.createdate)
                // .add(543, "years")
                .format("DD-MM-YYYY HH:mm")}
            </td>
          ),
          createby: (item) => <td>{item.createby ? item.createby : ""}</td>,
          updateby: (item) => <td>{item.updateby ? item.updateby : ""}</td>,
          option: (item, index) => (
            <td className="center">
              <CButtonGroup>
                <CButton
                  color="primary-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    editDetails(item);
                  }}
                  onMouseOver={(e) =>
                  (document.getElementById(`edit-${item.id}`).src =
                    edit_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`edit-${item.id}`).src = edit_icon)
                  }
                >
                  <img id={`edit-${item.id}`} src={edit_icon} />
                </CButton>
                {/* <CButton variant="ghost" size="sm" /> */}
                {/* <CButton
                  className="ml-3"
                  color="danger-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    deleteDetails(item);
                  }}
                  onMouseOver={(e) =>
                    (document.getElementById(`delete-${item.id}`).src =
                      trash_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`delete-${item.id}`).src =
                      trash_icon)
                  }
                >
                  <img id={`delete-${item.id}`} src={trash_icon} />
                </CButton> */}
              </CButtonGroup>
            </td>
          ),
          record_status: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.record_status == "A" ? true : false}
                onChange={(e) =>
                  func_change_status(e.target.checked, item)
                }
              />
            </td>
          ),
          is_superadmin: (item, index) => (
            <td align="center">
              <CSwitch
                type="radio"
                name={`edit-role-${item.id}`}
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.is_superadmin == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "superadmin", item.id)
                }
              />
            </td>
          ),
          is_admin: (item, index) => (
            <td align="center">
              <CSwitch
                type="radio"
                name={`edit-role-${item.id}`}
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.is_admin == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "admin", item.id)
                }
              />
            </td>
          ),
          is_operator: (item, index) => (
            <td align="center">
              <CSwitch
                type="radio"
                name={`edit-role-${item.id}`}
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.is_operator == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "operator", item.id)
                }
              />
            </td>
          ),
        }}
      />
      <CModal
        show={modalConfirm}
        onClose={setModalConfirm}
        color="danger-custom"
      >
        <CModalHeader closeButton>
          <CModalTitle>ลบรายการ : {username}</CModalTitle>
        </CModalHeader>
        <CModalBody>คุณต้องการลบรายการนี้ ?</CModalBody>
        <CModalFooter>
          <CButton onClick={onDelete} color="primary">
            ยืนยัน
          </CButton>{" "}
          <CButton color="secondary" onClick={() => setModalConfirm(false)}>
            ยกเลิก
          </CButton>
        </CModalFooter>
      </CModal>

      {/* <CModal size="lg" show={modal} onClose={toggle}>
          <CModalHeader closeButton>
            <CModalTitle>รายละเอียดผู้ใช้งาน</CModalTitle>
          </CModalHeader>
          <CForm onSubmit={onSubmit} action="" >
            <CModalBody>
              <CRow>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="username">รหัสผู้ใช้งาน <span style={{color:'red'}}>*</span></CLabel>
                    <CInput id="username" onChange={e => setUsername(e.target.value)} value={username} required readOnly={dataSelected.id} />
                  </CFormGroup>
                </CCol>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="password">รหัสผ่าน <span style={{color:'red'}}>*</span></CLabel>
                    <CInput type="password" id="password" onChange={e => setPassword(e.target.value)} value={password} required={!dataSelected.id} />
                  </CFormGroup>
                </CCol>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="empId">รหัสพนักงาน <span style={{color:'red'}}>*</span></CLabel>
                    <CInput id="empId" onChange={e => setEmpId(e.target.value)} value={empId} required  />
                  </CFormGroup>
                </CCol>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="userfullname">ชื่อ - นามสกุล <span style={{color:'red'}}>*</span></CLabel>
                    <CInput id="userfullname" onChange={e => setUserfullname(e.target.value)} value={userfullname} required />
                  </CFormGroup>
                </CCol>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="useremail">อีเมล <span style={{color:'red'}}>*</span></CLabel>
                    <CInput
                      type="email"
                      id="useremail"
                      onChange={e => setUserEmail(e.target.value)} value={useremail}
                      required
                    />
                  </CFormGroup>
                </CCol>
                <CCol xs="6" hidden={!dataSelected.id}>
                  <CFormGroup>
                      <CLabel htmlFor="is_lock" className="col-12 row">Lock</CLabel>
                      <CSwitch id="is_lock" className={'mx-1'} shape={'pill'} color={'primary'} variant="outline" labelOn={'\u2713'} labelOff={'\u2715'} checked={is_lock} onChange={(e) => setIsLock(!is_lock)}  />
                  </CFormGroup>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
                <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                <CButton
                    color="secondary"
                    onClick={toggle}
                >ยกเลิก</CButton>
            </CModalFooter>
          </CForm>
        </CModal> */}
    </>
  );
};

export default Tables;
