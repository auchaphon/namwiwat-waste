import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterTag from '../../api/ApiMasterTag';
import Swal from "sweetalert2";
import moment from 'moment';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ApiMasterCategory from "../../api/ApiMasterCategory";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => { }, func_add_log }) => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalConfirmDel, setModalConfirmDel] = useState(false);
  const [modalConfirmEdit, setModalConfirmEdit] = useState(false);
  const [dataSelected, setDataSelected] = useState('');
  const userState = useSelector((state) => state.user);
  const [username, setUsername] = useState('');
  const [categoryId, setCategoryId] = useState('');
  const [description, setDescription] = useState('');
  const [description_en, setDescription_en] = useState('');
  const [name, setName] = useState('');
  const [name_en, setName_en] = useState('');
  const [parent, setParent] = useState('0');
  const [checkBox , setCheckBox] = useState(true);
  const [seq, setSeq] = useState('');
  const [dataParent, setDataParent] = useState([]);
  // const [edit_icon_icon, setEditIcon] = useState(edit_icon);

  useEffect(() => {
      func_get_parent();
      return () => {
      }
  }, []);

  const func_get_parent = async () => {
    try {
        var result = await ApiMasterCategory.getddl();
        if (result.status === 200) {
            const { data } = result.data;
            // var res = data.sort((a, b) => a.name > b.name ? 1: -1);
            setDataParent(data);
        }
    } catch (error) {

    }
};


  const toggle = () => {
      setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%" } },
    {
        key: 'option',
        label: '',
        _style: { width: '1%' },
        filter: false,
    },
    {
      key: "record_status",
      label: "สถานะ",
      _style: { minWidth: "150px", textAlign: "center" },
      filter: false,
    },
    { key: "name", label: "ชื่อหมวดหมู่" , _style: { minWidth: "150px", textAlign: "center" }},
    { key: "name_en", label: "ชื่อหมวดหมู่ EN" , _style: { minWidth: "150px", textAlign: "center" }},
    { key: 'seq', label: "ลำดับแสดง", _style: { minWidth: '150px', textAlign: "center" } },
    { key: "description", label: "รายละเอียด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "description_en", label: "รายละเอียด EN", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "new_parent", label: "Parent", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "updatedate", label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "updateby", label: "ผู้แก้ไขข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
  ];

  const toggleConfirmDel = () => {
      setModalConfirmDel(!modalConfirmDel);
  }
  const toggleConfirmEdit = () => {
    setModalConfirmEdit(!modalConfirmEdit);
}

  const EditDetails = (record) => {
    console.log(record)
    record.parent == 0 ? setCheckBox(false) : setCheckBox(true)
    toggleConfirmEdit();
    setDataSelected(record);
    setUsername(record.username);
    setCategoryId(record.id);
    setName(record.name);
    setName_en(record.name_en);
    setDescription(record.description);
    setDescription_en(record.description_en);
    setParent(record.parent)
    setSeq(record.seq)
}
const insertData = (record) => {
    setDataSelected("");
    toggleConfirmEdit();
    setName('');
    setName_en('');
    setCategoryId('');
    setDescription('')
    setDescription_en('')
    setParent('0');
    setSeq('1');
    setCheckBox(true)
}

  const deleteDetails = (record) => {
      toggleConfirmDel();
      setDataSelected(record);
      setUsername(record.username);
      setCategoryId(record.id);
  }



  const onSubmit = (e) => {
    //   if(!parent){
    //     console.log("nope")
    //   }
    //   console.log(parent)
    //   console.log("check" + checkBox)
      e.preventDefault();
      const model = {
            "id": categoryId,
            "name": name,
            "name_en": name_en,
            "description": description,
            "description_en": description_en,
            "seq":seq,
            "createby":username,
            "updateby":username,
            "record_status": 'A',
            // "parent": parent || Number.isNaN(parent) ? parseInt(parent) : 0
            "parent": ((checkBox && parent) || Number.isNaN(parent)) ? parseInt(parent) : 0
            // "parent": parseInt(parent)
      }
      console.log(Number.isNaN(parent))
      saveData(model);
      console.log(model)
  }

  const func_change_status = async (e, record) => {
    record.record_status = e ? "A" : "I"
    if(record.parent == ""){
        record.parent = 0
    }
    delete record.order;
    saveData(record);
  };

  const saveData = async (data) =>{
    try {
    func_add_log("Save Category", JSON.stringify(data))
    const result = await ApiMasterCategory.save(data);
      if (result.status === 200) {
          const { data } = result.data;
          setModalConfirmEdit(false);
          Swal.fire({
              icon: "success",
              title: "บันทึกสำเร็จ",
              timer: 2000,
          }).then((success) => {
              refreshData();
          });
      }
        setName('');
        setName_en('');
        setCategoryId('');
        setDescription('');
        setSeq('1');
        // setCheckBox(false)
  } catch (error) {
      Swal.fire({
          icon: "error",
          title: error.response.data,
      });
  }
  }

  const onDelete = async () => {
      try {
          const model = {
              "id": categoryId,
              "record_status": 'I'
          }
            func_add_log("Delete Category", JSON.stringify(model))
            const result = await ApiMasterCategory.delete(model);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirmDel();
              Swal.fire({
                  icon: "success",
                  title: "ลบสำเร็จ",
                  timer: 2000,
              }).then((success) => {
                  refreshData();
              });
          }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
          });
      }
  }


  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => { insertData()}}
            color="primary-custom"
            variant="reverse"
          >
            <CIcon name="cil-plus" />
            <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
          </CButton>
        </CCol>
      </CRow>
        <CDataTable
            items={data}
            fields={fields}
            tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
            cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
                'order':
                    (item) => (
                        <td style={{ textAlign: 'center' }}>
                            {item.order}
                        </td>
                    ),
                'seq':
                    (item) => (
                        <td style={{ textAlign: 'center' }}>
                            {item.seq}
                        </td>
                    ),
                // 'username':
                //     (item) => (
                //         <td>
                //             <CLink href={`/#/user_role?id=${item.id}&user=${item.username}`}>{item.username}</CLink>
                //         </td>
                //     ),
                "updatedate": (item) => (
                    <td className="text-center">
                        {item.updatedate ? moment(item.updatedate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                "createdate": (item) => (
                    <td className="text-center">
                        {item.createdate ? moment(item.createdate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                "new_parent": (item) => (
                    <td>
                    {item.new_parent ? item.new_parent : ""}
                    </td>
                ),
                "createby": (item) => (
                    <td>
                        {item.createby ? item.createby : ""}
                    </td>
                ),
                "updateby": (item) => (
                    <td>
                        {item.updateby ? item.updateby : ""}
                    </td>
                ),
                'option':
                    (item) => (
                        <td className="center">
                            <CButtonGroup>
                                <CButton
                                    color="primary-custom"
                                    variant="reverse"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { EditDetails(item) }}
                                    onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                    onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                >
                                    <img id={`edit-${item.id}`} src={edit_icon} /> 
                                </CButton>
                                {/* <CButton variant="ghost" size="sm" /> */}
                                {/* <CButton
                                    className="ml-3"
                                    color="danger-custom"
                                    variant="reverse"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { deleteDetails(item) }}
                                    onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                    onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                >
                                    <img id={`delete-${item.id}`} src={trash_icon} />
                                </CButton> */}
                            </CButtonGroup>
                        </td>
                    ),
                record_status: (item, index) => (
                    <td align="center">
                        <CSwitch
                            key={index}
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={item.record_status == "A" ? true : false}
                            onChange={(e) =>
                                func_change_status(e.target.checked, item)
                            }
                        />
                    </td>
                ),
                
            }}
        />
        <CModal
              show={modalConfirmEdit}
              onClose={setModalConfirmEdit}
          >
              <CModalHeader closeButton>
                  <CModalTitle>{dataSelected.id ? "แก้ไขข้อมูล" : "เพิ่มข้อมูล"}</CModalTitle>
              </CModalHeader>
              <CForm onSubmit={onSubmit} action="" >
                  <CModalBody>
                      <CRow>
                          <CCol xs="12" sm="12">
                            {checkBox && (
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="idParent">Parent</CLabel>
                                            <CSelect custom name="idParent" id="idParent" onChange={e => setParent(e.target.value)} value={parent}>
                                            <option disabled>เลือก 1 รายการ</option>
                                            {dataParent.map((d,index) => 
                                                <option key={index} value={d.value}>{d.text}</option>
                                            )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            )}
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="name">ชื่อหมวดหมู่ <span style={{color:'red'}}>*</span></CLabel>
                                          <CInput onChange={e => setName(e.target.value)} value={name} id="name" required/>
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="name_en">ชื่อหมวดหมู่ EN <span style={{color:'red'}}>*</span></CLabel>
                                          <CInput onChange={e => setName_en(e.target.value)} value={name_en} id="name_en" required/>
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="seq">ลำดับแสดง <span style={{color:'red'}}>*</span></CLabel>
                                          <CInput type="number" onChange={e => setSeq(e.target.value)} value={seq} id="seq" required/>
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="description">รายละเอียด</CLabel>
                                          <CInput onChange={e => setDescription(e.target.value)} value={description} id="Description" />
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="description_en">รายละเอียด EN</CLabel>
                                          <CInput onChange={e => setDescription_en(e.target.value)} value={description_en} id="description_en" />
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                            {/* <CRow>
                                <CCol xs="12">
                                    <CLabel htmlFor="idParent">มี Parent หรือไม่ ?</CLabel>
                                </CCol>
                            </CRow>
                            <CRow className="mb-4">
                                <CCol xs="12">
                                    <CFormGroup variant="checkbox">
                                        <CInputCheckbox
                                            id="checkbox1"
                                            name="checkbox1"
                                            onChange={(e) => {setCheckBox(!checkBox)}}
                                            checked={checkBox}
                                        />
                                    </CFormGroup>
                                </CCol>
                            </CRow> */}
                          </CCol>
                      </CRow >
                  </CModalBody>
                  <CModalFooter>
                      <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                      <CButton
                          color="secondary"
                          onClick={() => setModalConfirmEdit(false)}
                      >ยกเลิก</CButton>
                  </CModalFooter>
              </CForm >
          </CModal >
        <CModal
            show={modalConfirmDel}
            onClose={setModalConfirmDel}
            color="danger-custom"
        >
            <CModalHeader closeButton>
                <CModalTitle>ลบรายการ : {username}</CModalTitle>
            </CModalHeader>
            <CModalBody>
                คุณต้องการลบรายการนี้ ?
            </CModalBody>
            <CModalFooter>
                <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                <CButton
                    color="secondary"
                    onClick={() => setModalConfirmDel(false)}
                >ยกเลิก</CButton>
            </CModalFooter>
        </CModal>
    </>
  );
};

export default Tables;