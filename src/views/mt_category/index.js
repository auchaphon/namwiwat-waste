import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterCategory from "../../api/ApiMasterCategory";
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiUser from "../../api/ApiUser";

const Mt_category = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [description, setDescription] = useState("");
  const [name, setName] = useState("");
  const [parent, setParent] = useState("");
  const [record_status, set_record_status] = useState("A");
  const userState = useSelector((state) => state.user);

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Category", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [name, parent, description]);

  const getData = async () => {
    try {
        var model = {
            name: name,
            parent: parent,
            description: description,
            record_status: record_status
        }
        func_add_log("Search Menu Master Category", JSON.stringify(model))
        var result = await ApiMasterCategory.get(model);
        //   var result;
        //   if (userState.role.role_code == "admin"){
        //     result = await ApiUser.get();
        //   }else if (userState.role.role_code == "user_branch"){
        //     result = await ApiUser.getByBranch(userState.branch_id);
        //   }else if (userState.role.role_code == "user_team" || userState.role.role_code == "user"){
        //     result = await ApiUser.getByTeam(userState.team_id);
        //   }
        if (result.status === 200) {
            const { data } = result.data;
            setRawDataTable(data);
            var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1);
            for (let i = 0; i < res.length; i++) {
                res[i].order = i + 1
                if (res[i].name == null) {
                    res[i].name = ""
                }
                if (res[i].description == null) {
                    res[i].description = ""
                }
                if (res[i].name_en == null) {
                    res[i].name_en = ""
                }
                if (res[i].description_en == null) {
                    res[i].description_en = ""
                }
                if (res[i].parent == null || data[i].parent == 0) {
                    res[i].parent = ""
                }

                // if (data[i].parent == ) {
                //     data[i].parent = "ไม่ระบุ"
                // }
                // for (let j = 0; j < data.length; j++) {
                //     if (data[i].id === data[j].parent) {
                //         data[j].new_parent = data[i].name
                //     }
                // }
            }
            setDataTable(res);
        }
    } catch (error) {

    }
};
const onSearch = async () => {
    getData();
    // var model = {
    //     activity_detail: "Menu Category Search",
    //     json: JSON.stringify({
    //         code: description,
    //         name: name,
    //         parent: parent
    //     })
    // }
    // if (rawDataTable && rawDataTable.length > 0) {
    //     var result = rawDataTable;
    //     if (description && description.length > 0) {
    //         result = result.filter((x) => x.description.toLowerCase().includes(description.toLowerCase()));
    //     }
    //     if (name && name.length > 0) {
    //         result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
    //     }
    //     if (parent && parent.length > 0) {
    //         result = result.filter((x) => x.parent.toLowerCase().includes(parent.toLowerCase()));
    //     }
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i + 1
    //     }
    //     setDataTable(result);
    // }
};

const resetSearchOption = () => {
        setDescription("");
        setName("");
        setParent("");
        set_record_status("A");
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }
    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>ชื่อหมวดหมู่</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="name" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>รายละเอียด</CLabel>
                                    <CInput onChange={e => setDescription(e.target.value)} value={description} id="description" />
                </CCol>
                <CCol sm="2">
                  <CLabel>Parent</CLabel>
                  <CInput
                    onChange={(e) => setParent(e.target.value)}
                    value={parent}
                    id="parent"
                  />
                </CCol>
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                    </CSelect>
                                </CCol>
                <CCol sm="1"></CCol>
                <CCol sm="3" className="pt-4">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>การจัดการหมวดหมู่สินค้า</b>
            </CCardHeader>
            <CCardBody>
              <Tables
                refreshData={getData}
                data={dataTable}
                func_add_log={func_add_log}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Mt_category;
