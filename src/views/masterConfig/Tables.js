import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import ApiTranLogBack from '../../api/ApiTranLogBack';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {
    const [modal, setModal] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);
    const [value, setValue] = useState('');
    const [code, setCode] = useState('');
    const [desc, setDesc] = useState('');
    const [dataSelected, setDataSelected] = useState('');

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setDataSelected('');
        setValue('');
        setCode('');
        setDesc('');
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setValue(record.config_value);
    }


    const editDetails = (record) => {
        toggle();
        setDataSelected(record);
        setValue(record.config_value);
        setCode(record.config_code);
        setDesc(record.config_description);
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%', textAlign: "center" } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%', textAlign: "center" },
            filter: false,
        },

        { key: 'config_code', label: "รหัสการตั้งค่า", _style: { width: '15%', textAlign: "center" } },
        { key: 'config_description', label: "รายละเอียดการตั้งค่า", _style: { width: '45%', textAlign: "center" } },
        { key: 'config_value', label: "ค่าของข้อมูล", _style: { width: '15%', textAlign: "center" } },
  
    ]

    const onSubmit = (e) => {
        e.preventDefault();
        const model = {
            "config_code": code,
            "config_value": value,
            "config_description": desc,
            "id": dataSelected.id,
        }
        saveData(model);
    }

    const onDelete = async () => {
        try {
            const model = {
                "id": dataSelected.id,
                "record_status": "I"
            }
            func_add_log("Delete Config", JSON.stringify(model))
            const result = await ApiMasterConfig.save(model);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }


    const saveData = async (data) => {
        try {
            func_add_log("Update Config", JSON.stringify(data))
            const result = await ApiMasterConfig.save(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    return (
        <>
            {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton onClick={newDetails} color={'info'}><CIcon name="cil-plus" /><span className="ml-2">สร้างข้อมูลตั้งค่า</span></CButton>
                </CCol>
            </CRow> */}
                <CDataTable
                    items={data}
                    fields={fields}
                    tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                    cleaner
                    itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                    itemsPerPage={10}
                    hover
                    sorter
                    striped
                    bordered
                    pagination
                    scopedSlots={{
                        'order':
                            (item) => (
                                <td style={{ textAlign: 'center' }}>
                                    {item.order}
                                </td>
                            ),
                        'option':
                            (item) => (
                                <td className="center">
                                    <CButtonGroup>
                                        <CButton
                                            color="primary-custom"
                                            variant="reverse"
                                            shape="square"
                                            size="sm"
                                            onClick={() => { editDetails(item) }}
                                            onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                            onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                        >
                                            <img id={`edit-${item.id}`} src={edit_icon} /> 
                                        </CButton>
                                        {/* <CButton variant="ghost" size="sm" />
                                        <CButton
                                            color="danger-custom"
                                            variant="outline"
                                            shape="square"
                                            size="sm"
                                            onClick={() => { deleteDetails(item) }}
                                        >
                                            <CIcon name="cil-trash" />
                                        </CButton> */}
                                    </CButtonGroup>
                                </td>
                            ),
                        
                    }}
                />
                <CModal
                    show={modalConfirm}
                    onClose={setModalConfirm}
                    color="danger-custom"
                >
                    <CModalHeader closeButton>
                        <CModalTitle>ลบรายการ : {code}</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        คุณต้องการลบรายการนี้ ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={() => setModalConfirm(false)}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CModal>
                <CModal
                    show={modal}
                    onClose={toggle}
                >
                    <CModalHeader closeButton>
                        <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                    </CModalHeader>
                    <CForm onSubmit={onSubmit} action="" >
                        <CModalBody>
                            <CRow>
                                <CCol xs="12" sm="12">
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel htmlFor="code">รหัสการตั้งค่า <span style={{color:'red'}}>*</span></CLabel>
                                                <CInput onChange={e => setCode(e.target.value)} value={code} id="code" required readOnly={dataSelected.id} />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel htmlFor="desc">รายละเอียดการตั้งค่า <span style={{color:'red'}}>*</span></CLabel>
                                                <CTextarea onChange={e => setDesc(e.target.value)} value={desc} id="desc" required/>
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">ค่าของข้อมูล</CLabel>
                                                <CInput onChange={e => setValue(e.target.value)} value={value} id="value" />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow >
                        </CModalBody>
                        <CModalFooter>
                            <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                            <CButton
                                color="secondary"
                                onClick={toggle}
                            >ยกเลิก</CButton>
                        </CModalFooter>
                    </CForm >
                </CModal >
        </>
    )
}

export default DemoTable
