import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiMasterConfig from '../../api/ApiMasterConfig';
import ApiTranLogBack from '../../api/ApiTranLogBack';

const Branch = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [code, setCode] = useState("");

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);

    const getData = async () => {
        try {
            const result = await ApiMasterConfig.get();
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                var res = data.sort((a, b) => a.config_code > b.config_code ? 1: -1)
                for (let i = 0; i < res.length; i++) {
                    res[i].order = i+1
                }
                setDataTable(res);
            }
        } catch (error) {

        }
    }
    const onSearch = async () => {
        var model = {
            activity_detail: "Menu Config Search",
            json: JSON.stringify({
                value: value,
                code: code
            })
        }
        await ApiTranLogBack.addlog(model);
        if(rawDataTable && rawDataTable.length > 0){
            var result = rawDataTable;
            if(value && value.length > 0){
                result = result.filter((x) => x.config_value.toLowerCase().includes(value.toLowerCase()));
            }
            if(code && code.length > 0){
                result = result.filter((x) => x.config_code.toLowerCase().includes(code.toLowerCase()));
            }
            for (let i = 0; i < result.length; i++) {
                result[i].order = i+1
            }
            setDataTable(result);
        }
    }

    const resetSearchOption = () => {
        setValue("");
        setCode("");
        var result = rawDataTable;
        for (let i = 0; i < result.length; i++) {
            result[i].order = i+1
        }
        setDataTable(result);
    }

    return (
        <>
            {/* <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>รหัสการตั้งค่า</CLabel>
                                    <CInput onChange={e => setCode(e.target.value)} value={code} id="code"/>
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>ค่าของข้อมูล</CLabel>
                                    <CInput onChange={e => setValue(e.target.value)} value={value} id="value"/>
                                </CCol>
                                <CCol sm="6"></CCol>
                                <CCol sm="2" className="pt-4">
                                    <CRow>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow> */}
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> การตั้งค่า ({dataTable.length}) </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Branch