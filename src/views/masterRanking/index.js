import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranLogBack from '../../api/ApiTranLogBack';
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiMasterRanking from '../../api/ApiMasterRanking';
import ApiUser from '../../api/ApiUser';

const MasterRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [code, setCode] = useState("");
    const [name, setName] = useState("");
    const [point, setPoint] = useState("");

    useEffect(() => {
        getData();
        func_add_log("Click Menu Master Ranking", "{}")
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    const getData = async () => {
        try {
            // func_add_log("Search Menu Master Ranking", "{}")
            const result = await ApiMasterRanking.getAllMtRanking();
            if (result.status === 200) {
                const { data } = result.data;
                var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1)
                setRawDataTable(res);
                for (let i = 0; i < res.length; i++) {
                    res[i].order = i + 1
                }
                setDataTable(res);
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         name: name,
        //         point: point
        //     })
        // }
        // await ApiTranLogBack.addlog(model);
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (name && name.length > 0) {
        //         result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
        //     }
        //     if (point && point.length > 0) {
        //         result = result.filter((x) => x.point.toString().toLowerCase().includes(point.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        // setName("");
        // setPoint("");
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    return (
        <>
            {/* <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>ชื่อ</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="name"/>
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>แต้ม</CLabel>
                                    <CInput onChange={e => setPoint(e.target.value)} value={point} id="point"/>
                                </CCol>
                                <CCol sm="4"></CCol>
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow> */}
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> การจัดการระดับสมาชิก </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} func_add_log={func_add_log} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default MasterRedeem