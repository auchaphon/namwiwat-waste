import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiMasterRanking from '../../api/ApiMasterRanking';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { }, func_add_log }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [name, setName] = useState('')
    const [name_en, setName_en] = useState('')
    const [description, setDescription] = useState('')
    const [description_en, setDescription_en] = useState('')
    const [maxScore, setMaxScore] = useState('')
    const [dataSelected, setDataSelected] = useState('');
    const [picture, setPicture] = useState('')
    const [campaignId, setCampaignId] = useState('')
    const [showImage, setShowImage] = useState('')

    const [campaign, setCampaign] = useState([])

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const editDetails = (record) => {
        toggle();
        setDataSelected(record);
        setName(record.name)
        setName_en(record.name_en)
        setDescription(record.description)
        setDescription_en(record.description_en)
        setMaxScore(record.max_score)
    }

    const onSubmitEdit = (e) => {
        e.preventDefault();
        const model = {
            "name": name,
            "description": description,
            "name_en": name_en,
            "description_en": description_en,
            "max_score": maxScore,
            "updateby": 0,
            "id": dataSelected.id
        }
        editDataMtRanking(model);
    }

    const editDataMtRanking = async (data) => {
        try {
            func_add_log("Update Ranking", JSON.stringify(data))
            const result = await ApiMasterRanking.updateMtRanking(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: 'name', label: "ชื่อระดับสมาชิก", _style: { width: '15%', textAlign: "center" } },
        { key: 'name_en', label: "ชื่อระดับสมาชิก EN", _style: { width: '15%', textAlign: "center" } },
        { key: 'max_score', label: "แต้มสูงสุด", _style: { width: '10%', textAlign: "center" } },
        { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { width: '10%', textAlign: "center" } },
        { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { width: '10%', textAlign: "center" } },
        { key: 'updatedate', label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { width: '10%', textAlign: "center" } },
        { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { width: '10%', textAlign: "center" } },

    ]

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    "name_en": (item) => (
                        <td>
                            {item.name_en ?? ""}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td>
                            {moment(item.updatedate)
                                .format("DD-MM-YYYY HH:mm")}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td>
                            {moment(item.createdate)
                                .format("DD-MM-YYYY HH:mm")}
                        </td>
                    ),
                    "createby": (item) => (
                        <td>
                            {item.createby ? item.createby : ""}
                        </td>
                    ),
                    "updateby": (item) => (
                        <td>
                            {item.updateby ? item.updateby : ""}
                        </td>
                    ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.id}`} src={edit_icon} />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name"> ชื่อระดับสมาชิก <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name_en"> ชื่อระดับสมาชิก EN <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName_en(e.target.value)} value={name_en} id="name_en" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">แต้มสูงสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setMaxScore(e.target.value)} value={maxScore} id="value" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด <span style={{ color: 'red' }}>*</span></CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description_en"> รายละเอียด EN <span style={{ color: 'red' }}>*</span></CLabel>
                                            <ReactQuill onChange={(e) => setDescription_en(e)} modules={modules} value={description_en} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
