import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import Api from '../../api/ApiEmailTemplate';
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from '../../api/ApiTranLogBack';

const EmailTemplate = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
//   const [empid, setEmpId] = useState("");
//   const [username, setUsername] = useState("");
//   const [userfullname, setUserfullname] = useState("");
//   const [email, setEmail] = useState("");
  const userState = useSelector((state) => state.user);

  useEffect(() => {
      getData();
      return () => {
      }
  }, []);

  const getData = async () => {
      try {
          var result = await Api.get();
        //   var result;
        //   if (userState.role.role_code == "admin"){
        //     result = await ApiUser.get();
        //   }else if (userState.role.role_code == "user_branch"){
        //     result = await ApiUser.getByBranch(userState.branch_id);
        //   }else if (userState.role.role_code == "user_team" || userState.role.role_code == "user"){
        //     result = await ApiUser.getByTeam(userState.team_id);
        //   }
          if (result.status === 200) {
              const { data } = result.data;
            //   setRawDataTable(data);
            var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1)
              for (let i = 0; i < res.length; i++) {
                res[i].order = i+1
                //   if(data[i].userfullname == null){
                //       data[i].userfullname = "ไม่ระบุ"
                //   }
                //   if(data[i].useremail == null){
                //       data[i].useremail = "ไม่ระบุ"
                //   }
                //   if(data[i].group_name == null){
                //       data[i].group_name = "ไม่ระบุ"
                //   }
                //   if(data[i].team_name == null){
                //       data[i].team_name = "ไม่ระบุ"
                //   }
                //   if(data[i].branch_name == null){
                //       data[i].branch_name = "ไม่ระบุ"
                //   }
              }
              setDataTable(res);
          }
      } catch (error) {

      }
  }
  const onSearch = async () => {
    var model = {
        activity_detail: "Menu Email Template Search",
        // json: JSON.stringify({
        //     empid: empid,
        //     username: username,
        //     userfullname: userfullname,
        //     email: email,
        // }),
        json: JSON.stringify({}),
    }
    await ApiTranLogBack.addlog(model);
      if(rawDataTable && rawDataTable.length > 0){
          var result = rawDataTable;
        //   if(empid && empid.length > 0){
        //       result = result.filter((x) => x.emp_id.toLowerCase().includes(empid.toLowerCase()));
        //   }
        //   if(username && username.length > 0){
        //       result = result.filter((x) => x.username.toLowerCase().includes(username.toLowerCase()));
        //   }
        //   if(userfullname && userfullname.length > 0){
        //       result = result.filter((x) => x.userfullname.toLowerCase().includes(userfullname.toLowerCase()));
        //   }
        //   if(email && email.length > 0){
        //       result = result.filter((x) => x.useremail.toLowerCase().includes(email.toLowerCase()));
        //   }
          for (let i = 0; i < result.length; i++) {
              result[i].order = i+1
          }
          setDataTable(result);
      }
  }

  const resetSearchOption = () => {
    //   setEmpId("");
    //   setUsername("");
    //   setUserfullname("");
    //   setEmail("");
      var result = rawDataTable;
      for (let i = 0; i < result.length; i++) {
          result[i].order = i+1
      }
      setDataTable(result);
  }
  return (
    <>
      {/* <CRow>
          <CCol xs="12" lg="12">
              <CCard>
                  <CCardBody>
                      <CRow>
                          <CCol sm="4">
                              <CLabel>รหัสพนักงาน</CLabel>
                              <CInput onChange={e => setEmpId(e.target.value)} value={empid} id="empid"/>
                          </CCol>
                          <CCol sm="4">
                              <CLabel>รหัสผู้ใช้งาน</CLabel>
                              <CInput onChange={e => setUsername(e.target.value)} value={username} id="username"/>
                          </CCol>
                          <CCol sm="4">
                              <CLabel>ชื่อพนักงาน</CLabel>
                              <CInput onChange={e => setUserfullname(e.target.value)} value={userfullname} id="userfullname"/>
                          </CCol>
                      </CRow>
                      <CRow className="mt-2">
                          <CCol sm="4">
                              <CLabel>Email พนักงาน</CLabel>
                              <CInput onChange={e => setEmail(e.target.value)} value={email} id="email"/>
                          </CCol>
                          <CCol sm="4"></CCol>
                          <CCol sm="4" className="pt-4">
                              <CRow>
                                  <CCol xs="6">
                                      <CButton
                                          className="mt-1"
                                          color="danger-custom"
                                          variant="reverse"
                                          block
                                          onClick={() => resetSearchOption()}
                                      >
                                          <b> ล้างข้อมูล </b>{" "}
                                      </CButton>
                                  </CCol>
                                  <CCol xs="6">
                                      <CButton
                                          className="mt-1"
                                          color="success-custom"
                                          variant="outline"
                                          block
                                          onClick={() => onSearch()}
                                      >
                                          <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                          <b> ค้นหา </b>
                                      </CButton>
                                  </CCol>
                              </CRow>
                          </CCol>
                      </CRow>
                  </CCardBody>
              </CCard>
          </CCol>
      </CRow> */}
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>Email Template</b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData}  data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default EmailTemplate;
