import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Api from "../../api/ApiEmailTemplate";
import Swal from "sweetalert2";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const Details = (props, { data }) => {
  var id = new URLSearchParams(props.location.search).get("id");
  const history = useHistory();
  const [emailCode, setEmailCode] = useState("");
  const [emailName, setEmailName] = useState("");
  const [emailSubject, setEmailSubject] = useState("");
  const [emailSubject_EN, setEmailSubject_EN] = useState("");
  const [emailBody, setEmailBody] = useState("");
  const [emailBody_EN, setEmailBody_EN] = useState("");
  const [emailTo, setEmailTo] = useState("");
  const [emailCc, setEmailCc] = useState("");

  const modules = {
    toolbar: [
      ["bold", "italic", "underline", "strike"], // toggled buttons
      // ['blockquote', 'code-block'],
      // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ list: "ordered" }, { list: "bullet" }],
      // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
      // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ color: [] }, { background: [] }], // dropdown with defaults from theme
      [{ font: [] }],
      [{ align: [] }],
      // ['clean']                                         // remove formatting button
    ],
  };

  useEffect(() => {
    if (id != 0) {
      getDetail();
    }

    return () => {};
  }, []);

  const getDetail = async () => {
    try {
      var result = await Api.getDetail(id);
      if (result.status === 200) {
        const { data } = result.data;
        setEmailCode(data.email_code);
        setEmailName(data.email_name);
        setEmailSubject(data.email_subject);
        setEmailSubject_EN(data.email_subject_en);
        setEmailBody(data.email_body ?? "");
        setEmailBody_EN(data.email_body_en ?? "");
        setEmailTo(data.email_to);
        setEmailCc(data.email_cc);
      }
    } catch (error) {}
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const model = {
      id: id,
      email_code: emailCode,
      email_name: emailName,
      email_subject: emailSubject,
      email_subject_en: emailSubject_EN,
      email_body: emailBody,
      email_body_en: emailBody_EN,
      email_to: emailTo,
      email_cc: emailCc,
    };
    saveData(model);
  };

  const saveData = async (data) => {
    try {
      const result = await Api.save(data);
      if (result.status === 200) {
        const { data } = result.data;
        // toggle();
        Swal.fire({
          icon: "success",
          title: "บันทึกสำเร็จ",
          timer: 2000,
        }).then((success) => {
          history.push(`/emailtemplate`);
          history.go(0);
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const onBack = () => {
    history.push(`/emailtemplate`);
    history.go(0);
  };
  return (
    <>
      <CCard>
        {/* <CCardHeader>
                    <b>Email Template</b>
                </CCardHeader> */}
        <CForm onSubmit={onSubmit} action="">
          <CCardBody>
            <CRow className="card-section-header">
              <b>แก้ไขข้อมูล</b>
            </CRow>
            <CRow>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="emailCode">
                    รหัสอีเมล <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="emailCode"
                    onChange={(e) => setEmailCode(e.target.value)}
                    value={emailCode}
                    required
                    readOnly={id != 0}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="emailName">
                    ชื่ออีเมล <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="emailName"
                    onChange={(e) => setEmailName(e.target.value)}
                    value={emailName}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="emailTo">ถึง (To)</CLabel>
                  <CInput
                    id="emailTo"
                    onChange={(e) => setEmailTo(e.target.value)}
                    value={emailTo}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="emailCc">สำเนาถึง (Cc)</CLabel>
                  <CInput
                    id="emailCc"
                    onChange={(e) => setEmailCc(e.target.value)}
                    value={emailCc}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="emailSubject">
                    หัวข้อเรื่อง <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="emailSubject"
                    onChange={(e) => setEmailSubject(e.target.value)}
                    value={emailSubject}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="emailSubject_EN">
                    หัวข้อเรื่อง EN<span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CInput
                    id="emailSubject_EN"
                    onChange={(e) => setEmailSubject_EN(e.target.value)}
                    value={emailSubject_EN}
                    required
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="emailBody">
                    รายละเอียด <span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CKEditor
                    editor={ClassicEditor}
                    data={emailBody}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setEmailBody(data);
                    }}
                    id="emailBody"
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="emailBody_EN">
                    รายละเอียด EN<span style={{ color: "red" }}>*</span>
                  </CLabel>
                  <CKEditor
                    editor={ClassicEditor}
                    data={emailBody_EN}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setEmailBody_EN(data);
                    }}
                    id="emailBody_EN"
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="10"></CCol>
              <CCol sm="1">
                <CButton
                  type="submit"
                  color="primary-custom"
                  variant="reverse"
                  block
                >
                  บันทึก
                </CButton>{" "}
              </CCol>
              <CCol sm="1">
                <CButton color="secondary" block onClick={onBack}>
                  ยกเลิก
                </CButton>
              </CCol>
            </CRow>
          </CCardBody>
          {/* <CCardFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={onBack}
                        >ยกเลิก</CButton>
                    </CCardFooter> */}
        </CForm>
      </CCard>
    </>
  );
};
export default Details;
