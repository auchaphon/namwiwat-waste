import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Api from '../../api/ApiEmailTemplate';
import Swal from "sweetalert2";
import moment from 'moment';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");

const Tables = ({ data = [], refreshData = () => { } }) => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalConfirm, setModalConfirm] = useState(false);
  const [dataSelected, setDataSelected] = useState('');
  const userState = useSelector((state) => state.user);
  const [emailCode, setEmailCode] = useState('');

  useEffect(() => {
      return () => {
      }
  }, []);


  const toggle = () => {
      setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%" } },
    {
        key: 'option',
        label: '',
        _style: { width: '1%' },
        filter: false,
    },
    { key: "email_code", label: "รหัสอีเมล", _style: { minWidth: "150px", textAlign: "center" }},
    { key: "email_name", label: "ชื่ออีเมล" , _style: { minWidth: "150px", textAlign: "center" }},
    { key: "updatedate", label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: "150px" , textAlign: "center"} },
    { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { minWidth: "150px", textAlign: "center" }},
  ];

  const toggleConfirm = () => {
      setModalConfirm(!modalConfirm);
  }

  // const newDetails = (record) => {
  //   history.push(`/emailtemplatedetail?id=0`)
  //   history.go(0);
  // }

  const deleteDetails = (record) => {
      toggleConfirm();
      setDataSelected(record);
      setEmailCode(record.email_code);
  }


  const editDetails = (record) => {
    history.push(`/emailtemplatedetail?id=${record.id}`);
    history.go(0);
  }

  const onDelete = async () => {
      try {
          const model = {
              "id": dataSelected.id,
              "record_status": "I"
          }
          const result = await Api.save(model);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirm();
              Swal.fire({
                  icon: "success",
                  title: "ลบสำเร็จ",
                  timer: 2000,
              }).then((success) => {
                  refreshData();
              });
          }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
          });
      }
  }

  return (
    <>
      {/* <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => newDetails()}
            color={"info"}
          >
            <CIcon name="cil-plus" />
            <span className="ml-2">สร้างผู้ใช้</span>
          </CButton>
        </CCol>
      </CRow> */}
        <CDataTable
            items={data}
            fields={fields}
            tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
            cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
                'order':
                    (item) => (
                        <td style={{ textAlign: 'center' }}>
                            {item.order}
                        </td>
                    ),
                'email_code':
                    (item) => (
                        <td className="text-center">
                            {item.email_code ?? ""}
                        </td>
                    ),
                'updateby':
                    (item) => (
                        <td className="text-center">
                            {item.updateby ?? ""}
                        </td>
                    ),
                "updatedate": (item) => (
                    <td className="text-center">
                        {item.updatedate ? moment(item.updatedate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                "createdate": (item) => (
                    <td className="text-center">
                        {item.createdate ? moment(item.createdate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                'option':
                    (item) => (
                        <td className="center">
                            <CButtonGroup>
                                <CButton
                                    color="primary-custom"
                                    variant="reverse"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { editDetails(item) }}
                                    onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                    onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                >
                                    <img id={`edit-${item.id}`} src={edit_icon} /> 
                                </CButton>
                                {/* <CButton variant="ghost" size="sm" />
                                <CButton
                                    color="danger-custom"
                                    variant="outline"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { deleteDetails(item) }}
                                >
                                    <CIcon name="cil-trash" />
                                </CButton> */}
                            </CButtonGroup>
                        </td>
                    ),
                
            }}
        />
        <CModal
            show={modalConfirm}
            onClose={setModalConfirm}
            color="danger-custom"
        >
            <CModalHeader closeButton>
                <CModalTitle>ลบรายการ : {emailCode}</CModalTitle>
            </CModalHeader>
            <CModalBody>
                คุณต้องการลบรายการนี้ ?
            </CModalBody>
            <CModalFooter>
                <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                <CButton
                    color="secondary"
                    onClick={() => setModalConfirm(false)}
                >ยกเลิก</CButton>
            </CModalFooter>
        </CModal>
    </>
  );
};

export default Tables;
