import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CDataTable,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CFormGroup,
  CInputCheckbox,
  CLabel,
} from "@coreui/react";
import { WEB_API } from "../../env";
import ApiMasterCustomer from "../../api/ApiMasterCustomer";
import Swal from "sweetalert2";
import { useHistory } from "react-router";

const defultImage = require("../../assets/icons/user-duotone-5.svg");

const CustomerEdit = (props) => {
  const history = useHistory();
  const [id, setId] = useState(0);
  const [modal, setModal] = useState(false);
  const [isgoogle, setIsgoogle] = useState(false);
  const [isfacebook, setIsfacebook] = useState(false);
  const [state, setstate] = useState({
    birthdate: "",
    email: "",
    firstname: "",
    lastname: "",
    phonenumber: "",
    image_profile: "",
    is_facebook: "",
    is_gmail: "",
    username: "username",
    customerAddress: [],
    tranredeem: [],
  });

  const fields = [
    {
      key: "image",
      label: "รูปภาพ",
      _style: { minWidth: "120px" },
    },
    {
      key: "redeemname",
      label: "ชื่อของรางวัล",
      _style: { minWidth: "150px" },
    },
    { key: "description", label: "รายละเอียด" },
    { key: "point", label: "แต้ม" },
  ];

  useEffect(() => {
    if (new URLSearchParams(props.location.search).get("id") != null) {
      setId(new URLSearchParams(props.location.search).get("id"));
    }
  }, []);

  useEffect(() => {
    if (id != 0) {
      func_loadata();
    }
  }, [id]);
  const func_loadata = async () => {
    try {
      const result = await ApiMasterCustomer.GetEdit(id);
      if (result.status === 200) {
        const { data } = result.data;
        console.log(data);
        if (data.is_facebook != null && data.is_facebook != "") {
          setIsfacebook(true);
        }

        if (data.is_gmail != null && data.is_gmail != "") {
          setIsgoogle(true);
        }
        setstate(data);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CRow className="mb-4">
            <CCol xs="3">
              {state.image_profile != "" && state.image_profile != null ? (
                <div>
                  <img
                    src={WEB_API + state.image_profile}
                    className="c-previewImage"
                  ></img>
                </div>
              ) : (
                <div>
                  <img src={defultImage} className="c-previewImage"></img>
                </div>
              )}
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol sm="2">
              <CButton
                block
                variant="outline"
                color="primary"
                onClick={() => {
                  setModal(true);
                }}
              >
                ประวัติการแลกของรางวัล
              </CButton>
            </CCol>
          </CRow>
          <CCard>
            <CCardHeader>
              <b>ข้อมูลลูกค้า</b>
            </CCardHeader>
            <CCardBody>
              <CRow className="mb-3">
                <CCol xs="2">ชื่อผู้ใช้</CCol>
                <CCol xs="3">
                  <label>{state.username}</label>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol xs="2">ชื่อ</CCol>
                <CCol xs="3">
                  <label>{state.firstname}</label>
                </CCol>
                <CCol xs="2">นามสกุล</CCol>
                <CCol xs="3">
                  <label>{state.lastname}</label>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol xs="2">อีเมล</CCol>
                <CCol xs="3">
                  <label>{state.email}</label>
                </CCol>
                <CCol xs="2">หมายเลขโทรศัพท์</CCol>
                <CCol xs="3">
                  <label>{state.phonenumber}</label>
                </CCol>
              </CRow>

              <CRow className="mb-3">
                <CCol xs="2">
                  <CFormGroup variant="checkbox" className="checkbox">
                    <CInputCheckbox
                      id="checkbox1"
                      name="checkbox1"
                      checked={isfacebook}
                    />
                    <CLabel htmlFor="checkbox1">User Facebook</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="2">
                  <CFormGroup variant="checkbox" className="checkbox">
                    <CInputCheckbox
                      id="checkbox2"
                      name="checkbox2"
                      checked={isgoogle}
                    />
                    <CLabel htmlFor="checkbox2">User Gamil</CLabel>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>

          <CCard>
            <CCardHeader>
              <b>ที่อยู่การจัดส่ง</b>
            </CCardHeader>
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  {state.customerAddress.filter((x) => x.is_tax != 1) &&
                    state.customerAddress.filter((x) => x.is_tax != 1).map((item, index) => {
                      return (
                        <CNavItem key={index}>
                          <CNavLink>ที่อยู่ {index + 1}</CNavLink>
                        </CNavItem>
                      );
                    })}
                </CNav>
                <CTabContent>
                  {state.customerAddress.filter((x) => x.is_tax != 1) &&
                    state.customerAddress.filter((x) => x.is_tax != 1).map((item, index) => {
                      return (
                        <CTabPane key={index}>
                          <div className="ml-3 mr-3 mt-3">
                            <b>ข้อมูลทั่วไป</b>
                            <hr />
                            <div className="ml-3 mr-3 mt-3">
                              <CRow className="mb-3">
                                <CCol xs="2">ชื่อ</CCol>
                                <CCol xs="3">
                                  <label>{item.firstname}</label>
                                </CCol>
                                <CCol xs="2">นามสกุล</CCol>
                                <CCol xs="3">
                                  <label>{item.lastname}</label>
                                </CCol>
                              </CRow>
                              <CRow className="mb-3">
                                <CCol xs="2">หมายเลขโทรศัพท์</CCol>
                                <CCol xs="3">
                                  <label>{item.phonenumber}</label>
                                </CCol>
                              </CRow>
                            </div>

                            <b>ข้อมูลที่อยู่</b>
                            <hr />
                            <div className="ml-3 mr-3 mt-3">
                              <CRow className="mb-3">
                                <CCol xs="2">ที่อยู่</CCol>
                                <CCol xs="3">
                                  <label>{item.address}</label>
                                </CCol>
                                <CCol xs="2">เขต/อำเภอ</CCol>
                                <CCol xs="3">
                                  <label>{item.district}</label>
                                </CCol>
                              </CRow>

                              <CRow className="mb-3">
                                <CCol xs="2">แขวง/ตำบล</CCol>
                                <CCol xs="3">
                                  <label>{item.sub_district}</label>
                                </CCol>
                                <CCol xs="2">จังหวัด</CCol>
                                <CCol xs="3">
                                  <label>{item.province}</label>
                                </CCol>
                              </CRow>

                              <CRow className="mb-3">
                                <CCol xs="2">รหัสไปรษณีย์</CCol>
                                <CCol xs="3">
                                  <label>{item.zip_code}</label>
                                </CCol>
                              </CRow>
                            </div>
                          </div>
                        </CTabPane>
                      );
                    })}
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>

          <CCard>
            <CCardHeader>
              <b>ที่อยู่สำหรับใบกำกับภาษีรับเงิน</b>
            </CCardHeader>
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  {state.customerAddress.filter((x) => x.is_tax == 1) &&
                    state.customerAddress.filter((x) => x.is_tax == 1).map((item, index) => {
                      return (
                        <CNavItem key={index}>
                          <CNavLink>ที่อยู่ {index + 1}</CNavLink>
                        </CNavItem>
                      );
                    })}
                </CNav>
                <CTabContent>
                  {state.customerAddress.filter((x) => x.is_tax == 1) &&
                    state.customerAddress.filter((x) => x.is_tax == 1).map((item, index) => {
                      return (
                        <CTabPane key={index}>
                          <div className="ml-3 mr-3 mt-3">
                            <b>ข้อมูลทั่วไป</b>
                            <hr />
                            <div className="ml-3 mr-3 mt-3">
                              <CRow className="mb-3">
                                <CCol xs="2">ชื่อ</CCol>
                                <CCol xs="3">
                                  <label>{item.firstname}</label>
                                </CCol>
                                <CCol xs="2">นามสกุล</CCol>
                                <CCol xs="3">
                                  <label>{item.lastname}</label>
                                </CCol>
                              </CRow>
                              <CRow className="mb-3">
                                <CCol xs="2">หมายเลขโทรศัพท์</CCol>
                                <CCol xs="3">
                                  <label>{item.phonenumber}</label>
                                </CCol>
                              </CRow>
                            </div>

                            <b>ข้อมูลที่อยู่</b>
                            <hr />
                            <div className="ml-3 mr-3 mt-3">
                              <CRow className="mb-3">
                                <CCol xs="2">ที่อยู่</CCol>
                                <CCol xs="3">
                                  <label>{item.address}</label>
                                </CCol>
                                <CCol xs="2">เขต/อำเภอ</CCol>
                                <CCol xs="3">
                                  <label>{item.district}</label>
                                </CCol>
                              </CRow>

                              <CRow className="mb-3">
                                <CCol xs="2">แขวง/ตำบล</CCol>
                                <CCol xs="3">
                                  <label>{item.sub_district}</label>
                                </CCol>
                                <CCol xs="2">จังหวัด</CCol>
                                <CCol xs="3">
                                  <label>{item.province}</label>
                                </CCol>
                              </CRow>

                              <CRow className="mb-3">
                                <CCol xs="2">รหัสไปรษณีย์</CCol>
                                <CCol xs="3">
                                  <label>{item.zip_code}</label>
                                </CCol>
                              </CRow>
                            </div>
                          </div>
                        </CTabPane>
                      );
                    })}
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>

          <CRow className="mb-3">
            <CCol sm="10"></CCol>
            <CCol sm="2">
              <CButton
                color="secondary"
                block
                onClick={() => {
                  history.push("/mt_customer");
                }}
              >
                ย้อนกลับ
              </CButton>
            </CCol>
          </CRow>
        </CCol>
      </CRow>

      <CModal size="xl" show={modal} onClose={setModal}>
        <CModalHeader closeButton>
          <CModalTitle>ประวัติการแลกของรางวัล</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs="12" sm="12">
              <CDataTable
                items={state.tranredeem}
                fields={fields}
                tableFilter={{
                  label: "ค้นหา",
                  placeholder: "พิมพ์คำที่ต้องการค้นหา",
                }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                  image: (item) => (
                    <td>
                      <img
                        style={{ width: "120px" }}
                        src={WEB_API + item.image}
                      ></img>
                    </td>
                  ),
                  description: (item) => (
                    <td>
                      <div
                        dangerouslySetInnerHTML={{ __html: item.description }}
                      />
                    </td>
                  ),
                }}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="secondary"
            onClick={() => {
              setModal(false);
            }}
          >
            ปิด
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default CustomerEdit;
