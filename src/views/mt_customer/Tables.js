import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterCustomer from "../../api/ApiMasterCustomer";
import Swal from "sweetalert2";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => { }, func_add_log }) => {
  // const [edit_icon_icon, setEditIcon] = useState(edit_icon);
  console.log(data)

  const history = useHistory();
  useEffect(() => {
    return () => { };
  }, []);

  const fields = [
    {
      key: "option",
      label: "",
      _style: { width: "1%", textAlign: "center" },
      filter: false,
    },
    {
      key: "record_status",
      label: "สถานะ",
      _style: { minWidth: "50px", textAlign: "center" },
      filter: false,
    },
    { key: "code", label: "รหัสลูกค้า", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "name", label: "ชื่อโรงพยาบาล", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "email", label: "อีเมล", _style: { minWidth: "150px", textAlign: "center" } },
    {
      key: "phonenumber",
      label: "หมายเลขโทรศัพท์",
      _style: { minWidth: "150px" },
    },
    {
      key: "updatedate",
      label: "วันที่แก้ไขข้อมูลล่าสุด",
      _style: { minWidth: "150px", textAlign: "center" },
    },
    {
      key: "updateby",
      label: "ผู้แก้ไขข้อมูล",
      _style: { minWidth: "150px", textAlign: "center" },
    },
  ];

  const EditDetails = (record) => {

  };

  const func_change_status = async (e, record) => {

  };

  const update_status = async (data) => {

  }

  return (
    <>
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          updatedate: (item) => (
            <td className="text-center">
              {item.updatedate ? moment(item.updatedate).format("DD-MM-YYYY HH:mm") : ""}
            </td>
          ),
          createdate: (item) => (
            <td className="text-center">
              {item.createdate ? moment(item.createdate).format("DD-MM-YYYY HH:mm") : ""}
            </td>
          ),
          point_amt: (item) => (
            <td className="text-center">
              {item.point_amt ?? ""}
            </td>
          ),
          createby: (item) => (
            <td>
              {item.createby ? item.createby : ""}
            </td>
          ),
          updateby: (item) => (
            <td>
              {item.updateby ? item.updateby : ""}
            </td>
          ),
          option: (item) => (
            <td className="center">
              <CButtonGroup>
                <CButton
                  color="primary-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    EditDetails(item);
                  }}
                  onMouseOver={(e) =>
                  (document.getElementById(`edit-${item.id}`).src =
                    edit_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`edit-${item.id}`).src = edit_icon)
                  }
                >
                  <img id={`edit-${item.id}`} src={edit_icon} />
                </CButton>
              </CButtonGroup>
            </td>
          ),
          record_status: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.recordstatus == "A" ? true : false}
                onChange={(e) =>
                  func_change_status(e.target.checked, item)
                }
              />
            </td>
          ),
        }}
      />
    </>
  );
};

export default Tables;
