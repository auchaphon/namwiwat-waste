import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterCustomer from "../../api/ApiMasterCustomer";
import ApiUser from "../../api/ApiUser"
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from "../../api/ApiTranLogBack";

const Mt_customer = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [email, setEmail] = useState("");
  // const [username, setUsername] = useState("");
  const [fullname, setFullName] = useState("");
  // const [lastname, setLastName] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const [record_status, set_record_status] = useState("A");
  const userState = useSelector((state) => state.user);

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Customer", "{}")
    return () => { };
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json
    }
    await ApiTranLogBack.addlog(model);
  }

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [fullname, email, phonenumber]);

  const getData = async () => {
    try {
      const models = [{
        "id": "1",
        "code": "CUS001",
        "username": "poteja",
        "phonenumber": "0982856576",
        "email": "kasemrart@gmail.com",
        "name": "โรงพยาบาลเกษมราษฏร์",
        "recordstatus": "A",
        "updatedate": "2022-01-26T03:15:58.000Z",
        "updateby": "Admin",
        "createby": null
      }];
      setRawDataTable(models);
      setDataTable(models);
    } catch (error) { }
  };

  const onExport = async () => {

  };

  const onSearch = async () => {
    var model = {
      activity_detail: "Menu Tag Search",
      json: JSON.stringify({
        // username: username,
        email: email,
        fullname: fullname,
        // lastname: lastname,
        phonenumber: phonenumber
      }),
    };
    getData();
    // if (rawDataTable && rawDataTable.length > 0) {
    //   var result = rawDataTable;
    //   if (name && name.length > 0) {
    //     console.log("abcdegasf");
    //     result = result.filter((x) =>
    //       x.name.toLowerCase().includes(name.toLowerCase())
    //     );
    //   }
    //   if (email && email.length > 0) {
    //     result = result.filter((x) =>
    //       x.email.toLowerCase().includes(email.toLowerCase())
    //     );
    //   }
    //   if (phonenumber && phonenumber.length > 0) {
    //     result = result.filter((x) =>
    //       x.phonenumber.toLowerCase().includes(phonenumber.toLowerCase())
    //     );
    //   }
    //   for (let i = 0; i < result.length; i++) {
    //     result[i].order = i + 1;
    //   }
    //   setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    // setUsername("");
    setEmail("");
    setFullName("");
    // setLastName("");
    setPhonenumber("")
    set_record_status("A");
    // var result = rawDataTable;
    // for (let i = 0; i < result.length; i++) {
    //   result[i].order = i + 1;
    // }
    // setDataTable(result);
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>ชื่อโรงพยาบาล</CLabel>
                  <CInput
                    onChange={(e) => setFullName(e.target.value)}
                    value={fullname}
                    id="fullname"
                  />
                </CCol>
                {/* <CCol sm="2">
                  <CLabel></CLabel>
                  <CInput
                    onChange={(e) => setLastName(e.target.value)}
                    value={lastname}
                    id="lastname"
                  />
                </CCol> */}
                <CCol sm="2">
                  <CLabel>อีเมล</CLabel>
                  <CInput
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="Email"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>หมายเลขโทรศัพท์</CLabel>
                  <CInput
                    onChange={(e) => setPhonenumber(e.target.value)}
                    value={phonenumber}
                    id="phonenumber"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>สถานะ</CLabel>
                  <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                    <option value="">ทั้งหมด</option>
                    <option value="A">Active</option>
                    <option value="I">Inactive</option>
                  </CSelect>
                </CCol>
                <CCol sm="4" className="pt-4">
                  <CRow>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="success-custom"
                        variant="reverse"
                        block
                        onClick={() => onExport()}
                      >
                        <b> ออกรายงาน </b>
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>ข้อมูลลูกค้า</b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} func_add_log={func_add_log} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Mt_customer;
