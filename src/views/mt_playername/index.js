import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterPlayername from "../../api/ApiMasterPlayername";
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiUser from "../../api/ApiUser";

const Mt_tag = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [snumber, setSnumber] = useState("");
  const [name, setName] = useState("");
  const [record_status, set_record_status] = useState("A");
  const userState = useSelector((state) => state.user);

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Tag", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [code, name]);

  const getData = async () => {
    try {
      var model = {
        snumber: snumber,
        name: name,
        record_status: record_status
      };
      func_add_log("Search Menu Master PlayerName", JSON.stringify(model));
      console.log("sadas");
      var result = await ApiMasterPlayername.get(model);
      //   var result;
      //   if (userState.role.role_code == "admin"){
      //     result = await ApiUser.get();
      //   }else if (userState.role.role_code == "user_branch"){
      //     result = await ApiUser.getByBranch(userState.branch_id);
      //   }else if (userState.role.role_code == "user_team" || userState.role.role_code == "user"){
      //     result = await ApiUser.getByTeam(userState.team_id);
      //   }
      if (result.status === 200) {
        const { data } = result.data;
        var res = data.sort((a, b) => (a.updatedate > b.updatedate ? -1 : 1));
        setRawDataTable(res);
        for (let i = 0; i < res.length; i++) {
          res[i].order = i + 1;
          if (res[i].snumber == null) {
            res[i].snumber = "";
          }
          if (res[i].name == null) {
            res[i].name = "";
          }
          if (res[i].price == null) {
            res[i].price = 0;
          }
        }
        console.log(res);
        setDataTable(res);
      }
    } catch (error) {}
  };
  const onSearch = async () => {
    getData();
    // var model = {
    //     activity_detail: "Menu Tag Search",
    //     json: JSON.stringify({
    //         code: code,
    //         name: name
    //     })
    // }
    // if (rawDataTable && rawDataTable.length > 0) {
    //     var result = rawDataTable;
    //     if (name && name.length > 0) {
    //         result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
    //     }
    //     if (code && code.length > 0) {
    //         result = result.filter((x) => x.code.toLowerCase().includes(code.toLowerCase()));
    //     }
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i + 1
    //     }
    //     setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    setSnumber("");
    setName("");
    set_record_status("A");
    // var result = rawDataTable;
    // for (let i = 0; i < result.length; i++) {
    //     result[i].order = i + 1
    // }
    // setDataTable(result);
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>หมายเลข</CLabel>
                  <CInput
                    onChange={(e) => setSnumber(e.target.value)}
                    value={snumber}
                    id="snumber"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชื่อ</CLabel>
                  <CInput
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    id="name"
                  />
                </CCol>
                <CCol sm="2">
                    <CLabel>สถานะ</CLabel>
                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                        <option value="">ทั้งหมด</option>
                        <option value="A">Active</option>
                        <option value="I">Inactive</option>
                    </CSelect>
                </CCol>
                <CCol sm="3"></CCol>

                <CCol sm="3" className="pt-4">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>ชื่อผู้เล่น</b>
            </CCardHeader>
            <CCardBody>
              <Tables
                refreshData={getData}
                data={dataTable}
                func_add_log={func_add_log}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Mt_tag;
