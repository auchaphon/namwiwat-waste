import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CTextarea,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiUser from '../../api/ApiUser';
import ApiTranRedeem from '../../api/ApiTranRedeem'
import Swal from "sweetalert2";
import moment from 'moment';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => { }, func_add_log }) => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [statusRedeem, setStatusRedeem] = useState([]);
  const [modalConfirm, setModalConfirm] = useState(false);
  const [modalConfirm2, setModalConfirm2] = useState(false);

  const [unapproveId, setUnapproveId] = useState('');
  const [note, setNote] = useState('');


  useEffect(() => {
    return () => {
    }
  }, []);


  const toggle = () => {
    setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%" } },
    {
      key: 'option',
      label: '',
      _style: { width: '1%' },
      filter: false,
    },
    { key: "name", label: "ชื่อของรางวัล", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "firstname", label: "ชื่อลูกค้า", _style: { minWidth: "150px", textAlign: "center" } },
    // { key: "updatedate", label: "วันที่แก้ไขล่าสุด", _style: { minWidth: "150px" } },
    // { key: "updateby", label: "ผู้แก้ไข", _style: { minWidth: "150px" } },
    { key: "express", label: "สถานะการจัดส่ง", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "redeem_status", label: "สถานะ", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "createdate", label: "วันที่ขอแลกของรางวัล", _style: { minWidth: "150px", textAlign: "center" } },
    // { key: "updatedate", label: "วันที่อนุมัติ", _style: { minWidth: "150px", textAlign: "center" } },
    // { key: "createby", label: "ผู้อนุมัติ", _style: { minWidth: "150px", textAlign: "center" } },
    // { key: "updateby", label: "ผู้อนุมัติ", _style: { minWidth: "150px", textAlign: "center" } },

    {
      key: 'option2',
      label: '',
      _style: { width: '8%' },
      filter: false,
    },
  ];


  const checkData = (e, item) => {
    let value = e.target.checked
    let arr = statusRedeem
    if (value) {
      arr.push(item.id)
    }
    else {
      let index = arr.indexOf(item.id)
      arr.splice(index, 1)
    }
    console.log(statusRedeem)
  }

  const redeemFor = () => {
    if (statusRedeem.length === 0) {
      console.log(statusRedeem.length)
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกข้อมูล",
      });
    }
    else {
      updateRedeemStatus(statusRedeem, 1)
    }
  }

  const updateRedeemStatus = async (status, update) => {
    try {
      func_add_log("Update Redeem Status", JSON.stringify({status: status, update : update}))
      const result = await ApiTranRedeem.updateRedeemStatus(status, update);
      if (result.status === 200) {
        toggleConfirm();
        Swal.fire({
          icon: "success",
          title: "แก้ไขข้อมูลสำเร็จ",
          timer: 2000,
        }).then((success) => {
          window.location.reload();
        });
      }
      return result
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  }

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  }

  const toggleConfirm2 = () => {
    setModalConfirm2(!modalConfirm2);
  }

  const onUnapprove = (record) => {
    toggleConfirm2()
    setUnapproveId(record.id)
  }

  const unapprove = async () => {
    const model = {
      "id": unapproveId,
      "note": note,
      "updateby": 1
    }
    try {
      func_add_log("Reject Redeem", JSON.stringify(model))
      const result = await ApiTranRedeem.unapproveRedeemStatus(model);
      if (result.status === 200) {
        toggleConfirm2();
        Swal.fire({
          icon: "success",
          title: "แก้ไขข้อมูลสำเร็จ",
          timer: 2000,
        }).then((success) => {
          window.location.reload();
        });
      }
      return result
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  }

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => toggleConfirm()}
            color="primary-custom"
            variant="reverse"
          >
            <span className><b>อนุมัติ</b></span>
          </CButton>
        </CCol>
      </CRow>
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          'order':
            (item) => (
              <td style={{ textAlign: 'center' }}>
                {item.order}
              </td>
            ),
          "updatedate": (item) => (
              <td className="text-center">
                  {item.updatedate ? moment(item.updatedate)
                      // .add(543, "years")
                      .format("DD-MM-YYYY HH:mm") : ""}
              </td>
          ),
          "createdate": (item) => (
              <td className="text-center">
                  {item.createdate ? moment(item.createdate)
                      // .add(543, "years")
                      .format("DD-MM-YYYY HH:mm") : ""}
              </td>
          ),
          "createby": (item) => (
            <td>
              {item.createby ? item.createby : ""}
            </td>
          ),
          "updateby": (item) => (
            <td>
              {item.updateby ? item.updateby : ""}
            </td>
          ),
          'option':
            (item) => (
              <td className="center">
                <CFormGroup variant="checkbox" className="checkbox">
                  <CInputCheckbox
                    id="checkbox1"
                    name="checkbox1"
                    onChange={(e) => checkData(e, item)}
                  />
                </CFormGroup>
              </td>
            ),
          'option2':
            (item) => (
              <td className="center">
                <CButton
                  className="btn-danger"
                  onClick={() => onUnapprove(item)}
                  color="danger-custom"
                  variant="reverse"
                >
                  ไม่อนุมัติ
                </CButton>
              </td>
            ),
          'delivery':
            (item) => (
              <td className="center">
                <span>กำลังจัดส่ง</span>
              </td>
            ),
        }}
      />
      <CModal
        show={modalConfirm}
        onClose={setModalConfirm}
        color="danger-custom"
      >
        <CModalHeader closeButton>
          <CModalTitle>เปลี่ยนสถานะ</CModalTitle>
        </CModalHeader>
        <CModalBody>
          คุณต้องการที่จะอนุมัติ ?
        </CModalBody>
        <CModalFooter>
          <CButton onClick={redeemFor} color="primary">ยืนยัน</CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => setModalConfirm(false)}
          >ยกเลิก</CButton>
        </CModalFooter>
      </CModal>

      <CModal
        show={modalConfirm2}
        onClose={setModalConfirm2}
        style={{ width: '133%' }}
      >
        <CModalHeader closeButton>
          <CModalTitle>หมายเหตุ</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={unapprove} action="" >
          <CModalBody >
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="desc">หมายเหตุ<span style={{ color: 'red' }}>*</span></CLabel>
                      <CTextarea onChange={(e) => { setNote(e.target.value) }} id="description" required />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow >
          </CModalBody>
          <CModalFooter>
            <CButton type="submit" color="primary">ยืนยัน</CButton>{' '}
            <CButton
              color="secondary"
              onClick={() => setModalConfirm2(false)}
            >ยกเลิก</CButton>
          </CModalFooter>
        </CForm >
      </CModal >
    </>
  );
};

export default Tables;