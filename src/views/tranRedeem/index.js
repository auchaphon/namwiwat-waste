import React, { useEffect, useState } from "react";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCardFooter,
    CCol,
    CRow,
    CButton,
    CLabel,
    CInput
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiTranRedeem from '../../api/ApiTranRedeem';
import ApiUser from '../../api/ApiUser';
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from '../../api/ApiTranLogBack';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import moment from "moment";

const TranRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    // const [empid, setEmpId] = useState("");
    // const [username, setUsername] = useState("");
    // const [userfullname, setUserfullname] = useState("");
    // const [email, setEmail] = useState("");
    // const userState = useSelector((state) => state.user);
    const [nameRedeem, setNameRedeem] = useState("");
    const [name, setName] = useState("");
    const [express, setExpress] = useState("");
    const [status, setStatus] = useState("");
    const [qDate, setQDate] = useState({ startDate: "", endDate: "" });
    const [qFocused, setQFocused] = useState();

    useEffect(() => {
        getData();
        func_add_log("Click Menu Tran Redeem", "{}")
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    // useEffect(() => {
    //     getData();
    //     return () => {
    //     }
    // }, [nameRedeem, name]);


    const getData = async () => {
        try {
            var model = {
                name: nameRedeem,
                firstname: name,
                fromdate: qDate.startDate,
                todate: qDate.endDate
            }
            func_add_log("Search Menu Tran Redeem", JSON.stringify(model))
            var result = await ApiTranRedeem.getAllTranRedeem(model);
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                var res = data.sort((a, b) => a.createdate > b.createdate ? -1: 1)
                for (let i = 0; i < res.length; i++) {
                    res[i].order = i + 1
                    res[i].express = "กำลังจัดส่ง"
                    if (res[i].redeem_status === "P") {
                        res[i].redeem_status = "รออนุมัติ"
                    }
                }
                setDataTable(res);
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         nameRedeem: nameRedeem,
        //         name: name,
        //         express: express,
        //         status: status,
        //     })
        // }
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (nameRedeem && nameRedeem.length > 0) {
        //         result = result.filter((x) => x.name.toLowerCase().includes(nameRedeem.toLowerCase()));
        //     }
        //     if (name && name.length > 0) {
        //         result = result.filter((x) => x.firstname.toLowerCase().includes(name.toLowerCase()));
        //     }
        //     if (express && express.length > 0) {
        //         result = result.filter((x) => x.express.toLowerCase().includes(express.toLowerCase()));
        //     }
        //     if (status && status.length > 0) {
        //         result = result.filter((x) => x.redeem_status.toLowerCase().includes(status.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setNameRedeem("")
        setName("");
        setQDate({startDate: "", endDate: ""})
        // setExpress("")
        // setStatus("")
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    const onExport = async () => {
      try {
        var model = {
            name: nameRedeem,
            firstname: name,
            fromdate: qDate.startDate,
            todate: qDate.endDate
        }
        func_add_log("Export Tran Redeem", JSON.stringify(model))
        var result = await ApiTranRedeem.onExportPending(model);
        if (result.status === 200) {
          window.open(result.data)
        }
      } catch (error) { }
    };

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>ชื่อของรางวัล</CLabel>
                                    <CInput onChange={e => setNameRedeem(e.target.value)} value={nameRedeem} id="name" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>ชื่อลูกค้า</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="point" />
                                </CCol>
                                <CCol sm="4">
                                    <CLabel>วันที่ขอแลกของรางวัล</CLabel>
                                    <DateRangePicker
                                        displayFormat="DD/MM/YYYY"
                                        small
                                        block
                                        align="center"
                                        startDatePlaceholderText="ตั้งแต่"
                                        startDate={qDate.startDate}
                                        startDateId="startQDate"
                                        endDate={qDate.endDate}
                                        endDateId="endQDate"
                                        endDatePlaceholderText="ถึง"
                                        onDatesChange={(value) => setQDate(value)}
                                        focusedInput={qFocused}
                                        onFocusChange={(focusedInput) =>
                                            setQFocused(focusedInput)
                                        }
                                        orientation="horizontal"
                                        openDirection="down"
                                        minimumNights={0}
                                        
                                        isOutsideRange={() => false}
                                    />
                                </CCol>
                                {/* <CCol sm="2">
                                    <CLabel>สถานะการจัดส่ง</CLabel>
                                    <CInput onChange={e => setExpress(e.target.value)} value={express} id="statusExpress" />
                                </CCol> */}
                                {/* <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CInput onChange={e => setStatus(e.target.value)} value={status} id="statusRedeem" />
                                </CCol> */}
                                {/* <CCol sm="2"></CCol> */}
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                        <CButton
                                            className="mt-1"
                                            color="success-custom"
                                            variant="reverse"
                                            block
                                            onClick={() => onExport()}
                                        >
                                            <b> ออกรายงาน </b>
                                        </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b>ของรางวัลที่รอการอนุมัติ</b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} func_add_log={func_add_log} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default TranRedeem;
