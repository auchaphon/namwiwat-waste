import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CInputCheckbox,
    CCard,
    CSelect,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea,
    CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterPromotion from '../../api/ApiMasterPromotion';
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiTranLogBack from '../../api/ApiTranLogBack';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } , func_add_log}) => {
    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [code, setCode] = useState('')
    const [description, setDescription] = useState('')
    const [startCondition, setStartCondition] = useState()
    const [checkPerUser, setCheckPerUser] = useState()
    const [checkPercent, setCheckPercent] = useState()
    const [limitUser, setlimitUser] = useState('')
    const [amount, setAmount] = useState()
    const [amountPercent, setAmountPercent] = useState()
    const [campaignId, setCampaignId] = useState('')
    const [name, setName] = useState('')
    const [name_en, setNameEn] = useState('')

    const [point, setPoint] = useState('')
    const [dataSelected, setDataSelected] = useState('');

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const [campaign, setCampaign] = useState([])

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const getAllCampaign = async () => {
        try {
            const result = await ApiMasterCampaign.getAllMtCampaign();
            if (result.status === 200) {
                const { data } = result.data;
                setCampaign(data);
            }
        } catch (error) {

        }
    }

    const checkPer = (e) => {
        console.log(e.target.checked)
        const value = e.target.checked
        if (value) {
            setCheckPercent(value)
        }
        setCheckPercent(value)
    }

    const newDetails = () => {
        getAllCampaign()
        setCode('')
        setName('')
        setNameEn('')
        setDescription('')
        setStartCondition('')
        setCheckPerUser(false)
        setCheckPercent(false)
        setlimitUser('')
        setAmount('')
        setAmountPercent('')
        setStartDate(new Date())
        setEndDate(new Date())
        setCampaignId('')
        toggleNew();
    }

    const onSubmitNew = (e) => {
        e.preventDefault();
        console.log(checkPercent)
        const model = {
            "code": code,
            "name": name,
            "name_en": name_en,
            "description": description,
            "is_persent": checkPercent,
            "start_condition": startCondition,
            "is_peruser": checkPerUser,
            "amount": (checkPercent ? 0 : amount),
            "amount_percent": (checkPercent ? amountPercent : 0),
            "limit_user": limitUser,
            "start_date": startDate.toISOString(),
            "end_date": endDate.toISOString(),
            "createby": 0,
            "updateby": 0,
            "campaign_id": campaignId,
        }
        newDataMtRedeem(model);
    }

    const newDataMtRedeem = async (data) => {
        try {
            func_add_log("Insert Promotion", JSON.stringify(data))
            const result = await ApiMasterPromotion.insertDataPromotion(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const editDetails = (record) => {
        toggle();
        getAllCampaign()
        setDataSelected(record);
        setCode(record.code)
        setName(record.name)
        setNameEn(record.name_en)
        setDescription(record.description)
        setStartCondition(record.start_condition)
        setCheckPerUser(record.is_peruser)
        setCheckPercent(record.is_persent)
        setlimitUser(record.limit_user)
        setAmount(record.amount)
        setAmountPercent(record.amount_percent)
        setStartDate(new Date(record.start_date))
        setEndDate(new Date(record.end_date))
        setCampaignId(record.campaign_id)
    }

    const onSubmitEdit = (e) => {
        e.preventDefault();
        const model = {
            "id": dataSelected.id,
            "code": code,
            "name": name,
            "name_en": name_en,
            "description": description,
            "is_persent": checkPercent,
            "start_condition": startCondition,
            "is_peruser": checkPerUser,
            "amount": (checkPercent ? 0 : amount),
            "amount_percent": (checkPercent ? amountPercent : 0),
            "limit_user": limitUser,
            "start_date": startDate.toISOString(),
            "end_date": endDate.toISOString(),
            "updateby": 0,
            "campaign_id":campaignId,
        }
        editDataMtRedeem(model);
    }

    const func_change_status = async (e, record) => {
      record.recordstatus = e ? "A" : "I"
      delete record.order;
      editDataMtRedeem(record);
    };

    const editDataMtRedeem = async (data) => {
        try {
            func_add_log("Update Promotion", JSON.stringify(data))
            const result = await ApiMasterPromotion.updateMtPromotion(data);
            if (result.status === 200) {
                const { data } = result.data;
                setModalEdit(false);
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
    }

    const onDelete = async () => {
        try {
            const id = dataSelected.id
            func_add_log("Delete Promotion", JSON.stringify(id))
            const result = await ApiMasterPromotion.deleteMtPromotion(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }


    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { minWidth: '50px' } },
        {
            key: 'option',
            label: '',
            _style: { minWidth: '50px' },
            filter: false,
        },
        {
          key: "record_status",
          label: "สถานะ",
          _style: { minWidth: "150px", textAlign: "center" },
          filter: false,
        },
        { key: 'code', label: "โค้ดส่วนลด", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'name', label: "ชื่อโปรโมชัน", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'name_en', label: "ชื่อโปรโมชัน EN", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'start_condition', label: "ราคาขั้นต่ำ (บาท)", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'limit_user', label: "จำนวนผู้ใช้", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'amount', label: "ส่วนลด (บาท)", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'amount_percent', label: "ส่วนลด (%)", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'start_date', label: "วันที่เริ่ม", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'end_date', label: "วันที่สิ้นสุด", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'updatedate', label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { minWidth: '150px', textAlign: "center" } },

    ]

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton
                        onClick={() => newDetails()}
                        color="primary-custom"
                        variant="reverse"
                    >
                        <CIcon name="cil-plus" />
                        <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
                    </CButton>
                </CCol>
            </CRow>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'name':
                        (item) => (
                            <td>
                                {item.name ?? ""}
                            </td>
                        ),
                    'name_en':
                        (item) => (
                            <td>
                                {item.name_en ?? ""}
                            </td>
                        ),
                    "start_date": (item) => (
                        <td className="text-center">
                            {item.start_date ? moment(item.start_date)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "end_date": (item) => (
                        <td className="text-center">
                            {item.end_date ? moment(item.end_date)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td className="text-center">
                            {item.updatedate ? moment(item.updatedate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createby": (item) => (
                        <td>
                            {item.createby ? item.createby : ""}
                        </td>
                    ),
                    "updateby": (item) => (
                        <td>
                            {item.updateby ? item.updateby : ""}
                        </td>
                    ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.id}`} src={edit_icon} />
                                    </CButton>
                                    {/* <CButton
                                        className="ml-3"
                                        color="danger-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                        onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                    >
                                        <img id={`delete-${item.id}`} src={trash_icon} />
                                    </CButton> */}
                                </CButtonGroup>
                            </td>
                        ),
                    record_status: (item, index) => (
                        <td align="center">
                        <CSwitch
                            key={index}
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={item.recordstatus == "A" ? true : false}
                            onChange={(e) =>
                            func_change_status(e.target.checked, item)
                            }
                        />
                        </td>
                    ),

                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> โค้ดส่วนลด <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setCode(e.target.value)} value={code} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name"> ชื่อโปรโมชัน <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name_en"> ชื่อโปรโมชัน EN <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setNameEn(e.target.value)} value={name_en} id="name_en" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectProduct">แคมเปญ<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CSelect custom name="selectProduct" id="selectProduct" onChange={(e) => setCampaignId(e.target.value)} value={campaignId}>
                                                <option>เลือก 1 รายการ</option>
                                                {campaign.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.title}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="desc">รายละเอียด</CLabel>
                                            <CTextarea onChange={e => setDescription(e.target.value)} value={description} id="desc" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">ราคาขั้นต่ำในการใช้โค้ด (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setStartCondition(e.target.value)} value={startCondition} id="value" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">จำนวนผู้ใช้ (คน)<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setlimitUser(e.target.value)} value={limitUser} id="limit_user" required />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputCheckbox id="checkbox1" name="checkbox1" onChange={(e) => setCheckPerUser(e.target.checked)} checked={checkPerUser} />
                                            <CLabel htmlFor="checkbox1">ส่วนลดต่อคน ?</CLabel>
                                        </CFormGroup>
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputCheckbox id="checkbox2" name="checkbox2" onChange={(e) => checkPer(e)} checked={checkPercent} />
                                            <CLabel htmlFor="checkbox2">ส่วนลดเป็น % ?</CLabel>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    {checkPercent ?
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput value={amount} id="amount" required disabled />
                                            </CFormGroup >
                                        </CCol>
                                        :
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput onChange={e => setAmount(e.target.value)} value={amount} id="amount" required />
                                            </CFormGroup >
                                        </CCol>
                                    }
                                    {checkPercent ?
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (%)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput onChange={e => setAmountPercent(e.target.value)} value={amountPercent} id="amount_percent" required />
                                            </CFormGroup >
                                        </CCol>
                                        :
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (%)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput id="amount_percent" value={amountPercent} disabled required />
                                            </CFormGroup >
                                        </CCol>
                                    }
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

            <CModal
                show={modalNew}
                onClose={toggleNew}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitNew} action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> โค้ดส่วนลด <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setCode(e.target.value)} value={code} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name"> ชื่อโปรโมชัน <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="name_en"> ชื่อโปรโมชัน EN <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setNameEn(e.target.value)} value={name_en} id="name_en" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectProduct">แคมเปญ<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CSelect custom name="selectNewCampaign" id="selectNewCampaign" onChange={(e) => setCampaignId(e.target.value)}>
                                                <option>เลือก 1 รายการ</option>
                                                {campaign.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.title}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="desc">รายละเอียด</CLabel>
                                            <CTextarea onChange={e => setDescription(e.target.value)} value={description} id="description" />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">ราคาขั้นต่ำในการใช้โค้ด (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setStartCondition(e.target.value)} value={startCondition} id="condition" />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">จำนวนผู้ใช้ (คน)<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setlimitUser(e.target.value)} value={limitUser} id="limit_user" required />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputCheckbox id="checkbox1" name="checkbox1" onChange={(e) => setCheckPerUser(e.target.checked)} checked={checkPerUser} />
                                            <CLabel htmlFor="checkbox1">ส่วนลดต่อคน ?</CLabel>
                                        </CFormGroup>
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputCheckbox id="checkbox2" name="checkbox2" onChange={(e) => checkPer(e)} checked={checkPercent} />
                                            <CLabel htmlFor="checkbox2">ส่วนลดเป็น % ?</CLabel>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    {checkPercent ?
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput value={amount} id="amount" required disabled />
                                            </CFormGroup >
                                        </CCol>
                                        :
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (บาท)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput onChange={e => setAmount(e.target.value)} value={amount} id="amount" required />
                                            </CFormGroup >
                                        </CCol>
                                    }
                                    {checkPercent ?
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (%)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput onChange={e => setAmountPercent(e.target.value)} value={amountPercent} id="amount_percent" required />
                                            </CFormGroup >
                                        </CCol>
                                        :
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel htmlFor="value">จำนวน (%)<span style={{ color: 'red' }}>*</span></CLabel>
                                                <CInput id="amount_percent" value={amountPercent} disabled required />
                                            </CFormGroup >
                                        </CCol>
                                    }
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"

                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
