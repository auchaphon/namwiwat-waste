import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiMasterPromotion from "../../api/ApiMasterPromotion";
import ApiUser from "../../api/ApiUser";

const MasterRedeem = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");
  const [startCondition, setStartCondition] = useState("");
  const [limitUser, setLimitUser] = useState("");
  const [amount, setAmount] = useState("");
  const [amountPercent, setAmountPercent] = useState("");
  const [record_status, set_record_status] = useState("A");

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Promotion", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [code, startCondition, limitUser, amount, amountPercent]);

  const getData = async () => {
    try {
        var model = {
            code: code,
            startCondition: startCondition,
            limitUser: limitUser,
            amount: amount,
            amountPercent: amountPercent,
            recordstatus: record_status
        }
        func_add_log("Search Menu Master Promotion", JSON.stringify(model))
        const result = await ApiMasterPromotion.getAllMtPromotion({
            code: code,
            recordstatus: record_status
        });
        if (result.status === 200) {
            const { data } = result.data;
            console.log(data)
            setRawDataTable(data);
            var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1);
            if (startCondition && startCondition.length > 0) {
                res = res.filter((x) => x.start_condition.toLowerCase().includes(startCondition.toLowerCase()));
            }
            if (limitUser && limitUser.length > 0) {
                res = res.filter((x) => x.limit_user.toString().toLowerCase().includes(limitUser.toLowerCase()));
            }
            if (amount && amount.length > 0) {
                res = res.filter((x) => x.amount.toLowerCase().includes(amount.toLowerCase()));
            }
            if (amountPercent && amountPercent.length > 0) {
                res = res.filter((x) => x.amount_percent.toLowerCase().includes(amountPercent.toLowerCase()));
            }
            for (let i = 0; i < res.length; i++) {
                res[i].order = i + 1
            }
            setDataTable(res);
        }
    } catch (error) {}
  };

  const onSearch = async () => {
    getData();
    // var model = {
    //     activity_detail: "Menu Config Search",
    //     json: JSON.stringify({
    //         code: code,
    //         startCondition: startCondition,
    //         limitUser: limitUser,
    //         amount: amount,
    //         amountPercent: amountPercent,
    //     })
    // }
    // // await ApiTranLogBack.addlog(model);
    // if (rawDataTable && rawDataTable.length > 0) {
    //     var result = rawDataTable;
    //     if (code && code.length > 0) {
    //         result = result.filter((x) => x.code.toLowerCase().includes(code.toLowerCase()));
    //     }
    //     if (startCondition && startCondition.length > 0) {
    //         result = result.filter((x) => x.start_condition.toLowerCase().includes(startCondition.toLowerCase()));
    //     }
    //     if (limitUser && limitUser.length > 0) {
    //         result = result.filter((x) => x.limit_user.toString().toLowerCase().includes(limitUser.toLowerCase()));
    //     }
    //     if (amount && amount.length > 0) {
    //         result = result.filter((x) => x.amount.toLowerCase().includes(amount.toLowerCase()));
    //     }
    //     if (amountPercent && amountPercent.length > 0) {
    //         result = result.filter((x) => x.amount_percent.toLowerCase().includes(amountPercent.toLowerCase()));
    //     }
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i + 1
    //     }
    //     setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    setCode("");
    setStartCondition("");
    setLimitUser("");
    setAmount("");
    setAmountPercent("");
    set_record_status("A");
    // var result = rawDataTable;
    // for (let i = 0; i < result.length; i++) {
    //     result[i].order = i + 1
    // }
    // setDataTable(result);
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>โค้ดส่วนลด</CLabel>
                  <CInput
                    onChange={(e) => setCode(e.target.value)}
                    value={code}
                    id="code"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ราคาขั้นต่ำ</CLabel>
                  <CInput
                    onChange={(e) => setStartCondition(e.target.value)}
                    value={startCondition}
                    id="condition"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>จำนวนผู้ใช้</CLabel>
                  <CInput
                    onChange={(e) => setLimitUser(e.target.value)}
                    value={limitUser}
                    id="limit_user"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ส่วนลด (บาท)</CLabel>
                  <CInput
                    onChange={(e) => setAmount(e.target.value)}
                    value={amount}
                    id="amount"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ส่วนลด (%)</CLabel>
                  <CInput
                    onChange={(e) => setAmountPercent(e.target.value)}
                    value={amountPercent}
                    id="amount_percent"
                  />
                </CCol>
                {/* <CCol sm="8"></CCol> */}
                <CCol sm="2">
                    <CLabel>สถานะ</CLabel>
                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                        <option value="">ทั้งหมด</option>
                        <option value="A">Active</option>
                        <option value="I">Inactive</option>
                    </CSelect>
                </CCol>
              </CRow>
              <CRow className="mt-2">
                <CCol sm="9"></CCol>
                <CCol sm="3">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> การจัดการโปรโมชัน </b>
            </CCardHeader>
            <CCardBody>
              <Tables
                refreshData={getData}
                data={dataTable}
                func_add_log={func_add_log}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default MasterRedeem;
