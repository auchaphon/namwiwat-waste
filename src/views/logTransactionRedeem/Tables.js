import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CInputCheckbox,
    CCard,
    CSelect,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
// import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
// import parse from 'html-react-parser';
import moment from 'moment';
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import ApiMasterPromotion from '../../api/ApiMasterPromotion';
// import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiTranRedeem from '../../api/ApiTranRedeem';

const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const get_redeem_status = (status) => {
    switch (status) {
        case "P": return "รออนุมัติ";
        case "A": return "อนุมัติ";
        case "C": return "ไม่อนุมัติ";
    }
}

const DemoTable = ({ data = [], refreshData = () => { }, setData }) => {

    const fields = [
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: 'username', label: 'รหัสลูกค้า', _style: { minWidth: "150px", textAlign: "center" } },
        { key: 'fullname', label: "ชื่อ-นามสกุลของลูกค้า", _style: { minWidth: "200px", textAlign: "center" } },
        { key: 'tracking_number', label: "หมายเลขพัสดุ", _style: { minWidth: "200px", textAlign: "center" } },
        { key: 'phonenumber', label: "หมายเลขโทรศัพท์", _style: { minWidth: "150px", textAlign: "center" } },
        { key: 'email', label: "อีเมล", _style: { minWidth: "150px", textAlign: "center" } },
        // { key: 'redeem_id', label: "รหัสสินค้า", _style: { width: '5%' } },
        { key: 'redeem_name', label: "ชื่อสินค้า", _style: { minWidth: "300px", textAlign: "center" } },
        { key: 'point', label: "จำนวนแต้มที่ถูกใช้", _style: { minWidth: "150px", textAlign: "center" } },
        { key: 'point_amt', label: "จำนวนแต้มคงเหลือ", _style: { minWidth: "200px", textAlign: "center" } },
        { key: "redeem_status_text", label: "สถานะ", _style: { minWidth: "150px", textAlign: "center" } },
        { key: "note", label: "เหตุผล", _style: { minWidth: "150px", textAlign: "center" } },
        { key: 'createdate', label: "วันที่ขอแลกของรางวัล", _style: { minWidth: "200px", textAlign: "center" } },
        // { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: "150px" } },
    ]

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'option':
                        (item) => (
                            <td className="center">
                                {item.redeem_status == "A" && (
                                    <CButtonGroup>
                                        <CButton
                                            className="ml-3"
                                            color="warning-custom"
                                            variant="reverse"
                                            shape="square"
                                            size="sm"
                                            title="พิมพ์ใบปะหน้า"
                                            // onClick={() => {
                                            //     history.push(`/invoicedetail?pid=${item.order_ref}`);
                                            // }}
                                        >
                                            <CIcon name="cilPrint"></CIcon>
                                        </CButton>
                                    </CButtonGroup>
                                )}
                            </td>
                        ),
                    "tracking_number": (item, index) => (
                        <td>
                            <CInput 
                                id={`item-${item.id}`} 
                                value={item.redeem_status == "A" ? item.tracking_number : ""}
                                disabled={item.redeem_status != "A"}
                                onChange={(e) =>{
                                    const oldData = [...data];
                                    oldData[index].tracking_number = e.target.value;
                                    setData(oldData);
                                    // await ApiTransaction.saveOrglevelPlant(plantList)
                                }}
                                onBlur={ async (e) => {
                                    try {
                                        const result = await ApiTranRedeem.updateTracking(item)
                                        if (result.status === 200) {
                                            refreshData();
                                        }
                                    } catch (error) {
                                        console.log("error::", error);
                                        console.log("error.response::", error.response);
                                        console.log("error.response.data::", error.response.data);
                                        Swal.fire({
                                            icon: "error",
                                            title: error.response.data,
                                        });
                                    }
                                }}
                            />
                        </td>
                    ),
                    "redeem_status_text": (item) => (
                        <td>
                            {/* {get_redeem_status(item.redeem_status)} */}
                            {item.redeem_status_text}
                        </td>
                    ),
                    "note": (item) => (
                        <td>
                            {item.redeem_status == "C" ? item.note ?? "" : ""}
                        </td>
                    ),
                    "point": (item) => (
                        <td className="text-center">
                            {item.point ?? ""}
                        </td>
                    ),
                    "point_amt": (item) => (
                        <td className="text-center">
                            {item.point_amt ?? ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                }}
            />
        </>
    )
}

export default DemoTable
