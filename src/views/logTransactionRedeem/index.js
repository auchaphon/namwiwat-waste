import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput,
    CSelect
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranRedeem from '../../api/ApiTranRedeem';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ApiTranLogBack from '../../api/ApiTranLogBack';
import moment from "moment";

const LogTransactionRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [name, setName] = useState("");
    const [fullname, setFullName] = useState("");
    const [redeem_status, set_redeem_status] = useState("");
    const [qDate, setQDate] = useState({ startDate: "", endDate: "" });
    const [qFocused, setQFocused] = useState();
    const [state, setState] = useState({
        redeem_id: "",
        redeem_name: "",
        redeem_status: "",
    });

    useEffect(() => {
        getData();
        func_add_log("Click Menu Log Tran Redeem", "{}")

        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    // useEffect(() => {
    //     getData();

    //     return () => {
    //     }
    // }, [fullname, name]);

    const getData = async () => {
        try {
            var model = {
                name: name,
                fullname: fullname,
                redeem_status: redeem_status,
                fromdate: qDate.startDate,
                todate: qDate.endDate
            }
            func_add_log("Search Menu Log Tran Redeem", JSON.stringify(model))
            const result = await ApiTranRedeem.getLogTranRedeem(model);
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                for (let i = 0; i < data.length; i++) {
                    data[i].order = i + 1
                    if (data[i].redeem_status === "P") {
                        data[i].redeem_status_text = "รออนุมัติ"
                    }
                    else if (data[i].redeem_status === "A") {
                        data[i].redeem_status_text = "อนุมัติ"
                    }
                    else if (data[i].redeem_status === "C") {
                        data[i].redeem_status_text = "ไม่อนุมัติ"
                    }
                }
                setDataTable(data.sort((a, b) => a.createdate > b.createdate ? -1: 1));
            }

        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         redeem_id: state.redeem_id,
        //         redeem_name: state.redeem_name
        //     })
        // }
        // // await ApiTranLogBack.addlog(model);
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (state.redeem_id && state.redeem_id.length > 0) {
        //         result = result.filter((x) => x.redeem_id.toLowerCase().includes(state.redeem_id.toLowerCase()));
        //     }
        //     if (state.redeem_name && state.redeem_name.length > 0) {
        //         result = result.filter((x) => x.redeem_name.toLowerCase().includes(state.redeem_name.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setFullName("")
        setName("")
        setQDate({startDate: "", endDate: ""})
        // setState({
        //     redeem_id: "",
        //     redeem_name: "",
        // });
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    const onExport = async () => {
      try {
          var model ={
            name: name,
            fullname: fullname,
            redeem_status: redeem_status,
            fromdate: qDate.startDate,
            todate: qDate.endDate
        }
        func_add_log("Export Log Tran Redeem", JSON.stringify(model))
        var result = await ApiTranRedeem.onExport(model);
        if (result.status === 200) {
          window.open(result.data)
          // const link = document.createElement("a");
          // link.href = result.data;
          // var fileName = `customer_report_${moment().format(
          //   "yyyyMMDD"
          // )}.xlsx`;
          // link.setAttribute("download", fileName); //or any other extension
          // document.body.appendChild(link);
          // link.click();
        }
      } catch (error) { }
    };

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                {/* <CCol sm="2">
                                    <CLabel>รหัสสินค้า</CLabel>
                                    <CInput onChange={e => setState({ ...state, redeem_id: e.target.value })} value={state.redeem_id} id="redeem_id" />
                                </CCol> */}
                                <CCol sm="2">
                                    <CLabel>ชื่อ-นามสกุลของลูกค้า</CLabel>
                                    <CInput onChange={e => setFullName(e.target.value)} value={fullname} id="fullname" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>ชื่อสินค้า</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="name" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>วันที่ขอแลกของรางวัล</CLabel>
                                    <DateRangePicker
                                        displayFormat="DD/MM/YYYY"
                                        small
                                        block
                                        align="center"
                                        startDatePlaceholderText="ตั้งแต่"
                                        startDate={qDate.startDate}
                                        startDateId="startQDate"
                                        endDate={qDate.endDate}
                                        endDateId="endQDate"
                                        endDatePlaceholderText="ถึง"
                                        onDatesChange={(value) => setQDate(value)}
                                        focusedInput={qFocused}
                                        onFocusChange={(focusedInput) =>
                                            setQFocused(focusedInput)
                                        }
                                        orientation="horizontal"
                                        openDirection="down"
                                        minimumNights={0}
                                        
                                        isOutsideRange={() => false}
                                    />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_redeem_status(e.target.value)} value={redeem_status} id="redeem_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="P">รออนุมัติ</option>
                                        <option value="A">อนุมัติ</option>
                                        <option value="C">ไม่อนุมัติ</option>
                                    </CSelect>
                                </CCol>
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="success-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onExport()}
                                            >
                                                <b> ออกรายงาน </b>
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> รายการแลกของรางวัล </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} setData={setDataTable} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default LogTransactionRedeem