import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiMasterRedeem from "../../api/ApiMasterRedeem";
import ApiMasterCampaign from "../../api/ApiMasterCampaign";
import ApiUser from "../../api/ApiUser";

const MasterCampaign = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");
  const [name, setName] = useState("");
  const [record_status, set_record_status] = useState("A");

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Campaign", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [name]);

    const getData = async () => {
        try {
            var model = {
                title: name,
                record_status: record_status
            }
            func_add_log("Search Menu Master Campaign", JSON.stringify(model))
            const result = await ApiMasterCampaign.getAllMtCampaign(model);
            if (result.status === 200) {
                const { data } = result.data;
                console.log(data)
                setRawDataTable(data);
                var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1)
                for (let i = 0; i < res.length; i++) {
                    const checkupdate = 0
                    const checkcreate = 0
                    res[i].order = i + 1
                }
                setDataTable(res);
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         name: name,
        //     })
        // }
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (name && name.length > 0) {
        //         result = result.filter((x) => x.title.toLowerCase().includes(name.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setName("");
        set_record_status("A");
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>หัวข้อ</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="name" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                    </CSelect>
                                </CCol>
                                <CCol sm="4"></CCol>
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="6">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>

        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> การจัดการแคมเปญ </b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default MasterCampaign;
