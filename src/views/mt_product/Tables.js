import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterTag from "../../api/ApiMasterTag";
import Swal from "sweetalert2";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ApiMasterProduct from "../../api/ApiMasterProduct";
import { WEB_API } from "../../env";
import { CheckFile } from "../../utils/uploadfile";

const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => {}, func_add_log }) => {
  const history = useHistory();
  const [dataSelected, setDataSelected] = useState({});
  const [modal, setModal] = useState(false);
  const [modalConfirmDel, setModalConfirmDel] = useState(false);
  const [modalConfirmEdit, setModalConfirmEdit] = useState(false);
  const userState = useSelector((state) => state.user);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [sku, setSku] = useState("");
  const [productName, setproductName] = useState("");
  // const [edit_icon_icon, setEditIcon] = useState(edit_icon);

  useEffect(() => {
    return () => {};
  }, []);

  const toggle = () => {
    setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%", textAlign: "center" } },
    {
      key: "option",
      label: "",
      _style: { width: "1%", textAlign: "center" },
      filter: false,
    },
    {
      key: "record_status",
      label: "สถานะ",
      _style: { minWidth: "150px", textAlign: "center" },
      filter: false,
    },
    { key: "sku", label: "รหัสสินค้า", _style: { minWidth: "250px", textAlign: "center" } },
    { key: "name", label: "ชื่อสินค้า", _style: { minWidth: "300px" , textAlign: "center"} },
    { key: "name_en", label: "ชื่อสินค้า EN", _style: { minWidth: "300px", textAlign: "center" } },
    { key: "regular_price", label: "ราคา", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "description", label: "รายละเอียด", _style: { minWidth: "300px", textAlign: "center" } },
    { key: "description_en", label: "รายละเอียด EN", _style: { minWidth: "300px", textAlign: "center" } },
    { key: "name_parent", label: "หมวดหมู่", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "sub_parent", label: "หมวดหมู่ย่อย", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "createdate", label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: "200px", textAlign: "center" } },
    { key: "createby", label: "ผู้สร้างข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "updatedate", label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: "200px", textAlign: "center" } },
    { key: "updateby", label: "ผู้แก้ไขข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
  ];

  const toggleConfirmDel = () => {
    setModalConfirmDel(!modalConfirmDel);
  };
  const toggleConfirmEdit = () => {
    setModalConfirmEdit(!modalConfirmEdit);
  };
  const insertData = (record) => {
    history.push("/Mt_productEdit");
  };

  // const onSubmit = (e) => {
  //   e.preventDefault();
  //   const model = {
  //     id: id,
  //     name: name,
  //     sku: sku,
  //   };
  //   saveData(model);
  //   console.log(model);
  // };

  const func_change_status = async (e, record) => {
    record.record_status = e ? "A" : "I"
    update_status(record);
  };

  const update_status = async (data) => {
    try {
      const result = await ApiMasterProduct.update_status(data);
      if (result.status === 200) {
        const { data } = result.data;
        toggleConfirmEdit();
        Swal.fire({
          icon: "success",
          title: "บันทึกสำเร็จ",
          timer: 2000,
        }).then((success) => {
          refreshData();
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const func_attachFile = async (e, id) => {
    if (e.files.length > 0) {
      var file = e.files[0];

      var type = ["image/png", "image/jpg", "image/jpeg"];
      var message = "";
      var check = CheckFile({
        file,
        size: 10,
        type: type,
        message,
      });
      if (check) {
        func_uploadFile(id, file);
      }
      e.value = "";
    }
  };

  const func_uploadFile = async (id, file) => {
    var formdata = new FormData();
    formdata.append("id", id);
    formdata.append("file", file);

    try {
      var res = await ApiMasterProduct.uploadFile(formdata);

      const { data, status } = res.data;
      refreshData();
    } catch (err) {
      if (err.response) {
        if (err.response.status === 400) {
          const { message } = err.response.data;
        }
      }
    }
  };

  const deleteDetails = (record) => {
    toggleConfirmDel();
    setproductName(record.name);
    setDataSelected(record);
  };

  const onDelete = async () => {
    try {
      const model = {
        id: dataSelected.id,
      };
      func_add_log("Delete Product", JSON.stringify(model))
      const result = await ApiMasterProduct.delete(model);
      if (result.status === 200) {
        const { data } = result.data;
        toggleConfirmDel();
        Swal.fire({
          icon: "success",
          title: "ลบสำเร็จ",
          timer: 2000,
        }).then((success) => {
          refreshData();
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => {
              insertData();
            }}
            color="primary-custom"
            variant="reverse"
          >
            <CIcon name="cil-plus" />
            <span className="ml-2">
              <b>เพิ่มข้อมูล</b>
            </span>
          </CButton>
        </CCol>
      </CRow>
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          record_status: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.record_status == "A" ? true : false}
                onChange={(e) =>
                  func_change_status(e.target.checked, item)
                }
              />
            </td>
          ),
          // 'username':
          //     (item) => (
          //         <td>
          //             <CLink href={`/#/user_role?id=${item.id}&user=${item.username}`}>{item.username}</CLink>
          //         </td>
          //     ),
          name_parent: (item) => (
            <td>
              {item.name_parent ?? ""}
            </td>
          ),
          sub_parent: (item) => (
            <td>
              {item.sub_parent ?? ""}
            </td>
          ),
          name: (item) => (
            <td>
              {item.name ?? ""}
            </td>
          ),
          name_en: (item) => (
            <td>
              {item.name_en ?? ""}
            </td>
          ),
          description: (item) => (
            <td>
              {item.description ?? ""}
            </td>
          ),
          description_en: (item) => (
            <td>
              {item.description_en ?? ""}
            </td>
          ),
          "updatedate": (item) => (
              <td className="text-center">
                  {item.updatedate ? moment(item.updatedate)
                      // .add(543, "years")
                      .format("DD-MM-YYYY HH:mm") : ""}
              </td>
          ),
          "createdate": (item) => (
              <td className="text-center">
                  {item.createdate ? moment(item.createdate)
                      // .add(543, "years")
                      .format("DD-MM-YYYY HH:mm") : ""}
              </td>
          ),
          "createby": (item) => (
            <td>
              {item.createby ? item.createby : ""}
            </td>
          ),
          "updateby": (item) => (
            <td>
              {item.updateby ? item.updateby : ""}
            </td>
          ),
          option: (item) => (
            <td className="center">
              <CButtonGroup>
                <CButton
                  color="primary-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    history.push(`/Mt_productEdit?pid=${item.id}`);
                  }}
                  onMouseOver={(e) =>
                    (document.getElementById(`edit-${item.id}`).src =
                      edit_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`edit-${item.id}`).src = edit_icon)
                  }
                >
                  <img id={`edit-${item.id}`} src={edit_icon} />
                </CButton>
{/* 
                <CButton
                  className="ml-3"
                  color="danger-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    deleteDetails(item);
                  }}
                  onMouseOver={(e) =>
                    (document.getElementById(`delete-${item.id}`).src =
                      trash_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`delete-${item.id}`).src =
                      trash_icon)
                  }
                >
                  <img id={`delete-${item.id}`} src={trash_icon} />
                </CButton> */}
              </CButtonGroup>
            </td>
          ),
          link: (item) => (
            <td>
              {console.log(item)}
              <input
                className="mb-3"
                type="file"
                onChange={(e) => {
                  func_attachFile(e.target, item.id);
                }}
              ></input>
              {item.signaturePath != "" && item.link != null && (
                <img style={{ width: "150px" }} src={WEB_API + item.link}></img>
              )}
            </td>
          ),
        }}
      />

      <CModal
        show={modalConfirmDel}
        onClose={setModalConfirmDel}
        color="danger-custom"
      >
        <CModalHeader closeButton>
          <CModalTitle>ลบรายการ : {productName}</CModalTitle>
        </CModalHeader>
        <CModalBody>คุณต้องการลบรายการนี้ ?</CModalBody>
        <CModalFooter>
          <CButton onClick={onDelete} color="primary">
            ยืนยัน
          </CButton>{" "}
          <CButton color="secondary" onClick={() => setModalConfirmDel(false)}>
            ยกเลิก
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default Tables;
