import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterProduct from "../../api/ApiMasterProduct";
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

const Mt_product = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [sku, setSku] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [parent, setParent] = useState("");
  const [subParent, setSubParent] = useState("");
  const [record_status, set_record_status] = useState("A");
  const userState = useSelector((state) => state.user);
  const [qDate, setQDate] = useState({ startDate: "", endDate: "" });
  const [qFocused, setQFocused] = useState();

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Product", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //   getData();
  //   return () => { };
  // }, [sku, name, description, parent, subParent]);

  const getData = async () => {
    try {
      var model = {
        sku: sku,
        name: name,
        description: description,
        parent: parent,
        subParent: subParent,
        fromdate: qDate.startDate,
        todate: qDate.endDate,
        record_status: record_status
      };
      func_add_log("Search Menu Master Product", JSON.stringify(model));
      var result = await ApiMasterProduct.get(model);
      //   var result;
      //   if (userState.role.role_code == "admin"){
      //     result = await ApiUser.get();
      //   }else if (userState.role.role_code == "user_branch"){
      //     result = await ApiUser.getByBranch(userState.branch_id);
      //   }else if (userState.role.role_code == "user_team" || userState.role.role_code == "user"){
      //     result = await ApiUser.getByTeam(userState.team_id);
      //   }
      if (result.status === 200) {
        const { data } = result.data;
        setRawDataTable(data);
        var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1);
        for (let i = 0; i < res.length; i++) {
          res[i].order = i + 1;
          if (res[i].parent === "0") {
            res[i].name_parent = data[i].sub_parent
            res[i].sub_parent = "ว่าง"
          }
          // if (res[i].name == null) {
          //   res[i].name = "ว่าง";
          // }
          // if (res[i].description == null) {
          //   res[i].description = "ว่าง";
          // }
          // if (res[i].parent == null) {
          //   res[i].parent = "ว่าง";
          // }
        }
        setDataTable(res);
      }
    } catch (error) {}
  };
  const onSearch = async () => {
    getData();
    // var model = {
    //   activity_detail: "Menu Category Search",
    //   json: JSON.stringify({
    //     name: name,
    //     sku: sku,
    //   }),
    // };

    // console.log(name);
    // if (rawDataTable && rawDataTable.length > 0) {
    //   var result = rawDataTable;
    //   if (name && name.length > 0) {
    //     result = result.filter((x) =>
    //       x.name.toLowerCase().includes(name.toLowerCase())
    //     );
    //   }
    //   if (sku && sku.length > 0) {
    //     result = result.filter((x) =>
    //       x.sku.toLowerCase().includes(sku.toLowerCase())
    //     );
    //   }
    //   if (price && price.length > 0) {
    //     result = result.filter((x) =>
    //       x.regular_price.toLowerCase().includes(price.toLowerCase())
    //     );
    //   }
    //   if (parent && parent.length > 0) {
    //     result = result.filter((x) =>
    //       x.name_parent.toLowerCase().includes(parent.toLowerCase())
    //     );
    //   }
    //   if (subParent && subParent.length > 0) {
    //     result = result.filter((x) =>
    //       x.sub_parent.toLowerCase().includes(subParent.toLowerCase())
    //     );
    //   }
    //   for (let i = 0; i < result.length; i++) {
    //     result[i].order = i + 1;
    //   }
    //   setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    setName("");
    setParent("");
    setSku("");
    setDescription("");
    setSubParent("");
    setQDate({ startDate: "", endDate: "" });
    set_record_status("A");
    // var result = rawDataTable;
    // for (let i = 0; i < result.length; i++) {
    //   result[i].order = i + 1;
    // }
    // setDataTable(result);
  };

  const onExport = async () => {
    try {
      var model = {
        sku: sku,
        name: name,
        description: description,
        parent: parent,
        subParent: subParent,
        fromdate: qDate.startDate,
        todate: qDate.endDate,
      };
      func_add_log("Export Master Banner", JSON.stringify(model));
      var result = await ApiMasterProduct.onExport(model);
      if (result.status === 200) {
        window.open(result.data);
      }
    } catch (error) {}
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>รหัสสินค้า</CLabel>
                  <CInput
                    onChange={(e) => setSku(e.target.value)}
                    value={sku}
                    id="sku"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชื่อสินค้า</CLabel>
                  <CInput
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    id="name"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>รายละเอียด</CLabel>
                  <CInput
                    onChange={(e) => setDescription(e.target.value)}
                    value={description}
                    id="description"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>หมวดหมู่</CLabel>
                  <CInput
                    onChange={(e) => setParent(e.target.value)}
                    value={parent}
                    id="cate"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>หมวดหมู่ย่อย</CLabel>
                  <CInput
                    onChange={(e) => setSubParent(e.target.value)}
                    value={subParent}
                    id="subCate"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>วันที่สร้างข้อมูล</CLabel>
                  <DateRangePicker
                    displayFormat="DD/MM/YYYY"
                    small
                    block
                    align="center"
                    startDatePlaceholderText="ตั้งแต่"
                    startDate={qDate.startDate}
                    startDateId="startQDate"
                    endDate={qDate.endDate}
                    endDateId="endQDate"
                    endDatePlaceholderText="ถึง"
                    onDatesChange={(value) => setQDate(value)}
                    focusedInput={qFocused}
                    onFocusChange={(focusedInput) => setQFocused(focusedInput)}
                    orientation="horizontal"
                    openDirection="down"
                    minimumNights={0}
                    isOutsideRange={() => false}
                  />
                </CCol>
              </CRow>
              <CRow>
                <CCol sm="2">
                    <CLabel>สถานะ</CLabel>
                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                        <option value="">ทั้งหมด</option>
                        <option value="A">Active</option>
                        <option value="I">Inactive</option>
                    </CSelect>
                </CCol>
                <CCol sm="6"></CCol>
                <CCol sm="4" className="pt-4">
                  <CRow>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="success-custom"
                        variant="reverse"
                        block
                        onClick={() => onExport()}
                      >
                        <b> ออกรายงาน </b>
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>การจัดการสินค้า</b>
            </CCardHeader>
            <CCardBody>
              <Tables
                refreshData={getData}
                data={dataTable}
                func_add_log={func_add_log}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Mt_product;
