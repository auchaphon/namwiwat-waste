import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CInput,
  CForm,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CDataTable,
  CButtonGroup,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CFormGroup,
  CSwitch,
  CLabel,
} from "@coreui/react";

import { WEB_API } from "../../env";
import Select from "react-select";
import ApiMasterCategory from "../../api/ApiMasterCategory";
import ApiMasterTag from "../../api/ApiMasterTag";
import ApiMasterValue from "../../api/ApiMasterValue";
import ApiMasterFontColour from "../../api/ApiMasterFontColour";
import SelectFile from "../../Components/SelectFile";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import ApiMasterProduct from "../../api/ApiMasterProduct";
import Swal from "sweetalert2";

import { CheckFile } from "../../utils/uploadfile";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import { useHistory } from "react-router";
import CIcon from "@coreui/icons-react";
import helper from "../../utils/helper";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import moment from "moment";

const ProductEdit = (props) => {
  const history = useHistory();
  const [tabAddress, setTabAddress] = React.useState("1");
  const [tabImage, setTabImage] = React.useState("3");

  const modules = {
    toolbar: [
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
      [{ color: [] }, { background: [] }],
      [{ font: [] }],
      [{ align: [] }],
    ],
  };

  const baseAttribute = {
    type: "size",
    name: "",
    nameSelect: {},
    stock_balance: 0
    // regular_price: "",
    // sale_price: "",
  };

  const basecategory = {
    category: "",
    subcategory: "",
    optionSubCate: [],
  };

  const [id, setId] = useState(0);
  const [ddlTag, setDdlTag] = useState([]);
  const [ddlCategory, setDdlCategory] = useState([]);
  const [ddlSubCategory, setDdlSubCategory] = useState([]);
  const [ddlsize, setDdlsize] = useState([]);
  const [ddlfontcolour, setDdlfontcolour] = useState([]);

  const [tmpImage, setTmpImage] = useState([]);
  const [sizeImage, setSizeImage] = useState([]);
  const [delfile, setDelfile] = useState([]);
  const [delFileSize, setDelFileSize] = useState([]);

  const [attribute, setAttribute] = useState([]);

  const [modal, setModal] = useState(false);

  const [rerateid, setRerateid] = useState([]);
  const [dataProduct, setDataProduct] = useState([]);

  const [sku, setSku] = useState("");
  const [productName, setProductName] = useState("");
  const [productNameEn, setProductNameEn] = useState("");
  const [desc, setDesc] = useState("");
  const [descEn, setDescEn] = useState("");
  const [price, setPrice] = useState("");
  const [fontcolour, setFontColour] = useState(null);
  const [saleprice, setSaleprice] = useState("");
  const [expireDate, setExpireDate] = useState(new Date());
  // const [expireDate, setExpireDate] = useState("");
  const [category, setCategory] = useState([basecategory]);
  const [is_optional_name, set_is_optional_name] = useState(false);
  const [is_optional_number, set_is_optional_number] = useState(false);

  const [iscostomname, setIscostomname] = useState(false);
  const [isplayername, setIsplayername] = useState(false);
  const [isgu12, setIsgu12] = useState(false);
  const [safety_balance, set_safety_balance] = useState(0);
  const [isarm, setIsArm] = useState(false);

  const [tag, setTag] = useState([]);
  const [addorremove, setAddorremove] = useState("");
  useEffect(() => {
    if (new URLSearchParams(props.location.search).get("pid") != null) {
      setId(new URLSearchParams(props.location.search).get("pid"));
    }

    func_loadcateDDL();
    func_loadtagDDL();
    func_loadSizeDDL();
    func_loadFontColourDDL();
    func_loadSubcateDDL();
  }, []);

  useEffect(() => {
    if (id != 0) {
      func_loadEdit(id);
    }
  }, [id]);

  useEffect(() => {
    return () => {};
  }, [ddlfontcolour]);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  const handleTabAddress = (idx) => {
    setTabAddress(idx);
  };

  const handleTabImage = (idx) => {
    setTabImage(idx);
  };

  const func_loadcateDDL = async () => {
    try {
      var res = await ApiMasterCategory.getddl();
      if (res.status == 200) {
        var data = res.data.data;
        var setdata = [];
        for (let i = 0; i < data.length; i++) {
          var subdata = { value: data[i].value, label: data[i].text };
          setdata.push(subdata);
        }
        setDdlCategory(setdata);
      }
    } catch (err) {}
  };

  const fields = [
    {
      key: "option",
      label: "",
      _style: { width: "1%" },
      filter: false,
    },
    { key: "sku", label: "รหัสสินค้า", _style: { minWidth: "150px" } },
    { key: "name", label: "ชื่อสินค้า", _style: { minWidth: "150px" } },
    { key: "images", label: "รูปสินค้า", _style: { minWidth: "150px" } },
  ];

  const func_loadSizeDDL = async () => {
    try {
      var res = await ApiMasterValue.getddlsize();
      if (res.status == 200) {
        var data = res.data.data;
        var setdata = [];
        for (let i = 0; i < data.length; i++) {
          var subdata = { value: data[i].value, label: data[i].text };
          setdata.push(subdata);
        }
        setDdlsize(setdata);
      }
    } catch (err) {}
  };

  const func_loadFontColourDDL = async () => {
    try {
      var res = await ApiMasterFontColour.getActive();
      if (res.status == 200) {
        const {data} = res.data;
        // var setdata = [];
        // for (let i = 0; i < data.length; i++) {
        //   var subdata = { value: data[i].id, label: data[i].colourname };
        //   setdata.push(subdata);
        // }
        console.log("data::", data);
        setDdlfontcolour(data);
      }
    } catch (err) {}
  };

  const func_loadtagDDL = async () => {
    try {
      var res = await ApiMasterTag.getddl();
      if (res.status == 200) {
        var data = res.data.data;
        var setdata = [];
        for (let i = 0; i < data.length; i++) {
          var subdata = { value: data[i].value, label: data[i].text };
          setdata.push(subdata);
        }
        setDdlTag(setdata);
      }
    } catch (err) {}
  };

  const func_loadSubcateDDL = async () => {
    try {
      var res = await ApiMasterCategory.getsubddl();
      if (res.status == 200) {
        var data = res.data.data;

        setDdlSubCategory(data);
      }
    } catch (err) {}
  };

  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ];

  const func_pickfile = (e) => {
    if (e.files.length > 0) {
      var file = e.files[0];

      var type = ["image/png", "image/jpg", "image/jpeg"];
      var message = "";
      var check = CheckFile({
        file,
        size: 10,
        type: type,
        message,
      });
      if (check) {
        var files = [...tmpImage];

        var modelfile = {
          type: "I",
          name: file.name,
          file: file,
          link: URL.createObjectURL(file),
          is_back: false,
        };
        files.push(modelfile);
        setTmpImage(files);
      }
      e.value = "";
    }
  };

  const func_pickFileImageSize = (e) => {
    if (e.files.length > 0) {
      var file = e.files[0];

      var type = ["image/png", "image/jpg", "image/jpeg"];
      var message = "";
      var check = CheckFile({
        file,
        size: 10,
        type: type,
        message,
      });
      if (check) {
        var files = [...sizeImage];

        var modelfile = {
          type: "I",
          name: file.name,
          file: file,
          link: URL.createObjectURL(file),
        };
        files.push(modelfile);
        setSizeImage(files);
      }
      e.value = "";
    }
  };

  const func_removeImage = (index) => {
    var datafile = [...tmpImage];

    if (datafile[index].type != "I") {
      var delmodel = [...delfile];
      delmodel.push({ name: datafile[index].name, link: datafile[index].link });
      setDelfile(delmodel);
    }

    datafile.splice(index, 1);
    setTmpImage(datafile);
  };

  const func_setIsBack = (check, index) => {
    var datafile = [...tmpImage];
    datafile[index].is_back = check;
    console.log("datafile::", datafile);
    setTmpImage(datafile);
  };

  const func_removeImageSize = (index) => {
    var datafile = [...sizeImage];

    if (datafile[index].type != "I") {
      var delmodel = [...delFileSize];
      delmodel.push({ name: datafile[index].name, link: datafile[index].link });
      setDelFileSize(delmodel);
    }

    datafile.splice(index, 1);
    setSizeImage(datafile);
  };

  const func_pushdata = () => {
    var data = [...attribute];
    data.push(baseAttribute);
    setAttribute(data);
  };

  const func_setAttribute = (key, index, value) => {
    var data = [...attribute];
    data[index][key] = value;
    setAttribute(data);
  };

  const func_check_dup = (option, value) => {
    // console.log("option::", option);
    // console.log("attribute::", attribute);
    // console.log("value::", value);
    // console.log("value[0].value::", value[0].value);
    var dup = attribute.find((x) => x.nameSelect.value == option.value);
    // console.log("dup::", dup);
    return dup;
  }

  const func_add_all_attr = () => {
    var data = [...attribute];
    ddlsize.forEach((x) => {
      var dup = attribute.find((a) => a.nameSelect.value == x.value);
      console.log("x::", x);
      console.log("dup::", dup);
      if (!dup) {
        data.push({
          type: "size",
          name: x.label,
          nameSelect: x,
          stock_balance: 0
        });
      }
    })
    setAttribute(data);
  }

  const func_setAttributevalidate = (key, index, value) => {
    console.log(value);
    var data = [...attribute];
    if (isNaN(value)) {
      data[index][key] = "";
      setAttribute(data);
    }
  };

  const func_removeAttribute = (index) => {
    var data = [...attribute];
    data.splice(index, 1);
    setAttribute(data);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    // console.log("category::", category);
    // console.log("category.length::", category.length);
    // console.log("category[0].category.value::", category[0].category);

    if (category.length == 1 && category[0].category == "") {
      Swal.fire({
        icon: "error",
        title: "กรุณาเลือกหมวดหมู่สินค้า",
      });
      return false;
    }

    var formData = new FormData();
    var model = {};

    var setAttribute = [];

    attribute.forEach((e, index) => {
      console.log(e);
      var setA = {
        type: e.type,
        name: e.nameSelect.label == undefined ? "" : e.nameSelect.label,
        nameSelect: e.nameSelect,
        sku: sku + "-00" + (index+1),
        stock_balance: parseInt(e.stock_balance),
        // regular_price: isNaN(e.regular_price) ? 0 : Number(e.regular_price),
        // sale_price: isNaN(e.sale_price) ? 0 : Number(e.sale_price),
      };

      setAttribute.push(setA);
    });

    var setmodel = {
      id: id,
      productName: productName,
      productNameEn: productNameEn,
      sku: sku,
      price: isNaN(price) ? 0 : Number(price),
      saleprice: isNaN(saleprice) ? 0 : Number(saleprice),
      attribute: setAttribute,
      desc: desc,
      descEn: descEn,
      delfile: delfile,
      delFileSize: delFileSize,
      images: tmpImage.filter((x) => x.type == "A"),
      tag: tag,
      is_optional_name: is_optional_name,
      is_optional_number: is_optional_number,
      iscostomname: iscostomname,
      isplayername: isplayername,
      isgu12: isgu12,
      safety_balance: safety_balance,
      category: category,
      expire_date: expireDate,
      rerateid: await helper.arraytostringcomma(rerateid),
      fontcolour_id: fontcolour == "" ? null : fontcolour,
      isarm: isarm,
    };

    formData.append("parm", JSON.stringify(setmodel));
    model.parm = setmodel
    var file= [];

    tmpImage.forEach((e) => {
      if (e.type == "I") {
        formData.append("file", e.file);
        file.push(e.file)
      }
    });
    model.file = file

    var imagesize= [];
    sizeImage.forEach((e) => {
      if (e.type == "I") {
        formData.append("imagesize", e.file);
        imagesize.push(e.file)
      }
    });
    model.imagesize = imagesize
    console.log("setmodel::", setmodel);
    func_save(formData, model);
  };

  const func_save = async (formdata, model) => {
    try {
      console.log("Json formdata::", JSON.stringify(model));
      func_add_log("Save Product", JSON.stringify(model));
      const result = await ApiMasterProduct.save(formdata, id);
      if (result.status === 200) {
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "บันทึกสำเร็จ",
          timer: 2000,
        }).then((success) => {
          history.push(`/Mt_productEdit?pid=${data}`);
          window.location.reload();
          //setId(data);
          //func_loadEdit(data);
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const func_loadEdit = async (pid) => {
    try {
      setAttribute([]);
      setTmpImage([]);
      setSizeImage([]);
      const result = await ApiMasterProduct.getedit(pid);
      if (result.status === 200) {
        const { data } = result.data;
        if (data.product.length > 0) {
          var resdata = data.product[0];
          setProductName(resdata.name);
          setProductNameEn(resdata.name_en);
          setSku(resdata.sku);
          setPrice(resdata.regular_price);
          setSaleprice(resdata.sale_price);
          setDesc(resdata.description);
          setDescEn(resdata.description_en);
          setAttribute(resdata.attributes);
          setTmpImage(resdata.images);
          set_is_optional_name(resdata.is_optional_name ?? false);
          set_is_optional_number(resdata.is_optional_number ?? false);
          setIscostomname(resdata.iscostomname ?? false);
          setIsplayername(resdata.isplayername ?? false);
          setIsgu12(resdata.isgu12 ?? false);
          set_safety_balance(resdata.safety_balance);
          setFontColour(resdata.fontcolour_id)
          setIsArm(resdata.isarm ?? false);

          setExpireDate(
            resdata.expire_date ? new Date(resdata.expire_date) : ""
          );
          // setExpireDate(resdata.expire_date);
          // setExpireDate(resdata.expire_date ? moment(resdata.expire_date).format("YYYY-MM-DD") : "");
          resdata.images_sizechart === null
            ? setSizeImage([])
            : setSizeImage(resdata.images_sizechart);

          if (
            resdata.relate_product_id != null &&
            resdata.relate_product_id != undefined &&
            resdata.relate_product_id != ""
          ) {
            setRerateid(
              await helper.stringcommatoarray(resdata.relate_product_id)
            );
          }

          var setdata = [];
          for (let i = 0; i < data.tag.length; i++) {
            var subdata = { value: data.tag[i].value, label: data.tag[i].text };
            setdata.push(subdata);
          }
          setTag(setdata);

          if (data.category.length > 0) {
            let datacate = data.category;

            let dcate = datacate.find((x) => !x.parent);
            let dsubcate = datacate.find((x) => x.parent);

            dcate =
              dcate != null && dcate != undefined
                ? { value: dcate.categoryid, label: dcate.categoryname }
                : {};
            dsubcate =
              dsubcate != null && dcate != undefined
                ? { value: dsubcate.categoryid, label: dsubcate.categoryname }
                : {};

            var setCate = {
              category: dcate,
              subcategory: dsubcate,
              optionSubCate: [],
            };

            setCategory([setCate]);
          }
        }
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  const fucn_setDataCate = (e, index) => {
    var data = [...category];
    var val = e.value;

    var setsubddl = ddlSubCategory.filter((x) => x.value2 == val);

    var setdata = [];
    for (let i = 0; i < setsubddl.length; i++) {
      var subdata = { value: setsubddl[i].value, label: setsubddl[i].text };
      setdata.push(subdata);
    }

    data[index].category = e;
    data[index].subcategory = {};
    data[index].optionSubCate = setdata;

    setCategory(data);
  };

  const func_setSubcategory = (e, index) => {
    var data = [...category];

    data[index].subcategory = e;

    setCategory(data);
  };

  const func_loadproduct = async (module) => {
    try {
      setModal(true);
      setAddorremove(module);
      var res;
      if (module == "A") {
        var idlist = [...rerateid];
        if (id != 0) {
          idlist.push(id);
        }

        var idcomma = await helper.arraytostringcomma(idlist);
        console.log(idcomma);
        res = await ApiMasterProduct.getlistidcommanotequeal({ pid: idcomma });
      } else {
        var idlist = [...rerateid];

        var idcomma = await helper.arraytostringcomma(idlist);
        res = await ApiMasterProduct.getlistbyidcomma({ pid: idcomma });
      }

      if (res.status == 200) {
        var data = res.data.data;
        console.log(data);
        setDataProduct(data);
      }
    } catch (err) {}
  };

  const func_addrerateid = (id, index) => {
    var data = [...rerateid];
    data.push(id);
    setRerateid(data);

    var datalist = [...dataProduct];
    datalist.splice(index, 1);
    setDataProduct(datalist);
  };

  const func_removererateid = (id, index) => {
    var data = [...rerateid];

    for (let i = 0; i < data.length; i++) {
      if (data[i] == id) {
        data.splice(i, 1);
      }
    }
    setRerateid(data);

    var datalist = [...dataProduct];
    datalist.splice(index, 1);
    setDataProduct(datalist);
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CRow className="mb-2">
            <CCol sm="2">
              <CButton
                block
                variant="outline"
                color="primary"
                onClick={() => {
                  func_loadproduct("A");
                }}
              >
                เพิ่มสินค้าที่เกี่ยวข้อง
              </CButton>
            </CCol>
            <CCol sm="2">
              <CButton
                block
                variant="outline"
                color="success-custom"
                onClick={() => {
                  func_loadproduct("D");
                }}
              >
                แสดงสินค้าที่เกี่ยวข้อง
              </CButton>
            </CCol>
          </CRow>

          <CForm onSubmit={onSubmit} action="">
            <CTabs
              activeTab={tabAddress}
              onActiveTabChange={(idx) => handleTabAddress(idx)}
            >
              <CNav variant="tabs" className="mb-3">
                <CNavItem>
                  <CNavLink data-tab="1">รายละเอียดสินค้า</CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="2">Attribute</CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="3">Flex</CNavLink>
                </CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane data-tab="1">
                  <CCard>
                    <CCardHeader>
                      <b>สินค้า</b>
                    </CCardHeader>
                    <CCardBody>
                      <CRow className="mb-3">
                        <CCol xs="1">
                          รหัสสินค้า <span style={{ color: "red" }}>*</span>
                        </CCol>
                        <CCol xs="3">
                          {id == 0 ? (
                            <CInput
                              value={sku}
                              onChange={(e) => {
                                setSku(e.target.value);
                              }}
                              required
                            />
                          ) : (
                            <CInput
                              disabled
                              value={sku}
                              onChange={(e) => {
                                setSku(e.target.value);
                              }}
                              required
                            />
                          )}
                        </CCol>
                        <CCol xs="1">Flash Sale</CCol>
                        <CCol xs="3">
                          {/* <CInput
                            type="date"
                            value={expireDate}
                            onChange={(e) => {
                              setExpireDate(e.target.value);
                            }}
                            required
                          /> */}
                          <DatePicker
                            selected={expireDate}
                            onChange={(date) => setExpireDate(date)}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            timeCaption="time"
                            dateFormat="MMMM d, yyyy h:mm aa"
                            className="w-100 border rounded pl-2"
                          />
                        </CCol>
                      </CRow>
                      <CRow className="mb-3">
                        <CCol xs="1">
                          ชื่อสินค้า <span style={{ color: "red" }}>*</span>
                        </CCol>
                        <CCol xs="3">
                          <CInput
                            value={productName}
                            onChange={(e) => {
                              setProductName(e.target.value);
                            }}
                            required
                          />
                        </CCol>
                        <CCol xs="1">
                          ชื่อสินค้า EN <span style={{ color: "red" }}>*</span>
                        </CCol>
                        <CCol xs="3">
                          <CInput
                            value={productNameEn}
                            onChange={(e) => {
                              setProductNameEn(e.target.value);
                            }}
                            required
                          />
                        </CCol>
                      </CRow>

                      <CRow className="mb-3">
                        <CCol xs="1">
                          ราคา <span style={{ color: "red" }}>*</span>
                        </CCol>
                        <CCol xs="3">
                          <CInput
                            value={price}
                            onChange={(e) => {
                              setPrice(e.target.value);
                            }}
                            onBlur={(e) => {
                              if (isNaN(e.target.value)) {
                                setPrice("");
                              }
                            }}
                            required
                          />
                        </CCol>
                        <CCol xs="1">ราคาที่ลด</CCol>
                        <CCol xs="3">
                          <CInput
                            value={saleprice}
                            onChange={(e) => {
                              setSaleprice(e.target.value);
                            }}
                            onBlur={(e) => {
                              if (isNaN(e.target.value)) {
                                setSaleprice("");
                              }
                            }}
                          />
                        </CCol>
                      </CRow>

                      <CRow className="mb-3">
                        <CCol xs="1">
                          Safety Stock<span style={{ color: "red" }}>*</span>
                        </CCol>
                        <CCol xs="3">
                          <CInput
                            value={safety_balance}
                            onChange={(e) => {
                              set_safety_balance(e.target.value);
                            }}
                            type="number"
                            required
                          />
                        </CCol>
                      </CRow>

                      <CRow className="mb-3">
                        <CCol xs="1">รายละเอียด</CCol>
                        <CCol xs="12">
                          <ReactQuill
                            onChange={setDesc}
                            value={desc}
                            modules={modules}
                            id="desc"
                          />
                        </CCol>
                      </CRow>

                      <CRow className="mb-3">
                        <CCol xs="1">รายละเอียด EN</CCol>
                        <CCol xs="12">
                          <ReactQuill
                            onChange={setDescEn}
                            value={descEn}
                            modules={modules}
                            id="descEn"
                          />
                        </CCol>
                      </CRow>
                    </CCardBody>
                  </CCard>
                  <CCard>
                    <CCardHeader>
                      <b>หมวดหมู่สินค้า</b>
                    </CCardHeader>
                    <CCardBody>
                      {category.map((item, index) => {
                        return (
                          <div key={index}>
                            <CRow className="mb-3">
                              <CCol xs="1">
                                หมวดหมู่ <span style={{ color: "red" }}>*</span>
                              </CCol>
                              <CCol xs="3">
                                <Select
                                  placeholder="เลือก 1 รายการ"
                                  options={ddlCategory}
                                  value={item.category}
                                  onChange={(e) => {
                                    fucn_setDataCate(e, index);
                                  }}
                                  required
                                />
                              </CCol>
                              <CCol xs="1">หมวดหมู่ย่อย</CCol>
                              <CCol xs="3">
                                <Select
                                  placeholder="เลือก 1 รายการ"
                                  options={item.optionSubCate}
                                  value={item.subcategory}
                                  onChange={(e) => {
                                    func_setSubcategory(e, index);
                                  }}
                                />
                              </CCol>
                            </CRow>
                            <CRow className="mb-2">
                              <CCol xs="1">แท็ก</CCol>
                              <CCol xs="3">
                                <Select
                                  placeholder="เลือก 1 รายการ"
                                  options={ddlTag}
                                  isMulti
                                  value={tag}
                                  onChange={(e) => {
                                    setTag(e);
                                  }}
                                />
                              </CCol>
                            </CRow>
                          </div>
                        );
                      })}
                    </CCardBody>
                  </CCard>
                  <CTabs
                    activeTab={tabImage}
                    onActiveTabChange={(idx) => handleTabImage(idx)}
                  >
                    <CNav variant="tabs" className="mb-3">
                      <CNavItem>
                        <CNavLink data-tab="3">รูปภาพสินค้า</CNavLink>
                      </CNavItem>
                      <CNavItem>
                        <CNavLink data-tab="4">รูปขนาดสินค้า</CNavLink>
                      </CNavItem>
                    </CNav>
                    <CTabContent>
                      <CTabPane data-tab="3">
                        <CCard>
                          <CCardHeader>
                            <b>รูปภาพสินค้า</b>
                          </CCardHeader>
                          <CCardBody>
                            <CRow className="mb-3">
                              <CCol xs="4">
                                <SelectFile
                                  onChange={(e) => {
                                    func_pickfile(e);
                                  }}
                                />
                              </CCol>
                            </CRow>

                            {tmpImage.map((item, index) => {
                              return (
                                <CRow key={index} className="mb-2">
                                  <CCol xs="12">
                                    <div className="d-flex">
                                      <div className="pr-3">
                                        <CButton
                                          onClick={() => {
                                            func_removeImage(index);
                                          }}
                                          block
                                          variant="outline"
                                          style={{ width: "40px" }}
                                          color="danger-custom"
                                        >
                                          <CIcon name="cil-trash" />
                                        </CButton>
                                      </div>
                                      {item.type == "A" && (
                                        <div className="pr-3">
                                          <CFormGroup>
                                            <CLabel>รูปหลังเสื้อ</CLabel>
                                            <CSwitch
                                              key={index}
                                              className={"mx-1"}
                                              shape={"pill"}
                                              color={"primary"}
                                              checked={item.is_back ?? false}
                                              onChange={(e) =>
                                                func_setIsBack(
                                                  e.target.checked,
                                                  index
                                                )
                                              }
                                            />
                                          </CFormGroup>
                                        </div>
                                      )}
                                      <div className="pr-3">
                                        {item.name}
                                        {item.type == "I" ? (
                                          <div className="imgPreview">
                                            <img
                                              src={item.link}
                                              className="imageTag"
                                            ></img>
                                          </div>
                                        ) : (
                                          <div className="imgPreview">
                                            <img
                                              src={WEB_API + item.link}
                                              className="imageTag"
                                            ></img>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </CCol>
                                </CRow>
                              );
                            })}
                          </CCardBody>
                        </CCard>
                      </CTabPane>
                      <CTabPane data-tab="4">
                        <CCard>
                          <CCardHeader>
                            <b>รูปขนาดสินค้า</b>
                          </CCardHeader>
                          <CCardBody>
                            <CRow className="mb-3">
                              <CCol xs="4">
                                <SelectFile
                                  onChange={(e) => {
                                    func_pickFileImageSize(e);
                                  }}
                                />
                              </CCol>
                            </CRow>

                            {sizeImage.map((item, index) => {
                              return (
                                <CRow key={index} className="mb-2">
                                  <CCol xs="4">
                                    <div className="d-flex">
                                      <div className="pr-3">
                                        <CButton
                                          onClick={() => {
                                            func_removeImageSize(index);
                                          }}
                                          block
                                          variant="outline"
                                          style={{ width: "40px" }}
                                          color="danger-custom"
                                        >
                                          <CIcon name="cil-trash" />
                                        </CButton>
                                      </div>
                                      <div className="pr-3">
                                        {item.name}
                                        {item.type == "I" ? (
                                          <div className="imgPreview">
                                            <img
                                              src={item.link}
                                              className="imageTag"
                                            ></img>
                                          </div>
                                        ) : (
                                          <div className="imgPreview">
                                            <img
                                              src={WEB_API + item.link}
                                              className="imageTag"
                                            ></img>
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                  </CCol>
                                </CRow>
                              );
                            })}
                          </CCardBody>
                        </CCard>
                      </CTabPane>
                    </CTabContent>
                  </CTabs>
                  {/* <CCard>
                    <CCardHeader>
                      <b>รูปภาพ</b>
                    </CCardHeader>
                    <CCardBody>
                      <CRow className="mb-3">
                        <CCol xs="2">
                          <SelectFile
                            onChange={(e) => {
                              func_pickfile(e);
                            }}
                          />
                        </CCol>
                      </CRow>

                      {tmpImage.map((item, index) => {
                        return (
                          <CRow key={index} className="mb-2">
                            <CCol xs="4">
                              <div className="d-flex">
                                <div className="pr-3">
                                  <CButton
                                    onClick={() => {
                                      func_removeImage(index);
                                    }}
                                    block
                                    variant="outline"
                                    style={{ width: "40px" }}
                                    color="danger-custom"
                                  >
                                    <CIcon name="cil-trash" />
                                  </CButton>
                                </div>
                                <div className="pr-3">
                                  {item.name}
                                  {item.type == "I" ? (
                                    <div className="imgPreview">
                                      <img src={item.link} className="imageTag"></img>
                                    </div>
                                  ) : (
                                    <div className="imgPreview">
                                      <img
                                        src={WEB_API + item.link}
                                        className="imageTag"
                                      ></img>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </CCol>
                          </CRow>
                        );
                      })}
                    </CCardBody>
                  </CCard> */}
                </CTabPane>
                <CTabPane data-tab="2">
                  <CCard>
                    <CCardHeader>
                      <b>Attribute</b>
                    </CCardHeader>
                    <CCardBody>
                      {attribute.length < ddlsize.length && (
                        <CRow className="mb-3">
                          <CCol sm="11">
                            <CButton
                              variant="outline"
                              // style={{ width: "40px" }}
                              onClick={() => {
                                func_add_all_attr();
                              }}
                              color="primary"
                            >
                              <CIcon name="cil-plus" /> Add All Attribute
                            </CButton>
                          </CCol>
                          <CCol sm="1">
                            <CButton
                              variant="outline"
                              style={{ width: "40px" }}
                              onClick={() => {
                                func_pushdata();
                              }}
                              color="primary"
                            >
                              <CIcon name="cil-plus" />
                            </CButton>
                          </CCol>
                        </CRow>
                      )}
                      {attribute.map((item, index) => {
                        return (
                          <div key={index}>
                            <CRow className="mb-3">
                              {/* <CCol xs="1">ราคา</CCol>
                              <CCol xs="3">
                                <CInput
                                  value={item.regular_price}
                                  onChange={(e) => {
                                    func_setAttribute(
                                      "regular_price",
                                      index,
                                      e.target.value
                                    );
                                  }}
                                  onBlur={(e) => {
                                    func_setAttributevalidate(
                                      "regular_price",
                                      index,
                                      e.target.value
                                    );
                                  }}
                                />
                              </CCol> */}
                              {/* <CCol xs="2">sale price</CCol>
                              <CCol xs="3">
                                <CInput
                                  value={item.sale_price}
                                  onChange={(e) => {
                                    func_setAttribute(
                                      "sale_price",
                                      index,
                                      e.target.value
                                    );
                                  }}
                                  onBlur={(e) => {
                                    func_setAttributevalidate(
                                      "sale_price",
                                      index,
                                      e.target.value
                                    );
                                  }}
                                />
                              </CCol> */}

                              <CCol xs="1">ขนาด</CCol>
                              <CCol xs="3">
                                <Select
                                  options={ddlsize}
                                  value={item.nameSelect}
                                  isOptionDisabled={(x, value) => func_check_dup(x, value)}
                                  onChange={(e) => {
                                    func_setAttribute("nameSelect", index, e);
                                  }}
                                ></Select>
                              </CCol>
                              <CCol xs="1">สต็อก</CCol>
                              <CCol xs="3">
                                <CInput
                                  type="number"
                                  value={item.stock_balance}
                                  readOnly={true}
                                  // onChange={(e) => {
                                  //   func_setAttribute(
                                  //     "stock_balance",
                                  //     index,
                                  //     e.target.value
                                  //   );
                                  // }}
                                  // onBlur={(e) => {
                                  //   func_setAttributevalidate(
                                  //     "stock_balance",
                                  //     index,
                                  //     e.target.value
                                  //   );
                                  // }}
                                />
                              </CCol>
                              <CCol xs="2">
                                <CButton
                                  onClick={() => {
                                    func_removeAttribute(index);
                                  }}
                                  block
                                  variant="outline"
                                  style={{ width: "40px" }}
                                  color="danger-custom"
                                >
                                  <CIcon name="cil-trash" />
                                </CButton>
                              </CCol>
                            </CRow>
                            {/* <CRow key={index} className="mb-3">
                            </CRow>
                            <hr /> */}
                          </div>
                        );
                      })}
                    </CCardBody>
                  </CCard>
                </CTabPane>
                <CTabPane data-tab="3">
                  <CCard>
                    <CCardHeader>
                      <b>Flex</b>
                    </CCardHeader>
                    <CCardBody>
                      <CRow className="mb-3">
                        <CCol xs="1">
                          <CLabel for="is_optional_name">
                            Arm
                          </CLabel>
                        </CCol>
                        <CCol xs="1">
                          <CSwitch
                            id="is_optional_name"
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={isarm}
                            onChange={(e) => {
                              setIsArm(e.target.checked);
                            }}
                          />
                        </CCol>
                      </CRow>
                      <CRow className="mb-3">
                        <CCol xs="1">
                          <CLabel for="is_optional_name">
                            Custom Name
                          </CLabel>
                        </CCol>
                        <CCol xs="1">
                          <CSwitch
                            id="is_optional_name"
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={iscostomname}
                            onChange={(e) => {
                              setIscostomname(e.target.checked);
                              set_is_optional_name(false);
                              set_is_optional_number(false);
                            }}
                          />
                        </CCol>
                      </CRow>

                      {iscostomname && (
                        <>
                          <CRow className="mb-3">
                            <CCol xs="1"></CCol>
                            <CCol xs="1">
                              <CLabel for="is_optional_name">มีชื่อ</CLabel>
                            </CCol>
                            <CCol xs="1">
                              <CSwitch
                                id="is_optional_name"
                                className={"mx-1"}
                                shape={"pill"}
                                color={"primary"}
                                checked={is_optional_name}
                                onChange={(e) =>
                                  set_is_optional_name(e.target.checked)
                                }
                              />
                            </CCol>
                          </CRow>
                          <CRow className="mb-3">
                            <CCol xs="1"></CCol>
                            <CCol xs="1">
                              <CLabel for="is_optional_number">มีเบอร์เสื้อ</CLabel>
                            </CCol>
                            <CCol xs="1">
                              <CSwitch
                                id="is_optional_number"
                                className={"mx-1"}
                                shape={"pill"}
                                color={"primary"}
                                checked={is_optional_number}
                                onChange={(e) =>
                                  set_is_optional_number(e.target.checked)
                                }
                              />
                            </CCol>
                          </CRow>
                        </>
                      )}
                      <CRow className="mb-3">
                        <CCol xs="1">
                          <CLabel for="is_optional_number">Player</CLabel>
                        </CCol>
                        <CCol xs="1">
                          <CSwitch
                            id="is_optional_number"
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={isplayername}
                            onChange={(e) => setIsplayername(e.target.checked)}
                          />
                        </CCol>
                      </CRow>
                      <CRow className="mb-3">
                        <CCol xs="1">
                          <CLabel for="is_optional_number">GU12</CLabel>
                        </CCol>
                        <CCol xs="1">
                          <CSwitch
                            id="is_optional_number"
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={isgu12}
                            onChange={(e) => setIsgu12(e.target.checked)}
                          />
                        </CCol>
                      </CRow>
                      <CRow>
                        <CCol xs="12">
                            <CRow className="mb-3">
                              <CCol xs="1">
                                <CLabel>สีตัวอักษร</CLabel>
                              </CCol>
                              {ddlfontcolour.map((c) => {
                                return (<>
                                  <CCol xs="1">
                                    <CLabel for={`is_font_colour_${c.id}`}>{c.colourname}</CLabel>
                                  </CCol>
                                  <CCol xs="1">
                                    <CSwitch
                                      type="radio"
                                      name="chk_fontcolour"
                                      id={`is_font_colour_${c.id}`}
                                      className={"mx-1"}
                                      shape={"pill"}
                                      color={"primary"}
                                      checked={fontcolour == c.id}
                                      onChange={(e) => setFontColour(c.id)}
                                    />
                                  </CCol>
                                </>)
                              })}
                            </CRow>
                        </CCol>
                      </CRow>
                    </CCardBody>
                  </CCard>
                </CTabPane>
              </CTabContent>
            </CTabs>
            <CRow className="mb-3">
              <CCol sm="10"></CCol>
              <CCol sm="1">
                <CButton
                  type="submit"
                  color="primary-custom"
                  variant="reverse"
                  block
                >
                  บันทึก
                </CButton>{" "}
              </CCol>
              <CCol sm="1">
                <CButton
                  color="secondary"
                  block
                  onClick={() => {
                    history.push("/Mt_product");
                  }}
                >
                  ยกเลิก
                </CButton>
              </CCol>
            </CRow>
          </CForm>
        </CCol>
      </CRow>

      <CModal size="xl" show={modal} onClose={setModal}>
        <CModalHeader closeButton>
          <CModalTitle>
            {addorremove == "A"
              ? "เพิ่มสินค้าที่เกี่ยวข้อง"
              : "แสดงสินค้าที่เกี่ยวข้อง"}
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs="12" sm="12">
              <CDataTable
                items={dataProduct}
                fields={fields}
                tableFilter={{
                  label: "ค้นหา",
                  placeholder: "พิมพ์คำที่ต้องการค้นหา",
                }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                  order: (item) => (
                    <td style={{ textAlign: "center" }}>{item.order}</td>
                  ),
                  option: (item, index) => (
                    <td className="center">
                      <CButtonGroup>
                        {addorremove == "A" ? (
                          <CButton
                            onClick={() => {
                              func_addrerateid(item.id, index);
                            }}
                            block
                            variant="outline"
                            style={{ width: "40px" }}
                            color="success-custom"
                          >
                            <CIcon name="cil-check" />
                          </CButton>
                        ) : (
                          <CButton
                            onClick={() => {
                              func_removererateid(item.id, index);
                            }}
                            block
                            variant="outline"
                            style={{ width: "40px" }}
                            color="danger-custom"
                          >
                            <CIcon name="cil-trash" />
                          </CButton>
                        )}
                      </CButtonGroup>
                    </td>
                  ),
                  images: (item) => (
                    <td className="center">
                      {item.images.length > 0 && (
                        <div className="imgPreview" style={{ width: "150px" }}>
                          <img
                            src={WEB_API + item.images[0].link}
                            className="imageTag"
                          ></img>
                        </div>
                      )}
                    </td>
                  ),
                }}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="secondary"
            onClick={() => {
              setModal(false);
            }}
          >
            ยกเลิก
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default ProductEdit;
