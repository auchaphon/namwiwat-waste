import React, { useState, useEffect } from "react";
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CRow,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CTextarea,
    CInputCheckbox,
    CPopover,
    CLink,
    CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Swal from "sweetalert2";
import moment from 'moment';
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");

const get_order_status = (status) => {
    switch (status) {
        case "PP": return "ยังไม่ได้ชำระ";
        case "P": return "กำลังเตรียมพัสดุ";
        case "DL": return "อยู่ระหว่างจัดส่ง";
        case "FF": return "จัดส่งแล้ว";
        case "C": return "ยกเลิกคำสั่งซื้อ";
        default: return "";
    }
}

const get_payment_method = (payment) => {
    switch (payment) {
        case "CRE": return "Credit Card";
        default: return "";
    }
}

const fields = [
    {
        key: 'option',
        label: '',
        _style: { width: '1%' },
        filter: false,
    },
    { key: "order_ref", label: "รหัสการสั่งซื้อ", _style: { minWidth: "200px", textAlign: "center" } },
    { key: 'total_amount', label: "ยอดรวม", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'total_item', label: "ยอดสินค้า", _style: { minWidth: "100px" , textAlign: "center"} },
    { key: 'order_status', label: "สถานะ", _style: { minWidth: "150px", textAlign: "center" } },
    // { key: 'order_status', label: "การขนส่ง", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'payment_method', label: "วิธีการชำระเงิน", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'payment_date', label: "วันที่ชำระเงิน", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'discount_code', label: "โปรโมชัน", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'campaign', label: "แคมเปญ", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createdate', label: "วันที่สั่งซื้อล่าสุด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createby', label: "ผู้สั่งซื้อ", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'updatedate', label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: "200px", textAlign: "center" } },
    { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },

];
const EditDetails = (record) => {
    // toggleConfirmEdit();
    // setDataSelected(record);
    // setUsername(record.username);
    // setCategoryId(record.id);
    // setName(record.name);
    // setDescription(record.description);
    // setParent(record.parent)
}

const Tables = ({ data = [], refreshData = () => { } }) => {

    const history = useHistory();
    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => {
                                            history.push(`/invoicedetail?pid=${item.order_ref}`);
                                        }}
                                    >
                                        <img id={`edit-${item.id}`} src={edit_icon} />
                                    </CButton>
                                    {/* <CButton variant="ghost" size="sm" /> */}
                                    {item.order_status == "P" && (
                                        <CButton
                                            className="ml-3"
                                            color="warning-custom"
                                            variant="reverse"
                                            shape="square"
                                            size="sm"
                                            title="พิมพ์ใบปะหน้า"
                                            // onClick={() => {
                                            //     history.push(`/invoicedetail?pid=${item.order_ref}`);
                                            // }}
                                        >
                                            <CIcon name="cilPrint"></CIcon>
                                        </CButton>
                                    )}
                                </CButtonGroup>
                            </td>
                        ),
                    'order_status':
                        (item) => (
                            <td>
                                {get_order_status(item.order_status)}
                            </td>
                        ),
                    // 'total_amount':
                    //     (item) => (
                    //         <td className="text-center">
                    //             {item.total_amount ?? ""}
                    //         </td>
                    //     ),
                    'total_item':
                        (item) => (
                            <td className="text-center">
                                {item.total_item ?? ""}
                            </td>
                        ),
                    'campaign':
                        (item) => (
                            <td>
                                {item.campaign ?? ""}
                            </td>
                        ),
                    'payment_method':
                        (item) => (
                            <td>
                                {get_payment_method(item.payment_method)}
                            </td>
                        ),
                    "payment_date": (item) => (
                        <td className="text-center">
                            {item.payment_date ? moment(item.payment_date)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td className="text-center">
                            {item.updatedate ? moment(item.updatedate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createby": (item) => (
                        <td>
                            {item.createby ? item.createby : ""}
                        </td>
                    ),
                    "updateby": (item) => (
                        <td>
                            {item.updateby ? item.updateby : ""}
                        </td>
                    ),
                    // 'option':
                    //     (item) => (
                    //         <td className="center">
                    //             <CButtonGroup>
                    //                 <CButton
                    //                     color="primary-custom"
                    //                     variant="reverse"
                    //                     shape="square"
                    //                     size="sm"
                    //                     onClick={() => { EditDetails(item) }}
                    //                     onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                    //                     onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                    //                 >
                    //                     <img id={`edit-${item.id}`} src={edit_icon} /> 
                    //                 </CButton>
                    //                 {/* <CButton variant="ghost" size="sm" /> */}
                    //                 <CButton
                    //                     className="ml-3"
                    //                     color="danger-custom"
                    //                     variant="reverse"
                    //                     shape="square"
                    //                     size="sm"
                    //                     onClick={() => { deleteDetails(item) }}
                    //                     onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                    //                     onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                    //                 >
                    //                     <img id={`delete-${item.id}`} src={trash_icon} />
                    //                     {/* <CIcon name="cil-trash" /> */}
                    //                 </CButton>
                    //             </CButtonGroup>
                    //         </td>
                    //     ),

                }}
            />
        </>
    );
}
export default Tables;
