import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CForm,
  CFormGroup,
  CSelect,
  CInputCheckbox,
  CDataTable,
  CTextarea,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,
  CButtonGroup,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiTranOrder from '../../api/ApiTranOrder';
import InputAddress from 'react-thailand-address-autocomplete'
import { useHistory } from 'react-router'
import moment from 'moment'
import Swal from "sweetalert2";
import axios from 'axios';

const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");



const detailFields = [
    { key: "sku", label: "หมายเลขสินค้า" , _style: { minWidth: "180px" }},
    { key: "product_name", label: "ชื่อสินค้า" , _style: { minWidth: "190px" }},
    { key: "option", label: "รายละเอียด" , _style: { minWidth: "250px" }},
    // { key: "optionNumber", label: "หมายเลข" , _style: { minWidth: "200px" }},
    { key: "price", label: "ราคาสินค้าต่อชิ้น 	", _style: { minWidth: "140px" } },
    { key: "quanlity", label: "จำนวนสินค้า", _style: { minWidth: "100px" } },
    { key: "total_amount", label: "ยอดรวม", _style: { minWidth: "140px" } },
];
const commentFields = [
    {
        key: 'option',
        label: '',
        _style: { width: '1%' },
        filter: false,
    },
    { key: "comment", label: "ความคิดเห็น" , _style: { minWidth: "150px" }},
    { key: 'createdate', label: "วันที่แสดงความคิดเห็น", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createby', label: "ผู้แสดงความคิดเห็น", _style: { width: "150px", textAlign: "center" } },
];
const logFields = [
    { key: "order_ref", label: "หมายเลขคำสั่งซื้อ" , _style: { minWidth: "150px" }},
    { key: "json_data", label: "ข้อมูล" , _style: { minWidth: "150px" }},
];

const get_order_class = (status) => {
    switch (status) {
        case "PP": return "text-warning";
        case "P": return "text-success";
        case "DL": return "text-success";
        case "FF": return "text-success";
        case "C": return "text-danger";
        default: return "";
    }
}

const get_order_status = (status) => {
    switch (status) {
        case "PP": return "ยังไม่ได้ชำระ";
        case "P": return "กำลังเตรียมพัสดุ";
        case "DL": return "อยู่ระหว่างจัดส่ง";
        case "FF": return "จัดส่งแล้ว";
        case "C": return "ยกเลิกคำสั่งซื้อ";
        default: return "";
    }
}

const Detail = (props) => {
    const [status, setStatus] = useState('');
    const [statusNote, setStatusNote] = useState('');
    const [payment, setPayment] = useState('');
    const [isEditAddress, setIsEditAddress] = useState(false);
    const [isEditReceipt, setIsEditReceipt] = useState(false);
    const [tabAddress, setTabAddress] = React.useState("1");
    const [orderRef,setOrderRef] = useState(new URLSearchParams(props.location.search).get("pid"));
    const [dataDetail, setDataDetail] = useState();
    const [dataComment, setDataComment] = useState();
    const [logPayment,setLogPayment] = useState(); 
    const history = useHistory()
    const [note,setNote] = useState();
    const [trackingNo,setTrackingNo] = useState("");
    const [comment,setComment] = useState("");
    const [modalConfirm, setModalConfirm] = useState(false);
    const [dataSelected, setDataSelected] = useState('');
    const [promotion,setPromotion] = useState("");
    const [campaign,setCampaign] = useState("");
    const [address_tax_id,set_address_tax_id] = useState(0);

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }
    
    const [address,setAddress] = useState({
        firstname:"",
        lastname:"",
        phonenumber:"",
        address:"",
        province:"",
        district:"",
        subdistrict:"",
        zipcode:"",
    });
    
    const [address_Tax,setAddress_Tax] = useState({
        firstname:"",
        lastname:"",
        phonenumber:"",
        address:"",
        province:"",
        district:"",
        subdistrict:"",
        zipcode:"",
    });

    const handleTabAddress = (idx) => {
        setTabAddress(idx);
    };

    const getData = async () => {
        try {
            const order_ref = {order_ref:orderRef}
            var result = await ApiTranOrder.getOrderDetail(order_ref);

            const { data } = result.data;
            // for (let i = 0; i < data.length; i++) {
            //     var option = data[i].option
            //     console.log("option::", option);
            //     data[i].optionName = `${option?.name ?? ""} ${option?.name_price ? `(${(option?.name_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} บาท)` : ""}`;
            //     data[i].optionNumber = `${option?.number ?? ""} ${option?.number_price ? `(${(option?.number_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} บาท)` : ""}`;
            // }
            // console.log("data::", data);
            setDataDetail(data)
        } catch (error) {
  
        }
    }
    const getDataAd = async () => {
        try {
            const order_ref = {order_ref:orderRef}
            var resultAd = await ApiTranOrder.getOrderRef(order_ref);
            set_address_tax_id(resultAd.data.data.address_tax)
            setAddress(resultAd.data.data.address_user_json)
            setAddress_Tax(resultAd.data.data.address_tax_json)
            setStatus(resultAd.data.data.order_status)
            setNote(resultAd.data.data.note)
            setTrackingNo(resultAd.data.data.tracking_number)
            setPromotion(resultAd.data.data.discount_code)
            setCampaign(resultAd.data.data.campaign)
            
        } catch (error) {
  
        }
    }
    const getDataLogPayment = async () => {
        try {
            const order_ref = {order_ref:orderRef}
            var resultlog = await ApiTranOrder.getLogPayment(order_ref);
            setLogPayment(resultlog.data.data)
            
            
        } catch (error) {
  
        }
    }

    const saveNote = async () => {
        try {
            const data = {
                note:note,
                order_ref:orderRef
            }
            var result = await ApiTranOrder.UpdateNote(data);
        } catch (error) {
  
        }
    }

    const saveTracking = async () => {
        try {
            const data = {
                tracking_number:trackingNo,
                order_status:status,
                order_ref:orderRef
            }
            var result = await ApiTranOrder.UpdateTracking(data);
        } catch (error) {
  
        }
    }
    const updateAd = async () => {
        try {
            const data = {
                address_user_json:address,
                order_ref:orderRef
            }
            var result = await ApiTranOrder.UpdateAd(data);
            history.go(0)
        } catch (error) {
  
        }
    }
    
    const onChange = (e) => {
        setAddress({...address,
            [e.target.name]: e.target.value
          })
        console.log(address)
      }
    const onSelect = (addresses) => {
    const { subdistrict, district, province, zipcode } = addresses
    setAddress({
        ...address,
        province:province,
        district:district,
        subdistrict:subdistrict,
        zipcode:zipcode
    })
    console.log(address)
    }

    useEffect(() => {
    }, [address]);
    useEffect(() => {
        
        getData()
        getDataAd()
        getDataLogPayment()
        getComment()
      }, []);

    const getComment = async () => {
        try {
            var result = await ApiTranOrder.getComment(orderRef);
            const {data} = result.data;
            setDataComment(data)
        } catch (error) {
  
        }
    }

    const submitComment = async (e) => {
        e.preventDefault();
        try {
            const data = {
                order_ref:orderRef,
                comment:comment
            }
            var result = await ApiTranOrder.insertComment(data);
            setComment("");
            if (result.status === 200) {
                getComment();
                // history.go(0);
            }
        } catch (error) {
  
        }
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        console.log("record::", record);
        setDataSelected(record);
    }


    const onDeleteComment = async () => {
        try {
            console.log("dataSelected::", dataSelected);
            const id = dataSelected.id
            const result = await ApiTranOrder.deleteComment(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    getComment();
                    // history.go(0)
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const func_print_invoice = async (e)=> {
        e.preventDefault();
        var items = [];
        for (let i = 0; i < dataDetail.length; i++) {
            items.push({
                NO: `${i}`,
                Sku: dataDetail[i].sku,
                Description: dataDetail[i].product_name,
                Quantity: dataDetail[i].quanlity,
                Unit: dataDetail[i].price,
                Amount: dataDetail[i].total_amount
            })
            
        }
        var data = {
            OrderNo: orderRef,
            Discount: 0.00,
            Items: items
        }

        axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
        axios.defaults.headers.common["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,PATCH,OPTIONS";
    }

    return (
        <>
            <CRow>
                <CCol xs="12">
                    <CButton color="info-custom" variant="reverse" onClick={()=>history.go(-1)}>กลับ</CButton>
                </CCol>
            </CRow>
            <CRow className="mt-3">
                <CCol xs="8">
                    <CCard>
                        <CCardHeader>
                            {'รายการสั่งซื้อหมายเลข ' + orderRef}
                            <div className="card-header-actions">
                                {moment().format("DD/MM/YYYY HH:mm")}
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={dataDetail}
                                fields={detailFields}
                                // bordered
                                scopedSlots={{
                                    "option": (item) => (
                                        <td>
                                            {item.size && item.size.length > 0 && (
                                                <>
                                                    ขนาด: {item.size}
                                                </>
                                            )}
                                            {item.option.type == "custom" && (
                                                <>
                                                    <br></br> ชื่อ: {item.option.name} {`(${(item.option?.name_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                    <br></br> ตราสัญลักษณ์: {item.option.arm_name} {`(${(item.option?.arm_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                </>
                                            )}
                                            {item.option.type == "player" && (
                                                <>
                                                    <br></br> ชื่อนักเตะ: {item.option.player_name} {`(${(item.option?.player_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                    <br></br> ตราสัญลักษณ์: {item.option.arm_name} {`(${(item.option?.arm_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                </>
                                            )}
                                            {item.option.type == "gu12" && (
                                                <>
                                                    <br></br> GU12: {`(${(item.option?.gu12_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                    <br></br> ตราสัญลักษณ์: {item.option.arm_name} {`(${(item.option?.arm_price*1.00).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} ฿)`}
                                                </>
                                            )}
                                        </td>
                                    ),
                                }}
                        />
                        </CCardBody>
                    </CCard>
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol xs="6">รหัสโปรโมชัน: {promotion}</CCol>
                                <CCol xs="6">แคมเปญ: {campaign}</CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                    <CCard>
                        <CCardHeader>
                            ข้อมูลสินค้าที่มีในคลัง
                        </CCardHeader>
                        <CCardBody>
                            {'หมายเลขอ้างอิง: ' + orderRef}
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol xs="4">
                    <CCard>
                        <CCardHeader className={"text-center"}>
                            สถานะ
                        </CCardHeader>
                        <CCardBody>
                            <CRow>
                                <CCol xs="12" className={"text-center"}>
                                    <CLabel className={get_order_class(status)} >{get_order_status(status)}</CLabel>
                                </CCol>
                            </CRow>
                            <CRow>
                                <CCol xs="12" className={"text-center"}>
                                    <CButton color="secondary" onClick={(e) => func_print_invoice(e)}>พิมพ์ใบกำกับภาษี</CButton>
                                </CCol>
                            </CRow>
                            <CRow>
                                <CCol xs="12">
                                    <CFormGroup>
                                        <CLabel htmlFor="status">สถานะ</CLabel>
                                        <CSelect onChange={e => setStatus(e.target.value)} value={status} id="status" disabled>
                                            <option value={"PP"}>รอชำระเงิน</option>
                                            <option value={"P"}>ชำระแล้ว</option>
                                            <option value={"DL"}>อยู่ระหว่างจัดส่ง</option>
                                            <option value={"R"}>ยกเลิก</option>
                                        </CSelect>
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow>
                                <CCol xs="12">
                                    <CFormGroup>
                                        <CInput rows={3} onChange={e => setNote(e.target.value)} value={note}></CInput>
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow>
                                <CCol xs="12" className={"text-center"}>
                                    <CButton color="primary-custom" variant="reverse" onClick={()=>{saveNote();}}>บันทึก</CButton>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                    
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="4">
                    <CCard>
                        <CCardHeader>
                            ชื่อและที่อยู่สำหรับจัดส่งสินค้า
                            <div className="card-header-actions">
                                {(!isEditAddress) &&
                                    <CButton color="secondary" variant="outline" onClick={(e) => setIsEditAddress(true)}>แก้ไข</CButton>
                                }
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            {isEditAddress ?
                                <CForm>
                                    <CRow>
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel>ชื่อ</CLabel>
                                                <CInput onChange={e => setAddress({...address,firstname:e.target.value})} value={address.firstname}/>
                                            </CFormGroup>
                                        </CCol>
                                        <CCol xs="6">
                                            <CFormGroup>
                                                <CLabel>นามสกุล</CLabel>
                                                <CInput onChange={e => setAddress({...address,lastname:e.target.value})} value={address.lastname}/>
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel>หมายเลขโทรศัพท์</CLabel>
                                                <CInput onChange={e => setAddress({...address,phonenumber:e.target.value})} value={address.phonenumber}/>
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel>ที่อยู่</CLabel>
                                                <CInput onChange={e => setAddress({...address,address:e.target.value})} value={address.address}/>
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                            <span className="col-start-1 col-end-2">จังหวัด :</span>
                                                <InputAddress
                                                    address="province"
                                                    value={address.province}
                                                    className="border-2 border-gray-300"
                                                    onChange={onChange}
                                                    onSelect={onSelect}
                                                />
                                            
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                            <span className="col-start-1 col-end-2">เขต / อำเภอ :</span>
                                            <InputAddress
                                                address="district"
                                                value={address.district}
                                                className="border-2 border-gray-300"
                                                onChange={onChange}
                                                onSelect={onSelect}
                                            />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                            <span className="col-start-1 col-end-2">แขวง / ตำบล :</span>
                                            <InputAddress
                                                address="subdistrict"
                                                value={address.subdistrict}
                                                className="border-2 border-gray-300"
                                                onChange={onChange}
                                                onSelect={onSelect}
                                            />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                            <span className="col-start-1 col-end-2">รหัสไปรษณีย์ :</span>
                                            <InputAddress
                                                address="zipcode"
                                                value={address.zipcode}
                                                className="border-2 border-gray-300"
                                                onChange={onChange}
                                                onSelect={onSelect}
                                            />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    
                                    <CRow>
                                        <CCol xs="12">
                                            <CButton type="button" color="primary" onClick={(e) => {setIsEditAddress(false);updateAd();}}>บันทึก</CButton>{' '}
                                            <CButton type="button" color="secondary"  onClick={(e) => setIsEditAddress(false)}>ยกเลิก</CButton>
                                        </CCol>
                                    </CRow>
                                </CForm>
                            :
                            <>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address.firstname + " " + address.lastname}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address.address + " แขวง" + address.subdistrict + " เขต" + address.district}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address.province + " " + address.zipcode}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{"หมายเลขโทรศัพท์: " + address.phonenumber}</CLabel>
                                    </CCol>
                                </CRow>
                            </>
                            }
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol xs="8">
                    <CCard>
                        <CCardHeader>
                            ชื่อและที่อยู่สำหรับใบกำกับภาษี
                            {/* <div className="card-header-actions">
                                {(!isEditReceipt) &&
                                    <CButton color="secondary" variant="outline" onClick={(e) => setIsEditReceipt(true)}>แก้ไข</CButton>
                                }
                            </div> */}
                        </CCardHeader>
                        <CCardBody>
                            {isEditReceipt ?
                                <CForm>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup variant="checkbox">
                                                <CInputCheckbox
                                                    // id="isOrderByPaidTime"
                                                    // name="isOrderByPaidTime"
                                                    // onChange={(e) => {setIsOrderByPaidTime(!isOrderByPaidTime)}}
                                                    // checked={isOrderByPaidTime}
                                                />
                                                <CLabel >ต้องการใบกำกับภาษี </CLabel>
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="6">
                                            <div className="fa-border">
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>ชื่อ</CLabel>
                                                        <CInput/>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>หมายเลขใบกำกับภาษี</CLabel>
                                                        <CInput/>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>ที่อยู่</CLabel>
                                                        <CTextarea rows={3}/>
                                                    </CCol>
                                                </CRow>
                                            </div>
                                        </CCol>
                                        <CCol xs="6">
                                            <div className="fa-border">
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CFormGroup variant="checkbox">
                                                            <CInputCheckbox
                                                                // id="isOrderByPaidTime"
                                                                // name="isOrderByPaidTime"
                                                                // onChange={(e) => {setIsOrderByPaidTime(!isOrderByPaidTime)}}
                                                                checked={address_tax_id > 0}
                                                            />
                                                            <CLabel >ต้องการการจัดส่ง </CLabel>
                                                        </CFormGroup>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>ชื่อ</CLabel>
                                                        <CInput onChange={e => setAddress({...address_Tax,firstname:e.target.value})} value={address_Tax.firstname}/>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>ที่อยู่</CLabel>
                                                        <CTextarea rows={3}/>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="6">
                                                        <CLabel>จังหวัด</CLabel>
                                                        <CSelect></CSelect>
                                                    </CCol>
                                                    <CCol xs="6">
                                                        <CLabel>รหัสไปรษณีย์</CLabel>
                                                        <CInput/>
                                                    </CCol>
                                                </CRow>
                                            </div>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CLabel>หมายเหตุ</CLabel>
                                            <CTextarea rows={3}/>
                                        </CCol>
                                    </CRow>
                                    <CRow className={"mt-3"}>
                                        <CCol xs="12">
                                            <CButton type="button" color="primary" onClick={(e) => setIsEditReceipt(false)}>บันทึก</CButton>{' '}
                                            <CButton type="button" color="secondary"  onClick={(e) => setIsEditReceipt(false)}>ยกเลิก</CButton>
                                        </CCol>
                                    </CRow>
                                </CForm>
                            : 
                            address_Tax.firstname ? 
                            <>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address_Tax.firstname + " " + address_Tax.lastname}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address_Tax.address + " แขวง" + address_Tax.subdistrict + " เขต" + address_Tax.district}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{address_Tax.province + " " + address_Tax.zipcode}</CLabel>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CLabel>{"หมายเลขโทรศัพท์: " + address_Tax.phonenumber}</CLabel>
                                    </CCol>
                                </CRow>
                            </>
                                : "ไม่ต้องการ"
                            }
                            
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12">
                    <CCard>
                        <CCardHeader>
                            หมายเลขพัสดุ
                            {/* <div className="card-header-actions">
                                <CButton color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div> */}
                        </CCardHeader>
                        <CCardBody>
                            <CRow>
                                <CCol xs="6">
                                    <CFormGroup>
                                        <CLabel htmlFor="trackingNo">หมายเลขพัสดุ</CLabel>
                                        <CInput onChange={e => setTrackingNo(e.target.value)} value={trackingNo}></CInput>
                                    </CFormGroup>
                                </CCol>
                                <CCol xs="4">
                                    <CFormGroup>
                                        <CLabel htmlFor="status-edit">สถานะ</CLabel>
                                        <CSelect onChange={e => setStatus(e.target.value)} value={status} id="status-edit">
                                            <option value={"PP"}>รอชำระเงิน</option>
                                            <option value={"P"}>กำลังเตรียมพัสดุ</option>
                                            <option value={"DL"}>อยู่ระหว่างจัดส่ง</option>
                                            <option value={"FF"}>จัดส่งแล้ว</option>
                                            <option value={"R"}>ยกเลิก</option>
                                        </CSelect>
                                    </CFormGroup>
                                </CCol>
                                <CCol xs="2" className="mt-4">
                                    <CRow className="mt-1">
                                        <CButton color="primary-custom" variant="reverse" onClick={()=>{saveTracking();}}>บันทึก</CButton>
                                        <CButton className="ml-3" color="primary-custom" variant="reverse" onClick={()=>{}}>พิมพ์ใบปะหน้า</CButton>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            {/* <CRow>
                <CCol xs="4">
                    <CCard>
                        <CCardHeader>
                            ประวัติการสั่งซื้อ
                            <div className="card-header-actions">
                                <CButton color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol xs="4">
                    <CCard>
                        <CCardHeader>
                            ประวัติอีเมล
                            <div className="card-header-actions">
                                <CButton color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow> */}
            <CRow>
                <CCol xs="12">
                    <CCard>
                        <CCardHeader>
                            ประวัติความคิดเห็น
                            <div className="card-header-actions">
                                <CButton onClick={() => getComment()} color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <CRow>
                                <CCol xs="12">
                                    <CDataTable
                                        items={dataComment}
                                        fields={commentFields}
                                        // bordered
                                        scopedSlots={{
                                            "createby": (item) => (
                                                <td className="text-center">
                                                    {item.createby ?? ""}
                                                </td>
                                            ),
                                            "createdate": (item) => (
                                                <td className="text-center">
                                                    {item.createdate ? moment(item.createdate)
                                                        // .add(543, "years")
                                                        .format("DD-MM-YYYY HH:mm") : ""}
                                                </td>
                                            ),
                                            'option': (item) => (
                                                <td className="center">
                                                    <CButton
                                                        className="ml-3"
                                                        color="danger-custom"
                                                        variant="reverse"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { deleteDetails(item) }}
                                                        onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                                        onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                                    >
                                                        <img id={`delete-${item.id}`} src={trash_icon} />
                                                    </CButton>
                                                </td>
                                            ),
                        
                                        }}
                                    />
                                </CCol>
                            </CRow>
                            <CForm onSubmit={(e) => submitComment(e)} action="">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CTextarea value={comment} onChange={(e) => setComment(e.target.value)} rows={3} required></CTextarea>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CButton type="submit" color="primary">เพิ่ม ความคิดเห็น</CButton>
                                    </CCol>
                                </CRow>
                            </CForm>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12">
                    <CCard>
                        <CCardHeader>
                            ประวัติการชำระเงิน 
                            <div className="card-header-actions">
                                <CButton onClick={() => getDataLogPayment()} color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            {/* {logPayment.map((d,index)=>
                                <div>
                                    
                                </div>
                            )} */}
                            <CDataTable
                                items={logPayment}
                                fields={logFields}
                                // bordered
                                scopedSlots={{
                                    "json_data": (item) => (
                                            <td>
                                                <pre>
                                                    {JSON.stringify(item.json_data)}
                                                </pre>
                                            </td>
                                        ),
                                }}
                        />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            {/* <CRow>
                <CCol xs="12">
                    <CCard>
                        <CCardHeader>
                            รายการหลักฐานโอนเงิน
                            <div className="card-header-actions">
                                <CButton color="secondary" variant="outline"><CIcon name="cil-sync"></CIcon></CButton>
                            </div>
                        </CCardHeader>
                        <CCardBody>
                            <CTabs
                                activeTab={tabAddress}
                                onActiveTabChange={(idx) => handleTabAddress(idx)}
                            >
                                <CNav variant="tabs" className="mb-3">
                                    <CNavItem>
                                        <CNavLink data-tab="1">List</CNavLink>
                                    </CNavItem>
                                    <CNavItem>
                                        <CNavLink data-tab="2">Upload</CNavLink>
                                    </CNavItem>
                                </CNav>
                                <CTabContent>
                                    <CTabPane data-tab="1">
                                        ยังไม่มีหลักฐานการโอนเงิน
                                    </CTabPane>
                                    <CTabPane data-tab="2">
                                        <CContainer>
                                            <CForm>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CFormGroup>
                                                            <CLabel>เลขที่คำสั่งซื้อ</CLabel>
                                                            <CInput readOnly/>
                                                        </CFormGroup>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CFormGroup>
                                                            <CLabel>โอนจากธนาคาร</CLabel>
                                                            <CInput />
                                                        </CFormGroup>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CFormGroup>
                                                            <CLabel>จำนวนเงินที่โอน (บาท)</CLabel>
                                                            <CInput />
                                                        </CFormGroup>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="2">
                                                        <CFormGroup>
                                                            <CLabel>วันที่โอน</CLabel>
                                                            <CInput />
                                                        </CFormGroup>
                                                    </CCol>
                                                    <CCol xs="1">
                                                        <CFormGroup>
                                                            <CLabel>เวลาที่โอน</CLabel>
                                                            <CSelect>
                                                                <option value="00">00</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                            </CSelect>
                                                        </CFormGroup>
                                                    </CCol>
                                                    <CCol xs="1">
                                                        <CFormGroup>
                                                            <CSelect>
                                                                <option value="00">00</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                                <option value="24">24</option>
                                                                <option value="25">25</option>
                                                                <option value="26">26</option>
                                                                <option value="27">27</option>
                                                                <option value="28">28</option>
                                                                <option value="29">29</option>
                                                                <option value="30">30</option>
                                                                <option value="31">31</option>
                                                                <option value="32">32</option>
                                                                <option value="33">33</option>
                                                                <option value="34">34</option>
                                                                <option value="35">35</option>
                                                                <option value="36">36</option>
                                                                <option value="37">37</option>
                                                                <option value="38">38</option>
                                                                <option value="39">39</option>
                                                                <option value="40">40</option>
                                                                <option value="41">41</option>
                                                                <option value="42">42</option>
                                                                <option value="43">43</option>
                                                                <option value="44">44</option>
                                                                <option value="45">45</option>
                                                                <option value="46">46</option>
                                                                <option value="47">47</option>
                                                                <option value="48">48</option>
                                                                <option value="49">49</option>
                                                                <option value="50">50</option>
                                                                <option value="51">51</option>
                                                                <option value="52">52</option>
                                                                <option value="53">53</option>
                                                                <option value="54">54</option>
                                                                <option value="55">55</option>
                                                                <option value="56">56</option>
                                                                <option value="57">57</option>
                                                                <option value="58">58</option>
                                                                <option value="59">59</option>
                                                            </CSelect>
                                                        </CFormGroup>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">

                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12">
                                                        <CLabel>ข้อมูลเพิ่มเติม</CLabel>
                                                        <CTextarea rows={3}></CTextarea>
                                                    </CCol>
                                                </CRow>
                                                <CRow className={"mt-3"}>
                                                    <CCol xs="12">
                                                        <CButton color="secondary">ส่งหลักฐานการโอนเงิน</CButton>
                                                    </CCol>
                                                </CRow>
                                            </CForm>
                                        </CContainer>
                                    </CTabPane>
                                </CTabContent>
                            </CTabs>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow> */}

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDeleteComment} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
        </>
    );
}
export default Detail;
