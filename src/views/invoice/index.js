import React, { useEffect, useState } from "react";
import {
  CCard,
  CFade,
  CLink,
  CCollapse,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
  CForm,
  CFormGroup,
  CSelect,
  CInputCheckbox,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useDispatch, useSelector } from "react-redux";
import Tables from "./Tables";
import ApiTranOrder from "../../api/ApiTranOrder";
import ApiUser from "../../api/ApiUser";
import moment from "moment-timezone";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// moment().tz("Thailand/Bangkok").format();
const InvoiceList = () => {
  const [collapsed, setCollapsed] = React.useState(false);
  const [showCard, setShowCard] = React.useState(true);

  const [orderId, setOrderId] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [payment, setPayment] = useState("");
  const [status, setStatus] = useState("");
  const [shipping, setShipping] = useState("");
  const [transactionFromDate, setTransactionFromDate] = useState("");
  const [transactionFromHr, setTransactionFromHr] = useState("");
  const [transactionFromMn, setTransactionFromMn] = useState("");
  const [transactionToDate, setTransactionToDate] = useState("");
  const [transactionToHr, setTransactionToHr] = useState("");
  const [transactionToMn, setTransactionToMn] = useState("");
  const [paidFromDate, setPaidFromDate] = useState("");
  const [paidFromHr, setPaidFromHr] = useState("");
  const [paidFromMn, setPaidFromMn] = useState("");
  const [paidToDate, setPaidToDate] = useState("");
  const [paidToHr, setPaidToHr] = useState("");
  const [paidToMn, setPaidToMn] = useState("");
  const [promotion, setPromotion] = useState("");
  const [campaign, setCampaign] = useState("");
  const [dataTable, setDataTable] = useState([]);
  const userState = useSelector((state) => state.user);

  const getData = async () => {
    try {
      // console.log(moment(`${moment(transactionFromDate).format("YYYY-MM-DD")} ${transactionFromHr} : ${transactionFromMn}`, 'YYYY-MM-DD HH:mm'));
      var model = {
        id: 0,
        order_ref: orderId,
        email: email,
        name: name,
        payment: payment,
        status: status,
        promotion: promotion,
        campaign: campaign,
        transactionFromDate: transactionFromDate
          ? moment.tz(
              `${moment(transactionFromDate).format(
                "YYYY-MM-DD"
              )} ${transactionFromHr} : ${transactionFromMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        transactiontoDate: transactionToDate
          ? moment.tz(
              `${moment(transactionToDate).format(
                "YYYY-MM-DD"
              )} ${transactionToHr} : ${transactionToMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        payFromDate: paidFromDate
          ? moment.tz(
              `${moment(paidFromDate).format(
                "YYYY-MM-DD"
              )} ${paidFromHr} : ${paidFromMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        payToDate: paidToDate
          ? moment.tz(
              `${moment(paidToDate).format(
                "YYYY-MM-DD"
              )} ${paidToHr} : ${paidToMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
      };
      var result = await ApiTranOrder.getlistOrder(model);
      func_add_log("Search Menu Invoice", JSON.stringify(model));
      if (result.status === 200) {
        const { data } = result.data;
        setDataTable(
          data.sort((a, b) => (a.updatedate > b.updatedate ? -1 : 1))
        );
      }
    } catch (error) {}
  };

  useEffect(() => {
    getData();
    func_add_log("Click Menu Invoice", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  const func_search_submit = (e) => {
    e.preventDefault();
    getData();
  };

  const onExport = async () => {
    try {
      var model = {
        id: 0,
        order_ref: orderId,
        email: email,
        name: name,
        payment: payment,
        status: status,
        promotion: promotion,
        campaign: campaign,
        transactionFromDate: transactionFromDate
          ? moment.tz(
              `${moment(transactionFromDate).format(
                "YYYY-MM-DD"
              )} ${transactionFromHr} : ${transactionFromMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        transactiontoDate: transactionToDate
          ? moment.tz(
              `${moment(transactionToDate).format(
                "YYYY-MM-DD"
              )} ${transactionToHr} : ${transactionToMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        payFromDate: paidFromDate
          ? moment.tz(
              `${moment(paidFromDate).format(
                "YYYY-MM-DD"
              )} ${paidFromHr} : ${paidFromMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
        payToDate: paidToDate
          ? moment.tz(
              `${moment(paidToDate).format(
                "YYYY-MM-DD"
              )} ${paidToHr} : ${paidToMn}`,
              "YYYY-MM-DD HH:mm",
              "Thailand/Bangkok"
            )
          : "",
      };
      func_add_log("Export Menu Invoice", JSON.stringify(model));
      var result = await ApiTranOrder.onExport(model);
      if (result.status === 200) {
        window.open(result.data);
      }
    } catch (error) {}
  };

  const func_clear_search = () => {
    setOrderId("");
    setEmail("");
    setName("");
    setPayment("");
    setStatus("");
    setTransactionFromDate("");
    setTransactionFromHr("00");
    setTransactionFromMn("00");
    setTransactionToDate("");
    setTransactionToHr("00");
    setTransactionToMn("00");
    setPaidFromDate("");
    setPaidFromHr("00");
    setPaidFromMn("00");
    setPaidToDate("");
    setPaidToHr("00");
    setPaidToMn("00");
    setPromotion("");
    setCampaign("");
  };

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CFade in={showCard}>
            <CCard>
              <CCardHeader
                style={{ cursor: "pointer" }}
                onClick={() => setCollapsed(!collapsed)}
              >
                ค้นหารายการสั่งซื้อ
                <div className="card-header-actions">
                  <CLink className="card-header-action">
                    {/* <CIcon name={collapsed ? 'cil-chevron-bottom' : 'cil-chevron-top'} /> */}
                    <CIcon
                      name={
                        collapsed ? "cil-chevron-top" : "cil-chevron-bottom"
                      }
                    />
                  </CLink>
                </div>
              </CCardHeader>
              <CCollapse show={collapsed}>
                <CCardBody>
                  <CForm onSubmit={(e) => func_search_submit(e)} action="">
                    <CRow>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="orderId">รหัสการสั่งซื้อ</CLabel>
                          <CInput
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            id="orderId"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="email">อีเมลของผู้สั่งซื้อ</CLabel>
                          <CInput
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            id="email"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="4">
                        <CFormGroup>
                          <CLabel htmlFor="name">
                            ชื่อ-นามสกุลของผู้สั่งซื้อ
                          </CLabel>
                          <CInput
                            onChange={(e) => setName(e.target.value)}
                            value={name}
                            id="name"
                          />
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol xs="3">
                        <CFormGroup>
                          <CLabel htmlFor="payment">วิธีการชำระเงิน</CLabel>
                          <CSelect
                            onChange={(e) => setPayment(e.target.value)}
                            value={payment}
                            id="payment"
                          >
                            <option value={""}>-</option>
                            <option value={"CRE"}>Credit Card</option>
                            {/* <option value={"2"}>Paypal</option>
                                                        <option value={"3"}>Bank Transfer</option>
                                                        <option value={"4"}>Cheque</option>
                                                        <option value={"5"}>Internet Banking</option>
                                                        <option value={"6"}>2C2P(123)</option>
                                                        <option value={"7"}>Over The Counter</option>
                                                        <option value={"8"}>Line Pay</option> */}
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="3">
                        <CFormGroup>
                          <CLabel htmlFor="status">สถานะ</CLabel>
                          <CSelect
                            onChange={(e) => setStatus(e.target.value)}
                            value={status}
                            id="status"
                          >
                            <option value={""}>-</option>
                            <option value={"PP"}>ยังไม่ได้ชำระ</option>
                            <option value={"P"}>กำลังเตรียมพัสดุ</option>
                            <option value={"DL"}>อยู่ระหว่างจัดส่ง</option>
                            <option value={"FF"}>จัดส่งแล้ว</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="3">
                        <CFormGroup>
                          <CLabel htmlFor="promotion">โปรโมชัน</CLabel>
                          <CInput
                            onChange={(e) => setPromotion(e.target.value)}
                            value={promotion}
                            id="promotion"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="3">
                        <CFormGroup>
                          <CLabel htmlFor="campaign">แคมเปญ</CLabel>
                          <CInput
                            onChange={(e) => setCampaign(e.target.value)}
                            value={campaign}
                            id="campaign"
                          />
                        </CFormGroup>
                      </CCol>
                      {/* <CCol xs="3">
                                                <CFormGroup>
                                                    <CLabel htmlFor="shipping">การขนส่ง</CLabel>
                                                    <CSelect onChange={e => setShipping(e.target.value)} value={shipping} id="shipping">
                                                        <option value={""}>-</option>
                                                        <option value={"1"}>จัดส่ง</option>
                                                        <option value={"2"}>ไม่จัดส่ง</option>
                                                    </CSelect>
                                                </CFormGroup>
                                            </CCol> */}
                    </CRow>
                    <CRow>
                      <CCol xs="12">
                        <CLabel style={{ fontWeight: "bold" }}>
                          วันที่สั่งซื้อ
                        </CLabel>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol xs="2">
                        <CFormGroup>
                          <CLabel htmlFor="transactionFromDate">ตั้งแต่</CLabel>

                          <DatePicker
                            selected={transactionFromDate}
                            onChange={(date) => setTransactionFromDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1">
                        <CFormGroup>
                          <CLabel htmlFor="transactionFromHr">เวลา</CLabel>
                          <CSelect
                            onChange={(e) =>
                              setTransactionFromHr(e.target.value)
                            }
                            value={transactionFromHr}
                            id="transactionFromHr"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1" className={"mt-1"}>
                        <CFormGroup>
                          <CSelect
                            className={"mt-4"}
                            onChange={(e) =>
                              setTransactionFromMn(e.target.value)
                            }
                            value={transactionFromMn}
                            id="transactionFromMn"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1"></CCol>
                      <CCol xs="2">
                        <CFormGroup>
                          <CLabel htmlFor="transactionToDate">จนถึง</CLabel>
                          <DatePicker
                            selected={transactionToDate}
                            onChange={(date) => setTransactionToDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1">
                        <CFormGroup>
                          <CLabel htmlFor="transactionToHr">เวลา</CLabel>
                          <CSelect
                            onChange={(e) => setTransactionToHr(e.target.value)}
                            value={transactionToHr}
                            id="transactionToHr"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1" className={"mt-1"}>
                        <CFormGroup>
                          <CSelect
                            className={"mt-4"}
                            onChange={(e) => setTransactionToMn(e.target.value)}
                            value={transactionToMn}
                            id="transactionToMn"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol xs="12">
                        <CLabel style={{ fontWeight: "bold" }}>
                          วันที่ชำระเงิน
                        </CLabel>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol xs="2">
                        <CFormGroup>
                          <CLabel htmlFor="paidFromDate">ตั้งแต่</CLabel>

                          <DatePicker
                            selected={paidFromDate}
                            onChange={(date) => setPaidFromDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1">
                        <CFormGroup>
                          <CLabel htmlFor="paidFromHr">เวลา</CLabel>
                          <CSelect
                            onChange={(e) => setPaidFromHr(e.target.value)}
                            value={paidFromHr}
                            id="paidFromHr"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1" className={"mt-1"}>
                        <CFormGroup>
                          <CSelect
                            className={"mt-4"}
                            onChange={(e) => setPaidFromMn(e.target.value)}
                            value={paidFromMn}
                            id="paidFromMn"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1"></CCol>
                      <CCol xs="2">
                        <CFormGroup>
                          <CLabel htmlFor="paidToDate">จนถึง</CLabel>

                          <DatePicker
                            selected={paidToDate}
                            onChange={(date) => setPaidToDate(date)}
                            dateFormat="dd/MM/yyyy"
                            className="form-control"
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1">
                        <CFormGroup>
                          <CLabel htmlFor="paidToHr">เวลา</CLabel>
                          <CSelect
                            onChange={(e) => setPaidToHr(e.target.value)}
                            value={paidToHr}
                            id="paidToHr"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol xs="1" className={"mt-1"}>
                        <CFormGroup>
                          <CSelect
                            className={"mt-4"}
                            onChange={(e) => setPaidToMn(e.target.value)}
                            value={paidToMn}
                            id="paidToMn"
                          >
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    {/* <CRow>
                                            <CCol xs="12">
                                                <CFormGroup variant="checkbox">
                                                    <CInputCheckbox
                                                        id="isOrderByPaidTime"
                                                        name="isOrderByPaidTime"
                                                        onChange={(e) => { setIsOrderByPaidTime(!isOrderByPaidTime) }}
                                                        checked={isOrderByPaidTime}
                                                    />
                                                    <CLabel htmlFor="isOrderByPaidTime">เรียงตามเวลาชำระเงิน </CLabel>
                                                </CFormGroup>
                                            </CCol>
                                        </CRow>
                                        <CRow>
                                            <CCol xs="3">
                                                <CFormGroup>
                                                    <CLabel htmlFor="stockRefId">Stock Ref ID</CLabel>
                                                    <CInput onChange={e => setStockRefId(e.target.value)} value={stockRefId} id="stockRefId" />
                                                </CFormGroup>
                                            </CCol>
                                        </CRow> */}
                    {/* <CRow>
                                            <CCol xs="12">
                                                <CFormGroup>
                                                    <CLabel htmlFor="product">สินค้า</CLabel>
                                                    <CSelect onChange={e => setProduct(e.target.value)} value={product} id="product">
                                                    </CSelect>
                                                </CFormGroup>
                                            </CCol>
                                        </CRow> */}
                    {/* <CRow>
                                            <CCol xs="4">
                                                <CFormGroup>
                                                    <CLabel htmlFor="receipt">Receipt:</CLabel>
                                                    <CSelect onChange={e => setReceipt(e.target.value)} value={receipt} id="receipt">
                                                        <option value="1">No filter</option>
                                                        <option value="2">Include</option>
                                                        <option value="3">Exclude</option>
                                                    </CSelect>
                                                </CFormGroup>
                                            </CCol>
                                        </CRow>
                                        <CRow>
                                            <CCol xs="12">
                                                <h2>Subscription</h2>
                                            </CCol>
                                        </CRow>
                                        <CRow>
                                            <CCol xs="4">
                                                <CFormGroup>
                                                    <CLabel htmlFor="subscription">Subscription:</CLabel>
                                                    <CSelect onChange={e => setSubscription(e.target.value)} value={subscription} id="subscription">
                                                        <option value="1">No filter</option>
                                                        <option value="2">Is subscription</option>
                                                        <option value="3">Not subscription</option>
                                                    </CSelect>
                                                </CFormGroup>
                                            </CCol>
                                            <CCol xs="4">
                                                <CFormGroup>
                                                    <CLabel htmlFor="id">ID</CLabel>
                                                    <CInput onChange={e => setID(e.target.value)} value={id} id="id" />
                                                </CFormGroup>
                                            </CCol>
                                        </CRow> */}
                    <CRow>
                      <CCol xs="12" className={"text-right"}>
                        <CButton
                          type="submit"
                          color="primary-custom"
                          variant="reverse"
                        >
                          ค้นหา
                        </CButton>{" "}
                        <CButton
                          type="button"
                          color="danger-custom"
                          variant="reverse"
                          onClick={() => func_clear_search()}
                        >
                          ล้างข้อมูล
                        </CButton>{" "}
                        <CButton
                          color="success-custom"
                          variant="reverse"
                          onClick={() => onExport()}
                        >
                          ออกรายงาน
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCollapse>
            </CCard>
          </CFade>
        </CCol>
        <CCol xs="12">
          <CCard>
            <CCardHeader>รายการสั่งซื้อ</CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12">
                  <Tables refreshData={getData} data={dataTable} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};
export default InvoiceList;
