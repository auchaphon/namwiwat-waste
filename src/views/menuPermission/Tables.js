import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterMenu from '../../api/ApiMasterMenu';
import Swal from "sweetalert2";

const DemoTable = ({ data = [], refreshData = () => { } }) => {
    const [modal, setModal] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);
    const [name, setName] = useState('');
    const [code, setCode] = useState('');
    const [dataSelected, setDataSelected] = useState('');

    const toggle = () => {
        setModal(!modal);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const newDetails = (record) => {
        toggle();
        setDataSelected('');
        setName('');
        setCode('');
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
        setName(record.menu_name);
    }


    const editDetails = (record) => {
        toggle();
        setDataSelected(record);
        setName(record.menu_name);
        setCode(record.menu_code);
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        // { key: 'menu_code', label: "รหัสเมนู", _style: { width: '15%' } },
        { key: 'menu_name', label: "ชื่อเมนู", _style: { width: '15%' } },
        { key: 'admin', label: "Admin", _style: { width: '15%' } },
        { key: 'user_branch', label: "User Branch", _style: { width: '15%' } },
        { key: 'user_team', label: "User Team", _style: { width: '15%' } },
        { key: 'user', label: "Head Office", _style: { width: '15%' } },
    ]

    const onSubmit = (e) => {
        e.preventDefault();
        const model = {
            "menu_code": code,
            "menu_name": name,
            "id": dataSelected.id,
        }
        saveData(model);
    }

    const onDelete = async () => {
        try {
            const model = {
                "id": dataSelected.id,
                "record_status": "I"
            }
            const result = await ApiMasterMenu.save(model);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }


    const saveData = async (data) => {
        try {
            const result = await ApiMasterMenu.save(data);
            if (result.status === 200) {
                const { data } = result.data;
                // toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const onSwitch = (e, type, item) => {
        e.preventDefault();
        var model = {};
        if (type == "active_admin"){
            model = {
                "active_admin": item.active_admin == 1 ? 0 : 1,
                "id": item.id,
            }
        }
        else if (type == "active_user_branch"){
            model = {
                "active_user_branch": item.active_user_branch == 1 ? 0 : 1,
                "id": item.id,
            }
        }
        else if (type == "active_user_team"){
            model = {
                "active_user_team": item.active_user_team == 1 ? 0 : 1,
                "id": item.id,
            }
        }
        else if (type == "active_user"){
            model = {
                "active_user": item.active_user == 1 ? 0 : 1,
                "id": item.id,
            }
        }
        saveData(model);
    }

    return (
        <>
            <CCardBody>
                <CDataTable
                    items={data}
                    fields={fields}
                    hover
                    sorter
                    striped
                    bordered
                    pagination
                    scopedSlots={{
                        'order': (item) => (
                                <td style={{ textAlign: 'center' }}>
                                    {item.order}
                                </td>
                        ),
                        'admin': (item) => (
                                <td>
                                    <CSwitch className={'mx-1'} shape={'pill'} color={'primary'} variant="outline" labelOn={'\u2713'} labelOff={'\u2715'} checked={item.active_admin == 1} onChange={(e) => onSwitch(e, "active_admin", item )} />
                                </td>
                        ),
                        'user_branch': (item) => (
                                <td>
                                    <CSwitch className={'mx-1'} shape={'pill'} color={'primary'} variant="outline" labelOn={'\u2713'} labelOff={'\u2715'} checked={item.active_user_branch == 1} onChange={(e) => onSwitch(e, "active_user_branch", item )}  />
                                </td>
                        ),
                        'user_team': (item) => (
                                <td>
                                    <CSwitch className={'mx-1'} shape={'pill'} color={'primary'} variant="outline" labelOn={'\u2713'} labelOff={'\u2715'} checked={item.active_user_team == 1} onChange={(e) => onSwitch(e, "active_user_team", item )}  />
                                </td>
                        ),
                        'user': (item) => (
                                <td>
                                    <CSwitch className={'mx-1'} shape={'pill'} color={'primary'} variant="outline" labelOn={'\u2713'} labelOff={'\u2715'} checked={item.active_user == 1}  onChange={(e) => onSwitch(e, "active_user", item )} disabled={item.menu_code == "user" || item.menu_code == "branch"} />
                                </td>
                        ),
                        
                    }}
                />
                <CModal
                    show={modalConfirm}
                    onClose={setModalConfirm}
                    color="danger-custom"
                >
                    <CModalHeader closeButton>
                        <CModalTitle>ลบรายการ : {name}</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        คุณต้องการลบรายการนี้ ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={() => setModalConfirm(false)}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CModal>
                <CModal
                    show={modal}
                    onClose={toggle}
                >
                    <CModalHeader closeButton>
                        <CModalTitle>รายละเอียด เมนู</CModalTitle>
                    </CModalHeader>
                    <CForm onSubmit={onSubmit} action="" >
                        <CModalBody>
                            <CRow>
                                <CCol xs="12" sm="12">
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel htmlFor="code">รหัสเมนู</CLabel>
                                                <CInput onChange={e => setCode(e.target.value)} value={code} id="code" required readOnly={dataSelected.id} />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                    <CRow>
                                        <CCol xs="12">
                                            <CFormGroup>
                                                <CLabel htmlFor="name">ชื่อเมนู</CLabel>
                                                <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                            </CFormGroup>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow >
                        </CModalBody>
                        <CModalFooter>
                            <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                            <CButton
                                color="secondary"
                                onClick={toggle}
                            >ยกเลิก</CButton>
                        </CModalFooter>
                    </CForm >
                </CModal >
            </CCardBody >
        </>
    )
}

export default DemoTable
