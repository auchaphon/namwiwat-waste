import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton
} from '@coreui/react'
import Tables from './Tables';
import ApiMasterMenu from '../../api/ApiMasterMenu';
import { useDispatch, useSelector } from "react-redux";
import { setLocalStorage } from "../../utils/localStorage.js";
import ApiTranLogBack from '../../api/ApiTranLogBack';

const Team = (props) => {
  const id = new URLSearchParams(props.location.search).get("id")
  const [dataTable, setDataTable] = useState([]);
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);

    const getData = async () => {
        try {
            const result = await ApiMasterMenu.get(id);
            if (result.status === 200) {
                const { data } = result.data;
                dispatch({ type: "set_user", user: state.user, token: state.token, menu: data });
                setLocalStorage("menu", data);

                for (let i = 0; i < data.length; i++) {
                    data[i].order = i+1
                }
                setDataTable(data);
            }
        } catch (error) {

        }
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> ข้อมูล สิทธิ์การเข้าถึงเมนู ({dataTable.length}) </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} id={id} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default Team