import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CRow,
  CButton,
  CLabel,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterCustomer from "../../api/ApiMasterCustomer";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import "moment/locale/th";

const CustomerReport = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const userState = useSelector((state) => state.user);

  useEffect(() => {
    // getData();
    return () => { };
  }, []);

  const onExport = async () => {
    try {
      var result = await ApiMasterCustomer.onExport({
        email: email,
        name: name,
        phonenumber: phonenumber
      });
      if (result.status === 200) {
        window.open(result.data)
      }
    } catch (error) { }
  };


  const resetSearchOption = () => {
    setEmail("");
    setName("");
    setPhonenumber("")
  };
  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CRow>
            <CCol xs="12">
              <span style={{color: "red"}}>* รายงานแสดงข้อมูลลูกค้าจะแสดงผลในรูปแบบ Excel กรุณากดปุ่ม รายงาน เพื่อ download ไฟล์</span>
            </CCol>
          </CRow>
          <CCard className="mt-3">
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>ชื่อ-นามสกุล</CLabel>
                  <CInput
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    id="name"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>อีเมล</CLabel>
                  <CInput
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="Email"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>เบอร์โทร</CLabel>
                  <CInput
                    onChange={(e) => setPhonenumber(e.target.value)}
                    value={phonenumber}
                    id="phonenumber"
                  />
                </CCol>
                <CCol sm="4"></CCol>
                <CCol sm="2" className="pt-4">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="success-custom"
                        variant="reverse"
                        block
                        onClick={() => onExport()}
                      >
                        <b> ออกรายงาน </b>
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default CustomerReport;
