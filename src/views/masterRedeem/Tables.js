import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea,
    CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { }, func_add_log }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [name_en, setName_en] = useState('')
    const [description_en, setDescription_en] = useState('')
    const [point, setPoint] = useState('')
    const [dataSelected, setDataSelected] = useState('');
    const [picture, setPicture] = useState('')
    const [campaignId, setCampaignId] = useState('')
    const [showImage, setShowImage] = useState('')

    const [campaign, setCampaign] = useState([])

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const getAllCampaign = async () => {
        try {
            const result = await ApiMasterCampaign.getAllMtCampaign();
            if (result.status === 200) {
                const { data } = result.data;
                setCampaign(data);
            }
        } catch (error) {

        }
    }

    const func_attachFile = async (e) => {
        if (e.files.length > 0) {
            var file = e.files[0];

            var type = ["image/png", "image/jpg", "image/jpeg"];
            var message = "";
            var check = CheckFile({
                file,
                size: 10,
                type: type,
                message,
            });
            if (check) {
                setPicture(file)
            }
        }
    };

    const newDetails = () => {
        setCampaign([])
        setName('')
        setDescription('')
        setName_en('')
        setDescription_en('')
        setPoint('')
        setCampaignId('')
        setPicture('')
        getAllCampaign()
        toggleNew();
    }

    const onSubmitNew = (e) => {
        e.preventDefault();
        var formdata = new FormData();
        formdata.append("name", name);
        formdata.append("description", description);
        formdata.append("name_en", name_en);
        formdata.append("description_en", description_en);
        formdata.append("point", point);
        formdata.append("campaign_id", campaignId);
        formdata.append("createby", 0);
        formdata.append("updateby", 0);
        formdata.append("file", picture);
        newDataMtRedeem(formdata);
    }

    const newDataMtRedeem = async (data) => {
        try {
            func_add_log("Insert Redeem Item", JSON.stringify(data))
            const result = await ApiMasterRedeem.insertDataMtRedeem(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
    }

    const editDetails = (record) => {
        toggle();
        getAllCampaign()
        setDataSelected(record);
        setName(record.name)
        setDescription(record.description)
        setName_en(record.name_en)
        setDescription_en(record.description_en)
        setPoint(record.point)
        setCampaignId(record.campaign_id)
        setShowImage(record.image)
        setPicture('')
        document.getElementById("myFile").value = "";
    }

    const onSubmitEdit = (e) => {
        e.preventDefault();
        var formdata = new FormData();
        formdata.append("name", name);
        formdata.append("description", description);
        formdata.append("name_en", name_en);
        formdata.append("description_en", description_en);
        formdata.append("point", point);
        formdata.append("updateby", 0);
        formdata.append("campaign_id", campaignId);
        formdata.append("id", dataSelected.id);
        formdata.append("image", showImage);
        formdata.append("file", picture);
        editDataMtRedeem(formdata);
    }

    const editDataMtRedeem = async (data) => {
        try {
            func_add_log("Update Redeem Item", JSON.stringify(data))
            const result = await ApiMasterRedeem.updateMtRedeem(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }

    const func_change_status = async (e, record) => {
      record.record_status = e ? "A" : "I"
      update_status(record);
    };

    const update_status = async (data) => {
        try {
            func_add_log("Update Redeem Item", JSON.stringify(data))
            const result = await ApiMasterRedeem.update_status(data);
            if (result.status === 200) {
                const { data } = result.data;
                // toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }


    const onDelete = async () => {
        try {
            const id = dataSelected.id
            func_add_log("Delete Redeem Item", JSON.stringify(id))
            const result = await ApiMasterRedeem.deleteMtRedeem(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { minWidth: '50px' } },
        {
            key: 'option',
            label: '',
            _style: { minWidth: '100px' },
            filter: false,
        },
        {
          key: "record_status",
          label: "สถานะ",
          _style: { minWidth: "150px", textAlign: "center" },
          filter: false,
        },
        { key: 'image', label: "รูปภาพ", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'name', label: "ชื่อของรางวัล", _style: { minWidth: '300px', textAlign: "center" } },
        { key: 'name_en', label: "ชื่อของรางวัล EN", _style: { minWidth: '300px', textAlign: "center" } },
        { key: 'point', label: "แต้ม", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'updatedate', label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { minWidth: '150px' , textAlign: "center"} },
        

    ]

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton
                        onClick={() => newDetails()}
                        color="primary-custom"
                        variant="reverse"
                    >
                        <CIcon name="cil-plus" />
                        <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
                    </CButton>
                </CCol>
            </CRow>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'point':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.point ?? ""}
                            </td>
                        ),
                    'name_en':
                        (item) => (
                            <td>
                                {item.name_en ?? ""}
                            </td>
                        ),
                    "start_date": (item) => (
                        <td className="text-center">
                            {item.start_date ? moment(item.start_date)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "end_date": (item) => (
                        <td className="text-center">
                            {item.end_date ? moment(item.end_date)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td className="text-center">
                            {item.updatedate ? moment(item.updatedate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createby": (item) => (
                        <td>
                            {item.createby ? item.createby : ""}
                        </td>
                    ),
                    "updateby": (item) => (
                        <td>
                            {item.updateby ? item.updateby : ""}
                        </td>
                    ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.id}`} src={edit_icon} />
                                    </CButton>
                                    {/* <CButton
                                        className="ml-3"
                                        color="danger-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                        onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                    >
                                        <img id={`delete-${item.id}`} src={trash_icon} />
                                    </CButton> */}
                                </CButtonGroup>
                            </td>
                        ),
                    record_status: (item, index) => (
                        <td align="center">
                        <CSwitch
                            key={index}
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={item.record_status == "A" ? true : false}
                            onChange={(e) =>
                            func_change_status(e.target.checked, item)
                            }
                        />
                        </td>
                    ),
                    image: (item) => (
                        <td>
                            {item.signaturePath != "" && item.image != null && (
                                <img style={{ height: "100px" }} src={WEB_API + item.image}></img>
                            )}
                        </td>
                    ),

                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name"> ชื่อของรางวัล <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name_en"> ชื่อของรางวัล EN<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName_en(e.target.value)} value={name_en} id="name_en" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">แต้ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setPoint(e.target.value)} value={point} id="value" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectProduct">แคมเปญ<span style={{color:'red'}}>*</span></CLabel>
                                            <CSelect custom name="selectProduct" id="selectProduct" onChange={(e) => setCampaignId(e.target.value)} value={campaignId}>
                                                <option>เลือก 1 รายการ</option>
                                                {campaign.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.title}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด</CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description_en"> รายละเอียด EN</CLabel>
                                            <ReactQuill onChange={(e) => setDescription_en(e)} modules={modules} value={description_en} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="image">รูปภาพ<span style={{ color: 'red' }}>* ขนาดที่แนะนำ 800 x 600 pixels</span></CLabel>
                                            <input onChange={(e) => func_attachFile(e.target)} type="file" id="myFile" />
                                            {
                                                picture && <img className="mt-2" style={{ height: "100px" }} src={URL.createObjectURL(picture)}></img>
                                            }
                                            {
                                                (showImage && !picture) && <img className="mt-2" style={{ height: "100px" }} src={WEB_API + showImage}></img>
                                            }
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

            <CModal
                show={modalNew}
                onClose={toggleNew}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitNew} action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name"> ชื่อของรางวัล <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName(e.target.value)} value={name} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name_en"> ชื่อของรางวัล EN<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setName_en(e.target.value)} value={name_en} id="name_en" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">แต้ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setPoint(e.target.value)} value={point} id="value" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectProduct">แคมเปญ<span style={{color:'red'}}>*</span></CLabel>
                                            <CSelect custom name="selectProduct" id="selectProduct" onChange={(e) => setCampaignId(e.target.value)} value={campaignId}>
                                                <option>เลือก 1 รายการ</option>
                                                {campaign.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.title}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด</CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description_en"> รายละเอียด EN</CLabel>
                                            <ReactQuill onChange={(e) => setDescription_en(e)} modules={modules} value={description_en} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="image">รูปภาพ<span style={{ color: 'red' }}>* ขนาดที่แนะนำ 800 x 600 pixels</span></CLabel>
                                            <input onChange={(e) => func_attachFile(e.target)} type="file" id="picture" />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
