import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput,
    CSelect
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranLogBack from '../../api/ApiTranLogBack';
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiUser from '../../api/ApiUser';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

const MasterRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [code, setCode] = useState("");
    const [name, setName] = useState("");
    const [point, setPoint] = useState("");
    const [qDate, setQDate] = useState({ startDate: "", endDate: "" });
    const [qFocused, setQFocused] = useState();
    const [record_status, set_record_status] = useState("A");

    useEffect(() => {
        getData();
        func_add_log("Click Menu Master Redeem", "{}")
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    // useEffect(() => {
    //     getData();
    //     return () => {
    //     }
    // }, [name]);

    const getData = async () => {
        try {
            var  model = {
                name: name,
                fromdate: qDate.startDate,
                todate: qDate.endDate,
                record_status: record_status
            }
            func_add_log("Search Menu Master Redeem", JSON.stringify(model))
            const result = await ApiMasterRedeem.getAllMtRedeem(model);
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1)
                for (let i = 0; i < res.length; i++) {
                    res[i].order = i + 1
                }
                setDataTable(res);
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         name: name,
        //         point: point
        //     })
        // }
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (name && name.length > 0) {
        //         result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
        //     }
        //     if (point && point.length > 0) {
        //         result = result.filter((x) => x.point.toString().toLowerCase().includes(point.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setName("");
        setQDate({startDate: "", endDate: ""})
        set_record_status("A");
        // setPoint("");
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    const onExport = async () => {
        try {
            var model = {
                name: name,
                fromdate: qDate.startDate,
                todate: qDate.endDate
            }
            func_add_log("Export Master Redeem", JSON.stringify(model))
            const result = await ApiMasterRedeem.onExport(model);
            if (result.status === 200) {
              window.open(result.data)
            }
        } catch (error) {

        }
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>ชื่อของรางวัล</CLabel>
                                    <CInput onChange={e => setName(e.target.value)} value={name} id="name" />
                                </CCol>
                                <CCol sm="4">
                                    <CLabel>วันที่สร้าง</CLabel>
                                    <DateRangePicker
                                        displayFormat="DD/MM/YYYY"
                                        small
                                        block
                                        align="center"
                                        startDatePlaceholderText="ตั้งแต่"
                                        startDate={qDate.startDate}
                                        startDateId="startQDate"
                                        endDate={qDate.endDate}
                                        endDateId="endQDate"
                                        endDatePlaceholderText="ถึง"
                                        onDatesChange={(value) => setQDate(value)}
                                        focusedInput={qFocused}
                                        onFocusChange={(focusedInput) =>
                                            setQFocused(focusedInput)
                                        }
                                        orientation="horizontal"
                                        openDirection="down"
                                        minimumNights={0}
                                        
                                        isOutsideRange={() => false}
                                    />
                                </CCol>
                                {/* <CCol sm="2">
                                    <CLabel>แต้ม</CLabel>
                                    <CInput onChange={e => setPoint(e.target.value)} value={point} id="point" />
                                </CCol> */}
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                    </CSelect>
                                </CCol>
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="success-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onExport()}
                                            >
                                                <b> ออกรายงาน </b>
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> การจัดการของรางวัล </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} func_add_log={func_add_log} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default MasterRedeem