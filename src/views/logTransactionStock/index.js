import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranStock from '../../api/ApiTranStock';
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import moment from "moment";
import ApiTranLogBack from '../../api/ApiTranLogBack';

const LogTransactionStock = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [qDate, setQDate] = useState({ startDate: "", endDate: "" });
    const [qFocused, setQFocused] = useState();
    const [state, setState] = useState({
        id_relate: "",
        name_product: "",
    });

    useEffect(() => {
        getData();
        func_add_log("Click Menu Log Tran Stock", "{}")
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    const getData = async () => {
        try {
            var model = {
                name: state.name_product,
                sku: state.id_relate,
                fromdate: qDate.startDate,
                todate: qDate.endDate
            }
            func_add_log("Search Menu Log Tran Stock", JSON.stringify(model))
            const result = await ApiTranStock.GetLogTranStock(model);
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                // for (let i = 0; i < data.length; i++) {
                //     data[i].order = i + 1
                // }
                setDataTable(data.sort((a, b) => a.createdate > b.createdate ? -1: 1));
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         id_relate: state.id_relate,
        //         name_product: state.name_product
        //     })
        // }
        // // await ApiTranLogBack.addlog(model);
        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (state.id_relate && state.id_relate.length > 0) {
        //         result = result.filter((x) => x.id_relate.toLowerCase().includes(state.id_relate.toLowerCase()));
        //     }
        //     if (state.name_product && state.name_product.length > 0) {
        //         result = result.filter((x) => x.name_product.toLowerCase().includes(state.name_product.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setState({
            id_relate: "",
            name_product: "",
        });
        setQDate({startDate: "", endDate: ""})
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    const onExport = async () => {
      try {
        var model ={
            name: state.name_product,
            sku: state.id_relate,
            fromdate: qDate.startDate,
            todate: qDate.endDate
        }
        func_add_log("Export Log Tran Stock", JSON.stringify(model))
        var result = await ApiTranStock.onLogExport(model);
        if (result.status === 200) {
          window.open(result.data)
          // const link = document.createElement("a");
          // link.href = result.data;
          // var fileName = `customer_report_${moment().format(
          //   "yyyyMMDD"
          // )}.xlsx`;
          // link.setAttribute("download", fileName); //or any other extension
          // document.body.appendChild(link);
          // link.click();
        }
      } catch (error) { }
    };

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>รหัสสินค้า</CLabel>
                                    <CInput onChange={e => setState({ ...state, id_relate: e.target.value })} value={state.id_relate} id="id_relate" />
                                </CCol>
                                <CCol sm="2">
                                    <CLabel>ชื่อสินค้า</CLabel>
                                    <CInput onChange={e => setState({ ...state, name_product: e.target.value })} value={state.name_product} id="name_product" />
                                </CCol>
                                <CCol sm="4">
                                    <CLabel>วันที่สร้างข้อมูล</CLabel>
                                    <DateRangePicker
                                        displayFormat="DD/MM/YYYY"
                                        small
                                        block
                                        align="center"
                                        startDatePlaceholderText="ตั้งแต่"
                                        startDate={qDate.startDate}
                                        startDateId="startQDate"
                                        endDate={qDate.endDate}
                                        endDateId="endQDate"
                                        endDatePlaceholderText="ถึง"
                                        onDatesChange={(value) => setQDate(value)}
                                        focusedInput={qFocused}
                                        onFocusChange={(focusedInput) =>
                                            setQFocused(focusedInput)
                                        }
                                        orientation="horizontal"
                                        openDirection="down"
                                        minimumNights={0}
                                        
                                        isOutsideRange={() => false}
                                    />
                                </CCol>
                                <CCol sm="4" className="pt-4">
                                    <CRow>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="primary-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onSearch()}
                                            >
                                                <CIcon name="cil-search" style={{ marginRight: "2px" }} />{" "}
                                                <b> ค้นหา </b>
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="danger-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => resetSearchOption()}
                                            >
                                                <b> ล้างข้อมูล </b>{" "}
                                            </CButton>
                                        </CCol>
                                        <CCol xs="4">
                                            <CButton
                                                className="mt-1"
                                                color="success-custom"
                                                variant="reverse"
                                                block
                                                onClick={() => onExport()}
                                            >
                                                <b> ออกรายงาน </b>
                                            </CButton>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> รายการเข้า - ออก คลังสินค้า </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default LogTransactionStock