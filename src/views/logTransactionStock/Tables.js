import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CInputCheckbox,
    CCard,
    CSelect,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const fields = [
        { key: 'sku', label: 'รหัสสินค้า', _style: { minWidth: "250px", textAlign: "center" } },
        { key: 'name_product', label: "ชื่อสินค้า", _style: { minWidth: "200px", textAlign: "center" } },
        // { key: 'type_product', label: "ประเภทสินค้า", _style: { width: '8%' } },
        { key: 'stock_change', label: "จำนวนสินค้า เข้า - ออก", _style: { minWidth: "200px", textAlign: "center" } },
        { key: 'stock_balance', label: "จำนวนสินค้าคงเหลือ", _style: { minWidth: "200px", textAlign: "center" } },
        { key: 'createdate', label: "วันที่สร้างข้อมูล", _style: { minWidth: "200px", textAlign: "center" } },
        { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: "150px", textAlign: "center"} },
    ]

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    "stock_change": (item) => (
                        <td className="text-center">
                            {item.stock_change ?? ""}
                        </td>
                    ),
                    "stock_balance": (item) => (
                        <td className="text-center">
                            {item.stock_balance ?? ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                }}
            />
        </>
    )
}

export default DemoTable
