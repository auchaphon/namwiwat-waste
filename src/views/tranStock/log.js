import React, { useEffect, useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CInputCheckbox,
    CInputRadio,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Swal from "sweetalert2";
import ApiTranStock from '../../api/ApiTranStock';
import { useHistory } from "react-router";
import moment from 'moment';
import ApiTranLogBack from '../../api/ApiTranLogBack';

const Log = (props) => {
    var id = new URLSearchParams(props.location.search).get("id");
    var type = new URLSearchParams(props.location.search).get("type");

    const history = useHistory();
    const [data, setData] = useState([]);

    useEffect(() => {
        getData();
        func_add_log("View Tran Stock Log", JSON.stringify({id_relate: id, type: type}))
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    const getData = async () => {
        try {
            const result = await ApiTranStock.GetTranStockByIdRelate({id_relate: id, type: type});
            if (result.status === 200) {
                const { data } = result.data;
                setData(data);
            }
        } catch (error) {

        }
    }

    const fields = [
        { key: 'createdate', label: "วันที่สร้าง", _style: { width: '15%' } },
        // { key: 'updatedate', label: "วันที่แก้ไขล่าสุด", _style: { width: '15%' } },
        // { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        // { key: 'type', label: "ชนิดสินค้า", _style: { width: '15%' } },
        // { key: 'name', label: "ชื่อสินค้า", _style: { width: '15%' } },
        { key: 'stock_change', label: "การเปลี่ยนแปลงสินค้า", _style: { width: '15%' } },
        { key: 'stock_balance', label: "สินค้าคงเหลือ", _style: { width: '15%' } },
        // { key: 'qty', label: "สินค้าคงเหลือ", _style: { width: '15%' } },
        // { key: 'option', label: "", _style: { width: '15%' } }

    ]

    return(
        <>
            <CRow>
                <CCol xs="12">
                    <CButton color="info-custom" variant="reverse" onClick={()=>history.go(-1)}>กลับ</CButton>
                </CCol>
            </CRow>
            <CRow>
                <CCol xs="12">
                    <CCard>
                        <CCardHeader>สต็อกสินค้า</CCardHeader>
                        <CCardBody>
                            <CRow>
                                <CCol xs="12">
                                    <CDataTable
                                        items={data}
                                        fields={fields}
                                        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                                        cleaner
                                        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                                        itemsPerPage={10}
                                        hover
                                        sorter
                                        striped
                                        bordered
                                        pagination
                                        scopedSlots={{
                                            "createdate": (item) => (
                                                <td>
                                                    {item.createdate? moment(item.createdate).format("DD-MM-YYYY HH:mm") : ""}
                                                </td>
                                            ),

                                        }}
                                    />
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
}

export default Log