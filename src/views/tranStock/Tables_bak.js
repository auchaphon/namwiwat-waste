import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CInputCheckbox,
    CInputRadio,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiTranStock from '../../api/ApiTranStock';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [idRelate, setIdRelate] = useState('')
    const [stockChange, setStockChange] = useState('')
    const [radios, setRadios] = useState('')
    const [mtProduct, setMtProduct] = useState([])

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const getProduct = async (e) => {
        setRadios(e.target.value)
        try {
            const result = await ApiTranStock.GetAllTranStockProduct();
            if (result.status === 200) {
                const { data } = result.data;
                setMtProduct(data);
            }
        } catch (error) {

        }
    }

    const getRedeem = async (e) => {
        setRadios(e.target.value)
        try {
            const result = await ApiTranStock.GetAllTranStockRedeem();
            if (result.status === 200) {
                const { data } = result.data;
                setMtProduct(data);
            }
        } catch (error) {

        }
    }

    const newDetails = () => {
        document.getElementById("checkbox1").checked = "";
        document.getElementById("checkbox2").checked = "";
        setStockChange('')
        setRadios('')
        setIdRelate('')
        setMtProduct([])
        toggleNew();
    }

    const onSubmitNew = async (e) => {

        const checkTranStock = {
            "id_relate": idRelate,
            "type": radios,
        }

        try {
            e.preventDefault();
            const result = await ApiTranStock.GetTranStockByIdRelate(checkTranStock);
            if (result.status === 200) {
                const { data } = result.data;
                const length = data.length
                if (length > 0) {
                    const stockBalance = data[length - 1].stock_balance
                    const newStockBalance = +stockChange + +stockBalance
                    if (newStockBalance < 0 ) {
                        throw 400
                    }
                    const model = {
                        "type": radios,
                        "id_relate": idRelate,
                        "stock_change": stockChange,
                        "stock_balance": newStockBalance,
                        "createdate": data[0].createdate,
                        "createby": data[0].createby,
                        "updateby": 0
                    }
                    updateDataTranStock(model);
                }
                else {
                    if(stockChange < 0){
                        throw 400
                    }
                    const model = {
                        "type": radios,
                        "id_relate": idRelate,
                        "stock_change": stockChange,
                        "stock_balance": stockChange,
                        "createby": 0,
                        "updateby": 0
                    }
                    newDataTranStock(model);
                }
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: "กรุณาเลือกข้อมูล หรือ สินค้ามีค่าติดลบ",
            });
        }
    }

    const newDataTranStock = async (data) => {
        try {
            const result = await ApiTranStock.insertDataTranStock(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    // const deleteDetails = (record) => {
    //     toggleConfirm();
    //     setDataSelected(record);
    // }

    // const editDetails = (record) => {
    //     toggle();
    //     console.log(record)
    //     setDataSelected(record);
    //     setName(record.name)
    //     setDescription(record.description)
    //     setPoint(record.point)
    //     setStartDate(new Date(record.start_date))
    //     setEndDate(new Date(record.end_date))
    //     setShowImage(record.image)
    //     setPicture('')
    // }

    // const onSubmitEdit = (e) => {
    //     e.preventDefault();
    //     var formdata = new FormData();
    //     formdata.append("name", name);
    //     formdata.append("description", description);
    //     formdata.append("point", point);
    //     formdata.append("start_date", startDate.toISOString());
    //     formdata.append("end_date", endDate.toISOString());
    //     formdata.append("updateby", 0);
    //     formdata.append("id", dataSelected.id);
    //     formdata.append("image", showImage);
    //     formdata.append("file", picture);
    //     // editDataMtRedeem(formdata);
    // }

    const updateDataTranStock = async (data) => {
        try {
            const result = await ApiTranStock.updateBalanceTranStock(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }


    // const onDelete = async () => {
    //     try {
    //         const id = dataSelected.id
    //         const result = await ApiTranStock.deleteTranStock(id);
    //         if (result.status === 200) {
    //             const { data } = result.data;
    //             toggleConfirm();
    //             Swal.fire({
    //                 icon: "success",
    //                 title: "ลบสำเร็จ",
    //                 timer: 2000,
    //             }).then((success) => {
    //                 refreshData();
    //             });
    //         }
    //     } catch (error) {
    //         Swal.fire({
    //             icon: "error",
    //             title: error.response.data,
    //         });
    //     }
    // }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        { key: 'type', label: "ชนิดสินค้า", _style: { width: '15%' } },
        { key: 'name', label: "ชื่อสินค้า", _style: { width: '15%' } },
        { key: 'stock_change', label: "การเปลี่ยนแปลงสินค้า", _style: { width: '15%' } },
        { key: 'stock_balance', label: "สินค้าคงเหลือ", _style: { width: '15%' } },
        { key: 'qty', label: "สินค้าคงเหลือ", _style: { width: '15%' } },
        { key: 'createdate', label: "วันที่สร้าง", _style: { width: '15%' } },
        { key: 'updatedate', label: "วันที่แก้ไข", _style: { width: '15%' } }

    ]

    const testtest = (e) => {
        // setTagHtml(e.target.value)
        console.log(e)
        // console.log(tagHtml)
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton
                        onClick={() => newDetails()}
                        color="primary-custom"
                        variant="reverse"
                    >
                        <CIcon name="cil-plus" />
                        <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
                    </CButton>
                </CCol>
            </CRow>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    "type": (item) => (
                        <td>
                            {item.type === "P" ? "สินค้า" : "ของรางวัล"}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td>
                            {moment(item.updatedate)
                                .format("DD-MM-YYYY HH:mm")}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td>
                            {moment(item.createdate)
                                .format("DD-MM-YYYY HH:mm")}
                        </td>
                    ),
                    // 'option':
                    //     (item) => (
                    //         <td className="center">
                    //             <CButtonGroup>
                    //                 <CButton
                    //                     color="primary-custom"
                    //                     variant="reverse"
                    //                     shape="square"
                    //                     size="sm"
                    //                     onClick={() => { editDetails(item) }}
                    //                     onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                    //                     onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                    //                 >
                    //                     <img id={`edit-${item.id}`} src={edit_icon} />
                    //                 </CButton>
                    //                 <CButton
                    //                     className="ml-3"
                    //                     color="danger-custom"
                    //                     variant="reverse"
                    //                     shape="square"
                    //                     size="sm"
                    //                     onClick={() => { deleteDetails(item) }}
                    //                     onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                    //                     onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                    //                 >
                    //                     <img id={`delete-${item.id}`} src={trash_icon} />
                    //                 </CButton>
                    //             </CButtonGroup>
                    //         </td>
                    //     ),
                    // image: (item) => (
                    //     <td>
                    //         {item.signaturePath != "" && item.image != null && (
                    //             <img style={{ height: "100px" }} src={WEB_API + item.image}></img>
                    //         )}
                    //     </td>
                    // ),

                }}
            />

            {/* <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal> */}
            {/* <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '100%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputRadio name="radio" value="P" onChange={(e) => getProduct(e)} />
                                            <CLabel htmlFor="checkbox1">สินค้า</CLabel>
                                        </CFormGroup>
                                        <CFormGroup variant="checkbox" className="checkbox">
                                            <CInputRadio name="radio" onChange value="R" onChange={(e) => getRedeem(e)} />
                                            <CLabel htmlFor="checkbox2">ของรางวัล</CLabel>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="idParent">เลือกสินค้า</CLabel>
                                            <CSelect custom name="idParent" id="idParent" onChange={(e) => setIdRelate(e.target.value)}>
                                                <option>เลือก 1 อย่าง</option>
                                                {mtProduct.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.name}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">จำนวนสินค้า<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setStockChange(e.target.value)} value={point} id="value" value={stockChange} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal > */}

            <CModal
                show={modalNew}
                onClose={toggleNew}
                style={{ width: '100%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitNew} action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectType">เลือกชนิดสินค้า</CLabel>
                                            <CFormGroup variant="checkbox" className="checkbox" >
                                                <CInputRadio id="checkbox1" name="radio" value="P" onChange={(e) => getProduct(e)} />
                                                <CLabel htmlFor="checkbox1">สินค้า</CLabel>
                                            </CFormGroup>
                                            <CFormGroup variant="checkbox" className="checkbox">
                                                <CInputRadio id="checkbox2" name="radio" onChange value="R" onChange={(e) => getRedeem(e)} />
                                                <CLabel htmlFor="checkbox2">ของรางวัล</CLabel>
                                            </CFormGroup>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="selectProduct">เลือกสินค้า</CLabel>
                                            <CSelect custom name="selectProduct" id="selectProduct" onChange={(e) => setIdRelate(e.target.value)}>
                                                <option>เลือก 1 อย่าง</option>
                                                {mtProduct.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.name}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="value">จำนวนสินค้า<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setStockChange(e.target.value)} id="value" value={stockChange} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
