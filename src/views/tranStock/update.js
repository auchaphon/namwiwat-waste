import React, { useEffect, useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CInputCheckbox,
    CInputRadio,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Swal from "sweetalert2";
import ApiTranStock from '../../api/ApiTranStock';
import { useHistory } from "react-router";
import ApiTranLogBack from '../../api/ApiTranLogBack';
import ApiMasterProduct from '../../api/ApiMasterProduct';

const Update = (props) => {
    var id = new URLSearchParams(props.location.search).get("id");
    var type = new URLSearchParams(props.location.search).get("type");
    var func = new URLSearchParams(props.location.search).get("func");

    const history = useHistory();
    const [name, setName] = useState('');
    const [currentqty, setCurrentQty] = useState(0);
    const [qty, setQty] = useState(0);
    const [total, setTotal] = useState(0);
    const [safety_balance, set_safety_balance] = useState(0);
    const [attr, set_attr] = useState([]);
    const [sku, setSku] = useState('');

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    useEffect(() => {
        if (func == "add") {
            setTotal(parseInt(currentqty) + parseInt(qty));
        }else{
            setTotal(parseInt(currentqty) - parseInt(qty));
        }
        return () => {
        }
    }, [qty, currentqty]);

    const getData = async () => {
        try {
            const result = await ApiTranStock.GetTranStockDetail({id: id, type: type});
            if (result.status === 200) {
                const { data } = result.data;
                console.log("data Detail::",data);
                data.attributes.forEach(attr => {
                    attr.stock_change = 0
                });
                set_attr(data.attributes);
                setName(data.name);
                setSku(data.sku);
                setCurrentQty(data.stock_balance ?? 0);
                set_safety_balance(data.safety_balance ?? 0);
            }
        } catch (error) {

        }
    }

    const func_on_submit = (e) => {
        e.preventDefault();
        if (attr && attr.length > 0){
            var changelist = attr.filter((x) => x.stock_change > 0);
            changelist.forEach((item) => {
                var modelTranStock = {
                    "type": type,
                    "id_relate": id,
                    "sku": sku,
                    "sub_sku": item.sku,
                    "stock_change": func == "add" ? item.stock_change : item.stock_change * -1,
                    "stock_balance": (func == "add" ? item.stock_change : item.stock_change * -1) + item.stock_balance,
                    "createby": 0,
                    "updateby": 0
                }
                newDataTranStockAttr(modelTranStock);
            });
            var saveattr = [];
            attr.forEach((x) => {
                saveattr.push({
                    type: x.type,
                    name: x.name,
                    nameSelect: x.nameSelect,
                    sku: x.sku,
                    stock_balance: x.stock_balance + (func == "add" ? x.stock_change : x.stock_change * -1)
                })
            })
            var model = {
                id: id,
                attributes: saveattr
            }
            update_stock_balance_attr(model);
        }else if(type == "P"){
            var modelP = {
                id: id,
                stock_balance: total
            }
            update_stock_balance(modelP);
            const model = {
                "type": type,
                "id_relate": id,
                "sku": sku,
                "sub_sku": sku,
                "stock_change": func == "add" ? qty : qty * -1,
                "stock_balance": total,
                "createby": 0,
                "updateby": 0
            }
            newDataTranStock(model);
        }
        else{
            const model = {
                "type": type,
                "id_relate": id,
                "stock_change": func == "add" ? qty : qty * -1,
                "stock_balance": total,
                "createby": 0,
                "updateby": 0
            }
            newDataTranStock(model);
        }
    }

    const update_stock_balance = async (data) => {
        try {
            // func_add_log("Insert Tran Stock Data", JSON.stringify(data))
            const result = await ApiMasterProduct.update_stock(data);
            if (result.status === 200) {
                const { data } = result.data;
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    history.go(-1);
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const update_stock_balance_attr = async (data) => {
        try {
            // func_add_log("Insert Tran Stock Data", JSON.stringify(data))
            const result = await ApiMasterProduct.update_stock_attr(data);
            if (result.status === 200) {
                const { data } = result.data;
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    history.go(-1);
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const newDataTranStockAttr = async (data) => {
        try {
            func_add_log("Insert Tran Stock Data", JSON.stringify(data))
            const result = await ApiTranStock.insertDataTranStock(data);
            if (result.status === 200) {
                const { data } = result.data;
                // Swal.fire({
                //     icon: "success",
                //     title: "บันทึกสำเร็จ",
                //     timer: 2000,
                // }).then((success) => {
                //     history.go(-1);
                // });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const newDataTranStock = async (data) => {
        try {
            func_add_log("Insert Tran Stock Data", JSON.stringify(data))
            const result = await ApiTranStock.insertDataTranStock(data);
            if (result.status === 200) {
                const { data } = result.data;
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    history.go(-1);
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const func_setAttribute = (key, index, value) => {
      var data = [...attr];
      data[index][key] = value;
      set_attr(data);
    };

    return(
        <>
            <CRow>
                <CCol xs="4" hidden={attr && attr.length > 0}>
                    <CCard>
                        <CCardHeader>{func == "add" ? "เพิ่มสต็อกสินค้า" : "ลดสต็อกสินค้า"}</CCardHeader>
                        <CCardBody>
                            <CForm onSubmit={(e) => func_on_submit(e)} action=''>
                                <CRow>
                                    <CCol xs="6">ชื่อสินค้า:</CCol>
                                    <CCol xs="6" className={"text-right"}>{name}</CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">จำนวนสินค้าในสต็อก:</CCol>
                                    <CCol xs="6" className={"text-right"}>{currentqty}</CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor='qty'>{func == "add" ? "เพิ่มสินค้าจำนวน" : "ลดสินค้าจำนวน"}</CLabel>
                                            <CInput type='number' min={0} value={qty} id='qty' onChange={(e) => setQty(e.target.value)}/>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">จำนวนสินค้าในสต็อกล่าสุด:</CCol>
                                    <CCol xs="6" className={"text-right"}>{total}</CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CButton type="submit" color="info">ยืนยัน</CButton>
                                        <CButton className={"ml-2"} color="secondary" onClick={() => history.go(-1)}>ยกเลิก</CButton>
                                    </CCol>
                                    <CCol xs="6" className={"text-right"}>Safety Stock: {safety_balance}</CCol>
                                </CRow>
                            </CForm>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol xs="8" hidden={(!attr) || attr.length == 0}>
                    <CCard>
                        <CCardHeader>{func == "add" ? "เพิ่มสต็อกสินค้า" : "ลดสต็อกสินค้า"}</CCardHeader>
                        <CCardBody>
                            <CForm onSubmit={(e) => func_on_submit(e)} action=''>
                                <CRow className="mb-3">
                                    <CCol xs="6">ชื่อสินค้า:</CCol>
                                    <CCol xs="6" className={"text-right"}>{name}</CCol>
                                </CRow>
                                {attr.map((item, index) => {
                                    return (
                                        <CRow>
                                            <CCol xs="1">ขนาด: {item.name}</CCol>
                                            <CCol xs="3">จำนวนสินค้าในสต็อก: {item.stock_balance}</CCol>
                                            <CCol xs="2">{func == "add" ? "เพิ่มสินค้าจำนวน" : "ลดสินค้าจำนวน"}:</CCol>
                                            <CCol xs="3">
                                                <CFormGroup>
                                                    <CInput type='number' min={0} value={item.stock_change} onChange={(e) => func_setAttribute("stock_change", index, parseInt(e.target.value))}/>
                                                </CFormGroup>
                                            </CCol>
                                            <CCol xs="3">จำนวนสินค้าในสต็อกล่าสุด: {item.stock_balance + (func == "add" ? item.stock_change : item.stock_change * -1)}</CCol>
                                        </CRow>
                                    );
                                })}
                                {/* <CRow>
                                    <CCol xs="6">จำนวนสินค้าในสต็อกล่าสุด:</CCol>
                                    <CCol xs="6" className={"text-right"}>{total}</CCol>
                                </CRow> */}
                                <CRow>
                                    <CCol xs="6">
                                        <CButton type="submit" color="info">ยืนยัน</CButton>
                                        <CButton className={"ml-2"} color="secondary" onClick={() => history.go(-1)}>ยกเลิก</CButton>
                                    </CCol>
                                    <CCol xs="6" className={"text-right"}>Safety Stock: {safety_balance}</CCol>
                                </CRow>
                            </CForm>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
}

export default Update