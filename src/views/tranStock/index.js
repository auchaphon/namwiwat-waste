import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiMasterRedeem from "../../api/ApiMasterRedeem";
import ApiTranStock from "../../api/ApiTranStock";

const MasterRedeem = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [balance, setBalance] = useState("");
  const [sku, setSku] = useState("");

  useEffect(() => {
    getData();
    func_add_log("Click Menu Tran Stock", JSON.stringify({}));
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [name, type, sku]);

  const getData = async () => {
    try {
      var model = {
        name: name,
        sku: sku,
        type: type,
      };
      func_add_log("Search Menu Tran Stock", JSON.stringify(model));
      const result = await ApiTranStock.getAllTranStock(model);
      if (result.status === 200) {
        const { data } = result.data;
        var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1);
        setRawDataTable(res);
        for (let i = 0; i < res.length; i++) {
          res[i].order = i + 1;
          if (res[i].type === "P") {
            res[i].typeFilter = "สินค้า";
          }
          if (res[i].type === "R") {
            res[i].typeFilter = "ของรางวัล";
          }
        }
        setDataTable(res);
      }
    } catch (error) {}
  };

  const onSearch = async () => {
    getData();
    // var model = {
    //     activity_detail: "Menu Config Search",
    //     json: JSON.stringify({
    //         name: name,
    //         type: type,
    //         stock_balance: balance
    //     })
    // }

    // if(rawDataTable && rawDataTable.length > 0){
    //     var result = rawDataTable;
    //     if(name && name.length > 0){
    //         result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
    //     }
    //     if(type && type.length > 0){
    //         result = result.filter((x) => x.typeFilter.toLowerCase().includes(type.toLowerCase()));
    //     }
    //     if(balance && balance > 0){
    //         result = result.filter((x) => x.qty?.toString().toLowerCase().includes(balance.toLowerCase()));
    //     }
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i+1
    //     }
    //     setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    setName("");
    setType("");
    setSku("");
    // var result = rawDataTable;
    // for (let i = 0; i < result.length; i++) {
    //     result[i].order = i+1
    // }
    // setDataTable(result);
  };

  const onExport = async () => {
    try {
      var model = {
        name: name,
        sku: sku,
        type: type,
      };
      func_add_log("Export Tran Stock", JSON.stringify(model))
      var result = await ApiTranStock.onExport(model);
      if (result.status === 200) {
        window.open(result.data)
      }
    } catch (error) { }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>รหัสสินค้า</CLabel>
                  <CInput
                    onChange={(e) => setSku(e.target.value)}
                    value={sku}
                    id="sku"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชื่อสินค้า</CLabel>
                  <CInput
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    id="name"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชนิดสินค้า</CLabel>
                  <CSelect onChange={e => setType(e.target.value)} value={type} id="type">
                      <option value={""}>-</option>
                      <option value={"P"}>สินค้า</option>
                      <option value={"R"}>ของรางวัล</option>
                  </CSelect>
                </CCol>
                <CCol sm="2"></CCol>
                <CCol sm="4" className="pt-4">
                  <CRow>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                        <CButton
                            className="mt-1"
                            color="success-custom"
                            variant="reverse"
                            block
                            onClick={() => onExport()}
                        >
                            <b> ออกรายงาน </b>
                        </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> คลังสินค้า </b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default MasterRedeem;
