import React, { useState } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CForm,
  CModalTitle,
  CButtonGroup,
  CLink,
  CSwitch,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterConfig from "../../api/ApiMasterConfig";
import Swal from "sweetalert2";
import parse from "html-react-parser";
import api from "../../api/ApiMasterFontColour";
import moment from "moment";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");

const fields = [
  {
    key: "colourname",
    label: "สีตัวอักษร",
    _style: { minWidth: "300px", textAlign: "center" },
  },
  {
    key: "record_status",
    label: "สถานะ",
    _style: { minWidth: "150px", textAlign: "center" },
    filter: false,
  },
  {
    key: "createdate",
    label: "วันที่สร้างข้อมูลล่าสุด",
    _style: { minWidth: "150px", textAlign: "center" },
  },
  {
    key: "createby",
    label: "ผู้สร้างข้อมูล",
    _style: { minWidth: "150px", textAlign: "center" },
  },
  {
    key: "updatedate",
    label: "วันที่แก้ไขข้อมูลล่าสุด",
    _style: { minWidth: "150px", textAlign: "center" },
  },
  {
    key: "updateby",
    label: "ผู้แก้ไขข้อมูล",
    _style: { minWidth: "150px", textAlign: "center" },
  },
];


const DemoTable = ({ data = [], refreshData = () => {} }) => {
  const [modal, setModal] = useState(false);
  const [colourname, setColourName] = useState('');
  const [status, setStatus] = useState('A');
  const toggle = () => {
      setModal(!modal);
  };

  const func_change_status = async (e, record) => {
    record.record_status = e ? "A" : "I"
    saveData(record);
  };

  const newDetails = () => {
    setColourName('');
    setStatus('A');
    setModal(true)
  }

  const onSubmit = (e) => {
    e.preventDefault();
    const model = {
      id: 0,
      colourname: colourname,
      record_status: status
    }
    saveData(model);
  }

  const saveData = async (data) => {
    try {
      const result = await api.save(data);
      if (result.status === 200) {
        const { data } = result.data;
        Swal.fire({
          icon: "success",
          title: "แก้ไขสำเร็จ",
          timer: 2000,
        }).then((success) => { 
          toggle();
          refreshData();
         });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  }

  return (
    <>
      <CRow>
          <CCol xs="12" lg="12" >
              <CButton onClick={newDetails} color={'info'}><CIcon name="cil-plus" /><span className="ml-2">เพิ่มข้อมูล</span></CButton>
          </CCol>
      </CRow>
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          colourname: (item) => 
          <td style={{ textAlign: "center" }}>
            {item.colourname ? item.colourname : ""}
          </td>,
          record_status: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.record_status == "A" ? true : false}
                onChange={(e) =>
                  func_change_status(e.target.checked, item)
                }
              />
            </td>
          ),
          updatedate: (item) => (
            <td className="text-center">
              {item.updatedate
                ? moment(item.updatedate)
                    // .add(543, "years")
                    .format("DD-MM-YYYY HH:mm")
                : ""}
            </td>
          ),
          createdate: (item) => (
            <td className="text-center">
              {item.createdate
                ? moment(item.createdate)
                    // .add(543, "years")
                    .format("DD-MM-YYYY HH:mm")
                : ""}
            </td>
          ),
          createby: (item) => <td>{item.createby ? item.createby : ""}</td>,
          updateby: (item) => <td>{item.updateby ? item.updateby : ""}</td>,
        }}
      />
      
      <CModal
        show={modal}
        onClose={setModal}
      >
        <CModalHeader closeButton>
            <CModalTitle>เพิ่มข้อมูล</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="" >
            <CModalBody>
                <CRow>
                    <CCol xs="12" sm="12">
                      <CRow>
                          <CCol xs="12">
                              <CFormGroup>
                                  <CLabel htmlFor="colourname">ชื่อสี <span style={{color:'red'}}>*</span></CLabel>
                                  <CInput onChange={e => setColourName(e.target.value)} value={colourname} id="colourname" required/>
                              </CFormGroup>
                          </CCol>
                      </CRow>
                      <CRow>
                          <CCol xs="12">
                              <CFormGroup>
                                  <CLabel htmlFor="status">สถานะ</CLabel>
                                  <CSelect custom name="status" id="status" onChange={e => setStatus(e.target.value)} value={status} required>
                                    <option value="A">Active</option>
                                    <option value="I">Inactive</option>
                                  </CSelect>
                              </CFormGroup>
                          </CCol>
                      </CRow>
                    </CCol>
                </CRow >
            </CModalBody>
            <CModalFooter>
                <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                <CButton
                    color="secondary"
                    onClick={() => setModal(false)}
                >ยกเลิก</CButton>
            </CModalFooter>
        </CForm >
      </CModal >
    </>
  );
};

export default DemoTable;
