import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import api from "../../api/ApiMasterFontColour";

const Branch = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async () => {
    try {
      const result = await api.get();
      if (result.status === 200) {
        const { data } = result.data;
        // setRawDataTable(data);
        // for (let i = 0; i < data.length; i++) {
        //   data[i].order = i + 1;
        // }
        setDataTable(data);
      }
    } catch (error) {}
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> สีตัวอักษร ({dataTable.length}) </b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Branch;
