import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea,
    CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterBanner from '../../api/ApiMasterBanner';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
import { setHeaderFromDataAuth } from '../../utils';
import ApiTranLogBack from '../../api/ApiTranLogBack';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");


const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [seq, setSeq] = useState('')
    const [link, setLink] = useState('')
    const [dataSelected, setDataSelected] = useState('');
    const [picture, setPicture] = useState('')
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [showImage, setShowImage] = useState('')

    const func_add_log = async (detail, json) => {
        var model = {
            activity_detail: detail,
            json: json
        }
        await ApiTranLogBack.addlog(model);
    }

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const func_attachFile = async (e) => {
        if (e.files.length > 0) {
            var file = e.files[0];

            var type = ["image/png", "image/jpg", "image/jpeg"];
            var message = "";
            var check = CheckFile({
                file,
                size: 10,
                type: type,
                message,
            });
            if (check) {
                setPicture(file)
            }
        }
    };

    const newDetails = () => {
        setTitle('')
        setDescription('')
        setSeq('')
        setLink('')
        setStartDate(new Date())
        setEndDate(new Date())
        setPicture('')
        toggleNew();
    }

    const onSubmitNew = (e) => {
        e.preventDefault();
        var formdata = new FormData();
        formdata.append("id",dataSelected.id);
        formdata.append("title",title);
        formdata.append("description",description);
        formdata.append("seq",seq);
        formdata.append("link_relate",link);
        formdata.append("start_date", startDate.toISOString());
        formdata.append("end_date", endDate.toISOString());
        formdata.append("createby", 0);
        formdata.append("updateby", 0);
        formdata.append("file", picture);
        newDataMtBanner(formdata);
    }


    const newDataMtBanner = async (data) => {
        try {
            func_add_log("Insert Banner", JSON.stringify(data))
            const result = await ApiMasterBanner.insertDataMtBanner(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
    }

    const editDetails = (record) => {
        toggle();
        setDataSelected(record);
        setTitle(record.title)
        setDescription(record.description)
        setSeq(record.seq)
        setLink(record.link_relate)
        setStartDate(new Date(record.start_date))
        setEndDate(new Date(record.end_date))
        setShowImage(record.image)
        setPicture('')
        document.getElementById("myFile").value = "";
    }

    const onSubmitEdit = (e) => {
        e.preventDefault();
        var formdata = new FormData();
        formdata.append("id",dataSelected.id);
        formdata.append("title",title);
        formdata.append("description",description);
        formdata.append("seq",seq);
        formdata.append("link_relate",link);
        formdata.append("start_date", startDate.toISOString());
        formdata.append("end_date", endDate.toISOString());
        formdata.append("updateby", 0);
        formdata.append("image", showImage);
        formdata.append("file", picture);
        editDataMtBanner(formdata);
    }

    const func_change_status = async (e, record) => {
      record.record_status = e ? "A" : "I"
      delete record.order;
      update_status(record);
    };

    const editDataMtBanner = async (data) => {
        try {
            func_add_log("Update Banner", JSON.stringify(data))
            const result = await ApiMasterBanner.updateMtBanner(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const update_status = async (data) => {
        try {
            func_add_log("Update Banner", JSON.stringify(data))
            const result = await ApiMasterBanner.update_status(data);
            if (result.status === 200) {
                const { data } = result.data;
                // toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }


    const onDelete = async () => {
        try {
            const id = dataSelected.id
            func_add_log("Delete Banner", JSON.stringify(id))
            const result = await ApiMasterBanner.deleteMtBanner(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: "record_status", label: "สถานะ", _style: { minWidth: "50px", textAlign: "center" }, filter: false},
        { key: 'image', label: "รูปภาพ", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'title', label: "ชื่อแบนเนอร์", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'seq', label: "ลำดับแสดง", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'link_relate', label: "URL", _style: { minWidth: '300px', textAlign: "center" } },
        { key: 'start_date', label: "วันที่เริ่ม", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'end_date', label: "วันที่สิ้นสุด", _style: { minWidth: '150px', textAlign: "center" } },
        { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: '150px', textAlign: "center" } },
        // { key: 'createby', label: "สร้างโดย", _style: { width: '10%' } },
        { key: 'updatedate', label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: '200px', textAlign: "center" } },
        { key: 'updateby', label: "ผู้แก้ไขข้อมูล", _style: { minWidth: '150px', textAlign: "center" } },
        // { key: 'updateby', label: "แก้ไขโดย", _style: { width: '10%' } },
        

    ]

    const testtest = (e) => {
        // setTagHtml(e.target.value)
        console.log(e)
        // console.log(tagHtml)
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton
                        onClick={() => newDetails()}
                        color="primary-custom"
                        variant="reverse"
                    >
                        <CIcon name="cil-plus" />
                        <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
                    </CButton>
                </CCol>
            </CRow>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    record_status: (item, index) => (
                        <td align="center">
                        <CSwitch
                            key={index}
                            className={"mx-1"}
                            shape={"pill"}
                            color={"primary"}
                            checked={item.record_status == "A" ? true : false}
                            onChange={(e) =>
                                func_change_status(e.target.checked, item)
                            }
                        />
                        </td>
                    ),
                    'seq':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.seq}
                            </td>
                        ),
                    "start_date": (item) => (
                        <td className="text-center">
                        {item.start_date ? moment(item.start_date)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "end_date": (item) => (
                        <td className="text-center">
                        {item.end_date ? moment(item.end_date)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "updatedate": (item) => (
                        <td className="text-center">
                            {item.updatedate ? moment(item.updatedate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createdate": (item) => (
                        <td className="text-center">
                            {item.createdate ? moment(item.createdate)
                                // .add(543, "years")
                                .format("DD-MM-YYYY HH:mm") : ""}
                        </td>
                    ),
                    "createby": (item) => (
                        <td>
                            {item.createby ? item.createby : ""}
                        </td>
                    ),
                    "updateby": (item) => (
                        <td>
                            {item.updateby ? item.updateby : ""}
                        </td>
                    ),
                    image: (item) => (
                        <td>
                            {item.signaturePath != "" && item.image != null && (
                                <img style={{ height: "100px" }} src={WEB_API + item.image}></img>
                            )}
                        </td>
                    ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.id}`} src={edit_icon} />
                                    </CButton>
                                    {/* <CButton
                                        className="ml-3"
                                        color="danger-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                        onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                    >
                                        <img id={`delete-${item.id}`} src={trash_icon} />
                                    </CButton> */}
                                </CButtonGroup>
                            </td>
                        ),

                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger-custom"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="title"> ชื่อแบนเนอร์ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setTitle(e.target.value)} value={title} id="title" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value"> ลำดับแสดง <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput type="number" onChange={e => setSeq(e.target.value)} value={seq} id="sequence" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="Link"> URL <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setLink(e.target.value)} value={link} id="Link" required />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด </CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                    <CFormGroup>
                                            <CLabel htmlFor="image">รูปภาพ<span style={{ color: 'red' }}>* ขนาดรูปที่แนะนำ 1920 x 725 pixels</span></CLabel>
                                            <input onChange={(e) => func_attachFile(e.target)} type="file" id="myFile"/>
                                            {
                                               picture && <img className = "mt-2" style={{ height: "100px" }} src={URL.createObjectURL(picture)}></img>
                                            }
                                            {
                                                (showImage && !picture) && <img className = "mt-2" style={{ height: "100px" }} src={WEB_API + showImage}></img>
                                            }
                                            {/* <img className = "mt-2" style={{ height: "100px" }} src={}></img> */}
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

            <CModal
                show={modalNew}
                onClose={toggleNew}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitNew} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="title"> ชื่อแบนเนอร์ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setTitle(e.target.value)} value={title} id="title" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="value"> ลำดับแสดง <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setSeq(e.target.value)} value={seq} id="sequence" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="Link"> URL <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setLink(e.target.value)} value={link} id="Link" required />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด </CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                        <CLabel htmlFor="image">รูปภาพ<span style={{ color: 'red' }}>* ขนาดรูปที่แนะนำ 1920 x 725 pixels</span></CLabel>
                                            <input onChange={(e) => func_attachFile(e.target)} type="file" id="picture" />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
