import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterBanner from "../../api/ApiMasterBanner";
import ApiUser from "../../api/ApiUser";
import ApiTranLogBack from "../../api/ApiTranLogBack";

const MasterBanner = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [link, setLink] = useState("");
  const [record_status, set_record_status] = useState("A");
  // const [seq, setSeq] = useState("");

  useEffect(() => {
    getData();
    func_add_log("Click Menu Master Banner", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [title, link]);

    const getData = async () => {
        try {
            var model = {
                title: title,
                link_relate: link,
                record_status: record_status
            }
            func_add_log("Search Menu Master Banner", JSON.stringify(model))
            const result = await ApiMasterBanner.getAllMtBanner(model);
            // var user = await ApiUser.get();
            if (result.status === 200) {
                const { data } = result.data;
                // const userAll = user.data.data;
                setRawDataTable(data);
                var res = data.sort((a, b) => a.updatedate > b.updatedate ? -1: 1)
                for (let i = 0; i < res.length; i++) {
                    // const checkupdate = 0
                    // const checkcreate = 0
                    res[i].order = i + 1
                    // for (let j = 0; j < userAll.length; j++) {

                    //     if (data[i].updateby == userAll[j].id) {
                    //         data[i].updateby = userAll[j].userfullname
                    //         checkupdate = 1
                    //     }
                    //     if (data[i].createby == userAll[j].id) {
                    //         data[i].createby = userAll[j].userfullname
                    //         checkcreate = 1
                    //     }
                    //     if (checkupdate == 0) {
                    //         data[i].updateby = null
                    //     }
                    //     if (checkcreate == 0) {
                    //         data[i].createby = null
                    //     }
                    // }
                }
                setDataTable(res);
                console.log(res)
            }
        } catch (error) {

        }
    }

    const onSearch = async () => {
        getData();
        // var model = {
        //     activity_detail: "Menu Config Search",
        //     json: JSON.stringify({
        //         title: title,
        //         seq: seq,
        //         link: link
        //     })
        // }

        // if (rawDataTable && rawDataTable.length > 0) {
        //     var result = rawDataTable;
        //     if (title && title.length > 0) {
        //         result = result.filter((x) => x.title.toLowerCase().includes(title.toLowerCase()));
        //     }
        //     if (link && link.length > 0) {
        //         result = result.filter((x) => x.link_relate.toLowerCase().includes(link.toLowerCase()));
        //     }
        //     if (seq && seq.length > 0) {
        //         result = result.filter((x) => x.seq.toString().toLowerCase().includes(seq.toLowerCase()));
        //     }
        //     for (let i = 0; i < result.length; i++) {
        //         result[i].order = i + 1
        //     }
        //     setDataTable(result);
        // }
    }

    const resetSearchOption = () => {
        setTitle("");
        // setSeq("");
        setLink("")
        // var result = rawDataTable;
        // for (let i = 0; i < result.length; i++) {
        //     result[i].order = i + 1
        // }
        // setDataTable(result);
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardBody>
                            <CRow>
                                <CCol sm="2">
                                    <CLabel>ชื่อแบนเนอร์</CLabel>
                                    <CInput onChange={e => setTitle(e.target.value)} value={title} id="code" />
                                </CCol>
                                {/* <CCol sm="2">
                                    <CLabel>ลำดับ</CLabel>
                                    <CInput onChange={e => setSeq(e.target.value)} value={seq} id="value" />
                                </CCol> */}
                <CCol sm="2">
                  <CLabel>ลิงก์</CLabel>
                  <CInput
                    onChange={(e) => setLink(e.target.value)}
                    value={link}
                    id="link"
                  />
                </CCol>
                                <CCol sm="2">
                                    <CLabel>สถานะ</CLabel>
                                    <CSelect onChange={e => set_record_status(e.target.value)} value={record_status} id="record_status">
                                        <option value="">ทั้งหมด</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                    </CSelect>
                                </CCol>
                <CCol sm="3"></CCol>
                <CCol sm="3" className="pt-4">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> การจัดการแบนเนอร์ </b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default MasterBanner;
