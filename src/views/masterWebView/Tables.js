import React, { useState } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CForm,
  CModalTitle,
  CButtonGroup,
  CLink,
  CTextarea,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterWebView from "../../api/ApiMasterWebView";
import Swal from "sweetalert2";
import parse from "html-react-parser";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ReactQuill, { Quill } from "react-quill";
import "quill/dist/quill.snow.css";

const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");

window.Quill = Quill;

const ImageResize = require("quill-image-resize-module").default;
Quill.register("modules/imageResize", ImageResize);

const DemoTable = ({ data = [], refreshData = () => {} }) => {
  const [modal, setModal] = useState(false);
  const [modalConfirm, setModalConfirm] = useState(false);

  const [id, setId] = useState(0);
  const [name, setName] = useState("");
  const [nameEn, setNameEn] = useState("");
  const [description, setDescription] = useState("");
  const [descriptionEn, setDescriptionEn] = useState("");
  const [recordstatus, setRecordstatus] = useState("");
  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  const toggle = () => {
    setModal(!modal);
  };

  const toggleConfirm = () => {
    setModalConfirm(!modalConfirm);
  };

  const editDetails = (record) => {
    toggle();
    setId(record.id);
    setName(record.name);
    setNameEn(record.name_en);
    setDescription(record.description);
    console.log(record.description);
    setDescriptionEn(record.description_en);
    setRecordstatus(record.recordstatus);
  };

  const modules = {
    imageResize: {
      // See optional "config" below
    },
    toolbar: {
      container: [
        ["bold", "italic", "underline", "strike"], // toggled buttons
        ["blockquote", "code-block"],
        [{ header: 1 }, { header: 2 }], // custom button values
        [{ list: "ordered" }, { list: "bullet" }],
        [{ script: "sub" }, { script: "super" }], // superscript/subscript
        [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
        [{ direction: "rtl" }], // text direction
        [{ size: ["small", false, "large", "huge"] }], // custom dropdown
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        [{ color: [] }, { background: [] }], // dropdown with defaults from theme
        [{ font: [] }],
        [{ align: [] }],
        ["link", "image"],
        ["clean"], // remove formatting button
      ],
    },
  };

  const fields = [
    {
      key: "order",
      label: "ลำดับ",
      _style: { width: "1%", textAlign: "center" },
    },
    {
      key: "option",
      label: "",
      _style: { width: "1%", textAlign: "center" },
      filter: false,
    },

    {
      key: "name",
      label: "ชื่อเมนู",
      _style: { width: "15%", textAlign: "center" },
    },
    {
      key: "name_en",
      label: "ชื่อเมนู EN",
      _style: { width: "15%", textAlign: "center" },
    },
    {
      key: "recordstatus",
      label: "สถานะ",
      _style: { width: "15%", textAlign: "center" },
    },
  ];

  const onSubmit = (e) => {
    e.preventDefault();
    const model = {
      id: id,
      name: name,
      name_en: nameEn,
      recordstatus: recordstatus,
      description: description,
      description_en: descriptionEn,
    };

    console.log(model.description);
    saveData(model);
  };

  const saveData = async (data) => {
    try {
      func_add_log("Update Config", JSON.stringify(data));
      const result = await ApiMasterWebView.update(data);
      if (result.status === 200) {
        const { data } = result.data;
        toggle();
        Swal.fire({
          icon: "success",
          title: "บันทึกสำเร็จ",
          timer: 2000,
        }).then((success) => {
          refreshData();
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: error.response.data,
      });
    }
  };

  return (
    <>
      {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton onClick={newDetails} color={'info'}><CIcon name="cil-plus" /><span className="ml-2">สร้างข้อมูลตั้งค่า</span></CButton>
                </CCol>
            </CRow> */}
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          recordstatus: (item) => (
            <td className="text-center">{item.recordstatus == "A" ? "Active" : "Inactive"}</td>
          ),
          option: (item) => (
            <td className="center">
              <CButtonGroup>
                <CButton
                  color="primary-custom"
                  variant="reverse"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    editDetails(item);
                  }}
                  onMouseOver={(e) =>
                    (document.getElementById(`edit-${item.id}`).src =
                      edit_hover)
                  }
                  onMouseOut={(e) =>
                    (document.getElementById(`edit-${item.id}`).src = edit_icon)
                  }
                >
                  <img id={`edit-${item.id}`} src={edit_icon} />
                </CButton>
                {/* <CButton variant="ghost" size="sm" />
                                        <CButton
                                            color="danger-custom"
                                            variant="outline"
                                            shape="square"
                                            size="sm"
                                            onClick={() => { deleteDetails(item) }}
                                        >
                                            <CIcon name="cil-trash" />
                                        </CButton> */}
              </CButtonGroup>
            </td>
          ),
        }}
      />
      <CModal size="lg" show={modal} onClose={toggle}>
        <CModalHeader closeButton>
          <CModalTitle>แก้ไขข้อมูล</CModalTitle>
        </CModalHeader>
        <CForm onSubmit={onSubmit} action="">
          <CModalBody>
            <CRow>
              <CCol xs="12" sm="12">
                <CRow>
                  <CCol xs="6">
                    <CFormGroup>
                      <CLabel>
                      ชื่อเมนู <span style={{ color: "red" }}>*</span>
                      </CLabel>
                      <CInput
                        id="code"
                        value={name}
                        onChange={(e) => {
                          setName(e.target.value);
                        }}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                  <CCol xs="6">
                    <CFormGroup>
                      <CLabel>
                      ชื่อเมนู EN <span style={{ color: "red" }}>*</span>
                      </CLabel>
                      <CInput
                        id="code"
                        value={nameEn}
                        onChange={(e) => {
                          setNameEn(e.target.value);
                        }}
                        required
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>

                <CRow>
                  <CCol xs="6">
                    <CFormGroup>
                      <CLabel>
                        สถานะ <span style={{ color: "red" }}>*</span>
                      </CLabel>
                      <CSelect
                        custom
                        onChange={(e) => setRecordstatus(e.target.value)}
                        value={recordstatus}
                      >
                        <option value={"A"}>Active</option>
                        <option value={"I"}>Inactive</option>
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                </CRow>

                <CRow className="mt-2">
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>รายละเอียด</CLabel>
                      <ReactQuill
                        onChange={(content, delta, source, editor) => {
                          setDescription(content);
                        }}
                        value={description}
                        modules={modules}
                        id="emailBody"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>

                <CRow className="mt-2">
                  <CCol xs="12">
                    <CFormGroup>
                      <CLabel>รายละเอียด EN</CLabel>
                      <ReactQuill
                        onChange={(content, delta, source, editor) => {
                          setDescriptionEn(content);
                        }}
                        value={descriptionEn}
                        modules={modules}
                        id="emailBody"
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton type="submit" color="primary">
              บันทึก
            </CButton>{" "}
            <CButton color="secondary" onClick={toggle}>
              ยกเลิก
            </CButton>
          </CModalFooter>
        </CForm>
      </CModal>
    </>
  );
};

export default DemoTable;
