import React, { useState, useEffect } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
  CInputCheckbox,
  CPopover,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterTag from '../../api/ApiMasterTag';
import Swal from "sweetalert2";
import moment from 'moment';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const Tables = ({ data = [], refreshData = () => { }, func_add_log }) => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalConfirmDel, setModalConfirmDel] = useState(false);
  const [modalConfirmEdit, setModalConfirmEdit] = useState(false);
  const [dataSelected, setDataSelected] = useState('');
  const userState = useSelector((state) => state.user);
  const [username, setUsername] = useState('');
  const [tagId, setTagId] = useState('');
  const [code, setCode] = useState('');
  const [name, setName] = useState('');
  // const [edit_icon_icon, setEditIcon] = useState(edit_icon);

  useEffect(() => {
      return () => {
      }
  }, []);


  const toggle = () => {
      setModal(!modal);
  };

  const fields = [
    { key: "order", label: "ลำดับ", _style: { width: "1%" } },
    {
        key: 'option',
        label: '',
        _style: { width: '1%' },
        filter: false,
    },
    {
      key: "record_status",
      label: "สถานะ",
      _style: { minWidth: "150px", textAlign: "center" },
      filter: false,
    },
    { key: "code", label: "โค้ดแท็ก", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "name", label: "ชื่อแท็ก" , _style: { minWidth: "150px", textAlign: "center" }},
    { key: 'createdate', label: "วันที่สร้างข้อมูลล่าสุด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: 'createby', label: "ผู้สร้างข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "updatedate", label: "วันที่แก้ไขข้อมูลล่าสุด", _style: { minWidth: "150px", textAlign: "center" } },
    { key: "updateby", label: "ผู้แก้ไขข้อมูล", _style: { minWidth: "150px", textAlign: "center" } },
    
  ];

  const toggleConfirmDel = () => {
      setModalConfirmDel(!modalConfirmDel);
  }
  const toggleConfirmEdit = () => {
    setModalConfirmEdit(!modalConfirmEdit);
}

  const EditDetails = (record) => {
    toggleConfirmEdit();
    setDataSelected(record);
    setUsername(record.username);
    setTagId(record.id);
    setCode(record.code);
    setName(record.name);
}
const insertData = (record) => {
    toggleConfirmEdit();
    setTagId('');
    setCode('');
    setName('');
}

  const deleteDetails = (record) => {
      toggleConfirmDel();
      setDataSelected(record);
      setUsername(record.username);
      setTagId(record.id);
      
  }



  const onSubmit = (e) => {
      e.preventDefault();
      const model = {
          "id": tagId,
          "code": code,
          "name": name,
          "createby":username,
          "updateby":username,
          "record_status":"A"
      }
      saveData(model);
  }

  const func_change_status = async (e, record) => {
    record.record_status = e ? "A" : "I"
    delete record.order;
    saveData(record);
  };

  const saveData = async (data) =>{
    try {
        func_add_log("Save Tag", JSON.stringify(data))
        const result = await ApiMasterTag.save(data);
        if (result.status === 200) {
            const { data } = result.data;
            setModalConfirmEdit(false);
            Swal.fire({
                icon: "success",
                title: "บันทึกสำเร็จ",
                timer: 2000,
            }).then((success) => {
                refreshData();
            });
        }
  } catch (error) {
      Swal.fire({
          icon: "error",
          title: error.response.data,
      });
  }
  }

  const onDelete = async () => {
      try {
          const model = {
              "id": tagId,
              "record_status": 'I'
          }
            func_add_log("Delete Tag", JSON.stringify(model))
            const result = await ApiMasterTag.delete(model);
          if (result.status === 200) {
              const { data } = result.data;
              toggleConfirmDel();
              Swal.fire({
                  icon: "success",
                  title: "ลบสำเร็จ",
                  timer: 2000,
              }).then((success) => {
                  refreshData();
              });
          }
      } catch (error) {
          Swal.fire({
              icon: "error",
              title: error.response.data,
          });
      }
  }


  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CButton
            onClick={() => { insertData()}}
            color="primary-custom"
            variant="reverse"
          >
            <CIcon name="cil-plus" />
            <span className="ml-2"><b>เพิ่มข้อมูล</b></span>
          </CButton>
        </CCol>
      </CRow>
        <CDataTable
            items={data}
            fields={fields}
            tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
            cleaner
            itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
            itemsPerPage={10}
            hover
            sorter
            striped
            bordered
            pagination
            scopedSlots={{
                'order':
                    (item) => (
                        <td style={{ textAlign: 'center' }}>
                            {item.order}
                        </td>
                    ),
                // 'username':
                //     (item) => (
                //         <td>
                //             <CLink href={`/#/user_role?id=${item.id}&user=${item.username}`}>{item.username}</CLink>
                //         </td>
                //     ),
                "updatedate": (item) => (
                    <td className="text-center">
                        {item.updatedate ? moment(item.updatedate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                "createdate": (item) => (
                    <td className="text-center">
                        {item.createdate ? moment(item.createdate)
                            // .add(543, "years")
                            .format("DD-MM-YYYY HH:mm") : ""}
                    </td>
                ),
                "createby": (item) => (
                    <td>
                        {item.createby ? item.createby : ""}
                    </td>
                ),
                "updateby": (item) => (
                    <td>
                        {item.updateby ? item.updateby : ""}
                    </td>
                ),
                'option':
                    (item) => (
                        <td className="center">
                            <CButtonGroup>
                                <CButton
                                    color="primary-custom"
                                    variant="reverse"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { EditDetails(item) }}
                                    onMouseOver={(e) => document.getElementById(`edit-${item.id}`).src = edit_hover}
                                    onMouseOut={(e) => document.getElementById(`edit-${item.id}`).src = edit_icon}
                                >
                                    <img id={`edit-${item.id}`} src={edit_icon} /> 
                                </CButton>
                                {/* <CButton variant="ghost" size="sm" /> */}
                                {/* <CButton
                                    className="ml-3"
                                    color="danger-custom"
                                    variant="reverse"
                                    shape="square"
                                    size="sm"
                                    onClick={() => { deleteDetails(item) }}
                                    onMouseOver={(e) => document.getElementById(`delete-${item.id}`).src = trash_hover}
                                    onMouseOut={(e) => document.getElementById(`delete-${item.id}`).src = trash_icon}
                                >
                                    <img id={`delete-${item.id}`} src={trash_icon} />
                                </CButton> */}
                            </CButtonGroup>
                        </td>
                    ),
                record_status: (item, index) => (
                    <td align="center">
                    <CSwitch
                        key={index}
                        className={"mx-1"}
                        shape={"pill"}
                        color={"primary"}
                        checked={item.record_status == "A" ? true : false}
                        onChange={(e) =>
                            func_change_status(e.target.checked, item)
                        }
                    />
                    </td>
                ),
                
            }}
        />
        <CModal
              show={modalConfirmEdit}
              onClose={setModalConfirmEdit}
          >
              <CModalHeader closeButton>
                  <CModalTitle>{tagId ? "แก้ไขข้อมูล" : "เพิ่มข้อมูล"}</CModalTitle>
              </CModalHeader>
              <CForm onSubmit={onSubmit} action="" >
                  <CModalBody>
                      <CRow>
                          <CCol xs="12" sm="12">
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="code">โค้ดแท็ก <span style={{color:'red'}}>*</span></CLabel>
                                          <CInput onChange={e => setCode(e.target.value)} value={code} id="code" required readOnly={tagId}/>
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                              <CRow>
                                  <CCol xs="12">
                                      <CFormGroup>
                                          <CLabel htmlFor="desc">ชื่อแท็ก <span style={{color:'red'}}>*</span></CLabel>
                                          <CInput onChange={e => setName(e.target.value)} value={name} id="name" required/>
                                      </CFormGroup>
                                  </CCol>
                              </CRow>
                          </CCol>
                      </CRow >
                  </CModalBody>
                  <CModalFooter>
                      <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                      <CButton
                          color="secondary"
                          onClick={() => setModalConfirmEdit(false)}
                      >ยกเลิก</CButton>
                  </CModalFooter>
              </CForm >
          </CModal >
        <CModal
            show={modalConfirmDel}
            onClose={setModalConfirmDel}
            color="danger-custom"
        >
            <CModalHeader closeButton>
                <CModalTitle>ลบรายการ : {username}</CModalTitle>
            </CModalHeader>
            <CModalBody>
                คุณต้องการลบรายการนี้ ?
            </CModalBody>
            <CModalFooter>
                <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                <CButton
                    color="secondary"
                    onClick={() => setModalConfirmDel(false)}
                >ยกเลิก</CButton>
            </CModalFooter>
        </CModal>
    </>
  );
};

export default Tables;