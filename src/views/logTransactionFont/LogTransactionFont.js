import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CLabel,
  CInput,
  CButton,
} from "@coreui/react";
import Tables from "./Tables";
import ApiTranLogFront from "../../api/ApiTranLogFront";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import CIcon from "@coreui/icons-react";
import moment from "moment";
import { encrypt, decrypt } from "../../utils/crypto";
import ApiTranLogBack from "../../api/ApiTranLogBack";

const LogTransaction = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  // const [empid, setEmpId] = useState("");
  const [username, setUsername] = useState("");
  const [fullname, setFullname] = useState("");
  // const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [qDate, setQDate] = useState({
    startDate: moment(),
    endDate: moment(),
  });
  const [qFocused, setQFocused] = useState();

  useEffect(() => {
    getData();
    func_add_log("Click Menu Log Front", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [username, email, fullname, qDate]);

    // useEffect(() => {
    //     getData();
    //     return () => {
    //     }
    // }, [username, email, fullname, qDate]);

    const getData = async () => {
        try {
            var model = {
                username: username,
                fullname: fullname,
                email: email,
                fromdate: qDate.startDate,
                todate: qDate.endDate
            }
            func_add_log("Search Menu Log Front", JSON.stringify(model))
            const result = await ApiTranLogFront.get(model);
            if (result.status === 200) {
                const { data } = result.data;
                // for (let i = 0; i < data.length; i++) {
                    // if (data[i].json.read_card_data) {
                        //   console.log("data[i]::" ,data[i].json);
                        //   console.log("rawData::" , data[i].json.read_card_data);
                        //   console.log("parse::" , JSON.parse(data[i].json.read_card_data));
                        // var decryptData = decrypt(JSON.parse(data[i].json.read_card_data));
                        // console.log("decrypted::" ,decryptData);
                        // var splitData = decryptData.split("#");
                        // console.log("splitData::" ,splitData);
                        // data[i].latitude = splitData[26];
                        // data[i].longitude = splitData[27];
                        // data[i].json.read_card_data = decryptData;
                        // data[i].json.read_card_data = JSON.stringify(decryptData);
                    // }
                // }
                // setRawDataTable(data);
                // onSearch();
                  setDataTable(data.sort((a, b) => a.createdate > b.createdate ? -1: 1));
            }
        } catch (error) {

        }
    }
  const onSearch = () => {
    getData();
    // if (rawDataTable && rawDataTable.length > 0) {
    //     var result = rawDataTable;
    //     if (empid && empid.length > 0) {
    //         result = result.filter((x) => x.emp_id && x.emp_id.length > 0 && x.emp_id.toLowerCase().includes(empid.toLowerCase()));
    //     }
    //     if (username && username.length > 0) {
    //         result = result.filter((x) => x.username && x.username.length > 0 && x.username.toLowerCase().includes(username.toLowerCase()));
    //     }
    //     if (userfullname && userfullname.length > 0) {
    //         result = result.filter((x) => x.userfullname && x.userfullname.length > 0 && x.userfullname.toLowerCase().includes(userfullname.toLowerCase()));
    //     }
    //     if (email && email.length > 0) {
    //         result = result.filter((x) => x.useremail && x.useremail.length > 0 && x.useremail.toLowerCase().includes(email.toLowerCase()));
    //     }
    //     if (qDate.startDate && qDate.startDate != null) {
    //         result = result.filter((x) => moment(x.createdate).startOf('day') >= moment(qDate.startDate).startOf('day'));
    //     }
    //     if (qDate.endDate && qDate.endDate != null) {
    //         result = result.filter((x) => moment(x.createdate).startOf('day') <= moment(qDate.endDate).startOf('day'));
    //     }
    //     setDataTable(result);
    // }
  };

  const resetSearchOption = () => {
    // setEmpId("");
    setUsername("");
    setFullname("");
    // setLastname("");
    setEmail("");
    setQDate({ startDate: moment(), endDate: moment() });
    //   var result = rawDataTable;
    setDataTable([]);
  };

  const onExport = async () => {
    try {
      var model ={
        username: username,
        fullname: fullname,
        email: email,
        fromdate: qDate.startDate,
        todate: qDate.endDate
      }
      func_add_log("Export Log Front", JSON.stringify(model))
      var result = await ApiTranLogFront.onExport(model);
      if (result.status === 200) {
        window.open(result.data)
        // const link = document.createElement("a");
        // link.href = result.data;
        // var fileName = `customer_report_${moment().format(
        //   "yyyyMMDD"
        // )}.xlsx`;
        // link.setAttribute("download", fileName); //or any other extension
        // document.body.appendChild(link);
        // link.click();
      }
    } catch (error) { }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                {/* <CCol sm="2">
                                    <CLabel>รหัสลูกค้า</CLabel>
                                    <CInput onChange={e => setEmpId(e.target.value)} value={empid} id="empid" />
                                </CCol> */}
                <CCol sm="2">
                  <CLabel>รหัสผู้ใช้งาน</CLabel>
                  <CInput
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    id="username"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชื่อ-นามสกุล</CLabel>
                  <CInput
                    onChange={(e) => setFullname(e.target.value)}
                    value={fullname}
                    id="fullname"
                  />
                </CCol>
                {/* <CCol sm="2">
                                    <CLabel>นามสกุล</CLabel>
                                    <CInput onChange={e => setLastname(e.target.value)} value={lastname} id="lastname" />
                                </CCol> */}
                <CCol sm="2">
                  <CLabel>อีเมล</CLabel>
                  <CInput
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="email"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>วันที่บันทึกข้อมูล</CLabel>
                  <DateRangePicker
                    displayFormat="DD/MM/YYYY"
                    small
                    block
                    align="center"
                    startDatePlaceholderText="ตั้งแต่"
                    startDate={qDate.startDate}
                    startDateId="startQDate"
                    endDate={qDate.endDate}
                    endDateId="endQDate"
                    endDatePlaceholderText="ถึง"
                    onDatesChange={(value) => setQDate(value)}
                    focusedInput={qFocused}
                    onFocusChange={(focusedInput) => setQFocused(focusedInput)}
                    orientation="horizontal"
                    openDirection="down"
                    minimumNights={0}
                    isOutsideRange={() => false}
                  />
                </CCol>
                <CCol sm="4" className="pt-4">
                  <CRow>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                    <CCol xs="4">
                        <CButton
                            className="mt-1"
                            color="success-custom"
                            variant="reverse"
                            block
                            onClick={() => onExport()}
                        >
                            <b> ออกรายงาน </b>
                        </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>Activity Log {"(Front End)"}</b>
            </CCardHeader>
            <CCardBody>
              <Tables data={dataTable} refreshData={getData} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default LogTransaction;
