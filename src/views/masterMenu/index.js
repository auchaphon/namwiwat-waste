import React, { useEffect, useState } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CModal,
  CModalHeader,
  CModalFooter,
  CButton,
  CLabel,
  CInput,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiMasterMenu from "../../api/ApiMasterMenu";
import ApiTranLogBack from "../../api/ApiTranLogBack";

const Branch = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");

  useEffect(() => {
    getData();
    return () => { };
  }, []);

  const getData = async () => {
    try {
      const data = [{
        "id": 50,
        "menu_code": "M01",
        "menu_name": "Dashboard",
        "record_status": "A",
        "active_admin": 1,
        "active_operator": 1,
      },
      {
        "id": 51,
        "menu_code": "M01",
        "menu_name": "ผู้ใช้งาน (Staff)",
        "record_status": "A",
        "active_admin": 1,
        "active_operator": 1,
      },
      {
        "id": 52,
        "menu_code": "M01",
        "menu_name": "สิทธิ์การใช้งาน (Staff)",
        "record_status": "A",
        "active_admin": 1,
        "active_operator": 1,
      },
      {
        "id": 53,
        "menu_code": "M01",
        "menu_name": "ข้อมูลลูกค้า",
        "record_status": "A",
        "active_admin": 1,
        "active_operator": 1,
      }]
      setRawDataTable(data);
      for (let i = 0; i < data.length; i++) {
        data[i].order = i + 1;
      }
      setDataTable(data);
    } catch (error) { }
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b> สิทธิ์การใช้งาน​ ({dataTable.length}) </b>
            </CCardHeader>
            <CCardBody>
              <Tables refreshData={getData} data={dataTable} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Branch;
