import React, { useState } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CCard,
  CRow,
  CCardHeader,
  CFormGroup,
  CLabel,
  CInput,
  CForm,
  CModalTitle,
  CButtonGroup,
  CLink,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiMasterConfig from "../../api/ApiMasterConfig";
import Swal from "sweetalert2";
import parse from "html-react-parser";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import ApiMasterMenu from "../../api/ApiMasterMenu";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {
  const fields = [
    {
      key: "menu_name",
      label: "ชื่อเมนู",
      _style: { width: "25%", textAlign: "center" },
    },
    {
      key: "active_operator",
      label: "Operation",
      _style: { width: "25%", textAlign: "center" },
    },
    {
      key: "active_admin",
      label: "Admin",
      _style: { width: "25%", textAlign: "center" },
      filter: false,
    },


    {
      key: "active_guest",
      label: "Guest",
      _style: { width: "25%", textAlign: "center" },
      filter: false,
    },
  ];

  const func_changeRole = async (e, modules, id) => {

  };

  return (
    <>
      {/* <CRow>
                <CCol xs="12" lg="12" >
                    <CButton onClick={newDetails} color={'info'}><CIcon name="cil-plus" /><span className="ml-2">สร้างข้อมูลตั้งค่า</span></CButton>
                </CCol>
            </CRow> */}
      <CDataTable
        items={data}
        fields={fields}
        tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
        cleaner
        itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
        itemsPerPage={10}
        hover
        sorter
        striped
        bordered
        pagination
        scopedSlots={{
          order: (item) => (
            <td style={{ textAlign: "center" }}>{item.order}</td>
          ),
          active_admin: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.active_admin == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "active_admin", item.id)
                }
              />
            </td>
          ),
          active_operator: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.active_operator == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "active_operator", item.id)
                }
              />
            </td>
          ),
          active_guest: (item, index) => (
            <td align="center">
              <CSwitch
                key={index}
                className={"mx-1"}
                shape={"pill"}
                color={"primary"}
                checked={item.active_operator == 1 ? true : false}
                onChange={(e) =>
                  func_changeRole(e.target.checked, "active_operator", item.id)
                }
              />
            </td>
          ),
        }}
      />
    </>
  );
};

export default DemoTable;
