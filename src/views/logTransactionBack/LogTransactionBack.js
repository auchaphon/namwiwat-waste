import React, { useEffect, useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CLabel,
  CInput,
  CButton,
} from "@coreui/react";
import Tables from "./Tables";
import ApiTranLogBack from "../../api/ApiTranLogBack";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import CIcon from "@coreui/icons-react";
import moment from "moment";

const LogTransaction = () => {
  const [dataTable, setDataTable] = useState([]);
  const [rawDataTable, setRawDataTable] = useState([]);
  const [empid, setEmpId] = useState("");
  const [username, setUsername] = useState("");
  const [userfullname, setUserfullname] = useState("");
  const [email, setEmail] = useState("");
  const [qDate, setQDate] = useState({
    startDate: moment(),
    endDate: moment(),
  });
  const [qFocused, setQFocused] = useState();

  useEffect(() => {
    getData();
    func_add_log("Click Menu Log Backoffice", "{}");
    return () => {};
  }, []);

  const func_add_log = async (detail, json) => {
    var model = {
      activity_detail: detail,
      json: json,
    };
    await ApiTranLogBack.addlog(model);
  };

  // useEffect(() => {
  //     getData();
  //     return () => {
  //     }
  // }, [empid, username, email, userfullname, qDate]);

  const getData = async () => {
      try {
          var model = {
            empid: empid,
            username: username,
            userfullname: userfullname,
            email: email,
            fromdate: qDate.startDate,
            todate: qDate.endDate
          }
          const result = await ApiTranLogBack.get(model);
          func_add_log("Search Menu Log Backoffice", JSON.stringify(model))
          if (result.status === 200) {
              const { data } = result.data;
              setRawDataTable(data);
            //   onSearch();
              setDataTable(data.sort((a, b) => a.createdate > b.createdate ? -1: 1));

      }
    } catch (error) {}
  };
  const onSearch = () => {
    getData();
    //   if(rawDataTable && rawDataTable.length > 0){
    //       var result = rawDataTable;
    //       if(empid && empid.length > 0){
    //           result = result.filter((x) => x.emp_id && x.emp_id.length > 0 && x.emp_id.toLowerCase().includes(empid.toLowerCase()));
    //       }
    //       if(username && username.length > 0){
    //           result = result.filter((x) => x.username && x.username.length > 0 && x.username.toLowerCase().includes(username.toLowerCase()));
    //       }
    //       if(userfullname && userfullname.length > 0){
    //           result = result.filter((x) => x.userfullname && x.userfullname.length > 0 && x.userfullname.toLowerCase().includes(userfullname.toLowerCase()));
    //       }
    //       if(email && email.length > 0){
    //           result = result.filter((x) => x.useremail && x.useremail.length > 0 && x.useremail.toLowerCase().includes(email.toLowerCase()));
    //       }
    //       if(qDate.startDate && qDate.startDate != null){
    //           result = result.filter((x) => moment(x.createdate).startOf('day') >= moment(qDate.startDate).startOf('day'));
    //       }
    //       if(qDate.endDate && qDate.endDate != null){
    //           result = result.filter((x) => moment(x.createdate).startOf('day') <= moment(qDate.endDate).startOf('day'));
    //       }
    //       setDataTable(result);
    //   }
  };

  const resetSearchOption = () => {
    setEmpId("");
    setUsername("");
    setUserfullname("");
    setEmail("");
    setQDate({ startDate: moment(), endDate: moment() });
    //   var result = rawDataTable;
    //   setDataTable([]);
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardBody>
              <CRow>
                <CCol sm="2">
                  <CLabel>รหัสพนักงาน</CLabel>
                  <CInput
                    onChange={(e) => setEmpId(e.target.value)}
                    value={empid}
                    id="empid"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>รหัสผู้ใช้งาน</CLabel>
                  <CInput
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    id="username"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>ชื่อ-นามสกุล</CLabel>
                  <CInput
                    onChange={(e) => setUserfullname(e.target.value)}
                    value={userfullname}
                    id="userfullname"
                  />
                </CCol>
                <CCol sm="2">
                  <CLabel>อีเมลพนักงาน</CLabel>
                  <CInput
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="email"
                  />
                </CCol>
                <CCol sm="4">
                  <CLabel>วันที่บันทึกข้อมูล</CLabel>
                  <DateRangePicker
                    displayFormat="DD/MM/YYYY"
                    small
                    block
                    align="center"
                    startDatePlaceholderText="ตั้งแต่"
                    startDate={qDate.startDate}
                    startDateId="startQDate"
                    endDate={qDate.endDate}
                    endDateId="endQDate"
                    endDatePlaceholderText="ถึง"
                    onDatesChange={(value) => setQDate(value)}
                    focusedInput={qFocused}
                    onFocusChange={(focusedInput) => setQFocused(focusedInput)}
                    orientation="horizontal"
                    openDirection="down"
                    minimumNights={0}
                    isOutsideRange={() => false}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-2">
                <CCol className="9"></CCol>
                <CCol sm="3" className="pt-4">
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="primary-custom"
                        variant="reverse"
                        block
                        onClick={() => onSearch()}
                      >
                        <CIcon
                          name="cil-search"
                          style={{ marginRight: "2px" }}
                        />{" "}
                        <b> ค้นหา </b>
                      </CButton>
                    </CCol>
                    <CCol xs="6">
                      <CButton
                        className="mt-1"
                        color="danger-custom"
                        variant="reverse"
                        block
                        onClick={() => resetSearchOption()}
                      >
                        <b> ล้างข้อมูล </b>{" "}
                      </CButton>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              <b>Activity Log {"(Back End)"}</b>
            </CCardHeader>
            <CCardBody>
              <Tables data={dataTable} refreshData={getData} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default LogTransaction;
