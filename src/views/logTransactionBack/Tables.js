import React, { useState } from "react";
import {
  CCardBody,
  CButton,
  CDataTable,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CForm,
  CModalTitle,
  CButtonGroup,
  CTextarea,
} from "@coreui/react";
import moment from "moment";

const Tables = ({ data = [], refreshData = () => {} }) => {
  const [modal, setModal] = useState(false);

  const fields = [
    { key: "emp_id", label: "รหัสพนักงาน" , _style: { minWidth: '150px', textAlign: "center"}},
    { key: "username", label: "รหัสผู้ใช้งาน" , _style: { minWidth: '150px', textAlign: "center"}},
    { key: "userfullname", label: "ชื่อ-นามสกุล", _style: { minWidth: '200px', textAlign: "center"}},
    { key: "useremail", label: "อีเมลพนักงาน", _style: { minWidth: '150px', textAlign: "center"}},
    { key: "activity_detail", label: "Activity Details", _style: { minWidth: '250px', textAlign: "center"}},
    { key: "createdate", label: "วันที่บันทึกข้อมูล", _style: { minWidth: '150px', textAlign: "center"}},
    { key: "json", label: "JSON", _style: { width: '400px', minWidth: '400px', maxWidth: '400px', textAlign: "center" }},
  ];

  const toggle = () => {
    setModal(!modal);
  };

  const showJson = (record) => {
    toggle();
  };

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12"></CCol>
      </CRow>
        <CDataTable
          items={data}
          fields={fields}
          // tableFilter={{
          //   label: "ค้นหา",
          //   placeholder: "พิมพ์คำที่ต้องการค้นหา",
          // }}
          // cleaner
          itemsPerPage={10}
          hover
          sorter
          striped
          bordered
          pagination
          scopedSlots={{
            "emp_id": (item) => (
              <td>
              {item.emp_id ?? ""}
              </td>
            ),
            "username": (item) => (
              <td>
              {item.username ?? ""}
              </td>
            ),
            "userfullname": (item) => (
              <td>
              {item.userfullname ?? ""}
              </td>
            ),
            "useremail": (item) => (
              <td>
              {item.useremail ?? ""}
              </td>
            ),
            "activity_detail": (item) => (
              <td>
              {item.activity_detail ?? ""}
              </td>
            ),
            "json": (item) => (
              <td>
              {JSON.stringify(item.json)}
              </td>
            ),
            "createdate": (item) => (
                <td className="text-center">
                    {item.createdate ? moment(item.createdate)
                        // .add(543, "years")
                        .format("DD-MM-YYYY HH:mm") : ""}
                </td>
            ),
          }}
        />
        <CModal show={modal} onClose={toggle}>
          <CModalHeader closeButton>
            <CModalTitle>JSON</CModalTitle>
          </CModalHeader>
          <CForm action="">
            <CModalBody>
              <CRow>
                <CCol xs="12" sm="12">
                  <CRow>
                    <CCol xs="12">
                      <CFormGroup>
                        <CLabel htmlFor="name">JSON String</CLabel>
                        <CTextarea
                          value='{"id":"20","name":"","date":"2021-05-16T17:00:00.000Z","invited":0,"joined":0,"commented":0,"report_file":"","document_file":"","survey_id":312,"record_status":"A","created_user":"superadmin","created_date":"2021-05-17T08:18:42.000Z","updated_user":"superadmin","updated_date":"2021-05-17T08:18:42.000Z"}'
                          id="json"
                          placeholder="Json Data"
                          rows="10"
                          readOnly
                        />
                      </CFormGroup>
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
              <CButton color="secondary" onClick={toggle}>
                ยกเลิก
              </CButton>
            </CModalFooter>
          </CForm>
        </CModal>
    </>
  );
};

export default Tables;
